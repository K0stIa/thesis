NAME=thesis_main

PDFLATEX=pdflatex
BIBTEX=bibtex

all:
	${PDFLATEX} ${NAME}.tex
	${PDFLATEX} ${NAME}.tex
	${BIBTEX} ${NAME}
	${BIBTEX} own.aux
	${BIBTEX} nonisi.aux
	${BIBTEX} impactedjournals.aux
	${BIBTEX} otherisi.aux
	${BIBTEX} othernonisi.aux
	${PDFLATEX} ${NAME}.tex

clean:
	rm -f *.aux *.toc *.log *.out *.pdf *.bbl *.blg

