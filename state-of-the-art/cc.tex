%!TEX root = ../thesis_main.tex

\chapter{State-of-the-art}
\label{cha:state-of-the-art}

\section{Development of the statistical consistency of learning methods}

\begin{comment}
Plan:
\begin{enumerate}
\item \cite{Cour-LearnPartLabel-JMLR2011} Tskar paper
\item \cite{Cid-SueiroGS14-FlatPartialConsistency} Consistency of Losses for Learning from Weak Labels
\item \cite{Zhang04a} Consistency of $0-1$ Problem
\item \cite{CC-dimm-Multiclass} Classification Calibration Dimension for General Multiclass Losses
\item \cite{VernetWR11-CompositeMulticlassLosses} Composite losses.
\item \cite{Yu-MultiLabel-ICML2014} Large-scale Multi-Label Learning with Missing Labels
\end{enumerate}
\end{comment}


Undisputably, the consistency of a learning method is a desirable property, i.e.
a good learning method should recover the Bayes classifier at least if provided
with an infinitely large training set under the condition that the class of
considered classifiers contains the Bayes classifier. The design of consistent
learning methods for supervised multiclass prediction received the attention in
the last decade: ~\cite{Zhang-Annals2004,Zhang04a, Bartlett-05, HillD07-kernels,
  Tewari-JMLR2007, Liu07-FisherConsistency, Santos-Rodríguez2009, ZhangJLY09,
  CC-dimm-Multiclass}. Statistical properties of learning algorithms based on
the risk minimization formulation are relatively well-understood for the
supervised setting due to the aforementioned works and others. However, there
are quite few works studying risk based minimization methods for the learning
setting with the missing labels. Among the few exceptions belong the works
of~\cite{Taskar11-LearningfromPartialLabels,
  Cid-SueiroGS14-FlatPartialConsistency, Yu-MultiLabel-ICML2014}.

\subsection{Statistical consistency of the supervised flat classifiers}

\cite{Zhang-Annals2004} showed first that binary classifiers obtained by
minimizing infinite-sample consistent surrogate loss for
supervised learning (e.g. the hinge-loss, logistic loss, etc.) can approach
Bayes classifier. \cite{Zhang04a} analysed the consistency of the hinge loss and its
modifications in the context of the multiclass classification formulations such
as pairwise comparison, constrained comparison and One-Versus-All methods.
% gainst the $0/1$-loss.
%In particular,~\cite{Zhang04a} makes analysis of widely used hinge loss and its
%modifications. 
\cite{Liu07-FisherConsistency} considered several multiclass generalizations of
the hinge-loss used in various multiclass SVMs algorithms and showed that some
of them were and others were not statistically consistent.  For some
inconsistent losses,~\cite{Liu07-FisherConsistency} showed how to modify training
algorithm to make the losses behave consistently.  \cite{Tewari-JMLR2007}
characterized classification calibration of supervised multiclass problems in
terms of geometric properties of some sets associated with the surrogate loss
function. Based on these properties, they provided certain sufficient conditions
for the classification calibration and examine the consistency of a few
multiclass methods. \cite{CC-dimm-Multiclass} extended the notion of the
classification calibration from $0/1$-loss and/or binary classification problems
to the general multiclass setting with a general loss.
\cite{CC-dimm-Multiclass} deriveed necessary and sufficient conditions for a
surrogate loss function to be classification calibrated with respect to a given
target loss. They introduced the notion of so called convex classification
calibration dimension of a multiclass loss matrix measuring the size of a
prediction space, in which it is possible to design a convex surrogate that is
calibrated with respect to the target loss. They derived lower and upper
bounds of the classification calibration dimension as well.  These notions can be very
useful if for a given target loss one has to prove existence or non-existence of
a corresponding convex calibrated surrogate loss.  The consistency of multiclass
losses were also considered in the development of various types of other
settings, e.g. the multiclass classification with reject
option~\cite{RamaswamyT015ClassificationReject}, hierarchical
classification~\cite{RamaswamyT015HierarchicalClassification}, multiclass
boosting~\cite{Zhu09multi-classadaboost, MukherjeeS13-boosting}, etc.

\subsection{Statistical consistency of the supervised ordinal classifiers}

\cite{BachOrdConsistency} studied the statistical consistency of methods used
for supervised ordinal classifier learning of rich family of surrogate loss
functions including proportional odds and support vector ordinal regression.
Authors consider the threshold and the regression based models for which they
derived sufficient conditions on statistical consistency for the margin based
methods and the surrogates of the V-shape loss functions.
%
%either prove consistency or provide sufficient conditions for
%threshold and regression based approaches. In particular, the authors derived
%sufficient conditions on statistical consistency for the margin based methods
%and the surrogates of the V-shape loss functions.
% for margin based
%models and used V-shaped surrogate loss, authors derived the sufficient
%conditions on statistical consistency.  
In Section~\ref{sec:partial_formulation}, we will extend the notion of V-shaped
surrogate losses for dealing with interval
annotations. %We discuss its consistency in Chapter~\ref{}.

\subsection{Statistical consistency of the supervised structured output classifiers}

Although there is a progress in studying the supervised multiclass setting for
flat classifiers, there are only few works dedicated directly to the structured
output prediction. \cite{ShiRCHW15} investigated the relationship between the
classification calibration of multiclass losses and losses for a structured
output prediction in supervised scenario.  They proposed a hybrid loss for
supervised multiclass and structured output problems that is a convex
combination of a logarithmic loss for \ac{CRF} and a multiclass hinge loss from
the SVM methods. Their family of losses is similar to those proposed previously
by~\cite{ZhangJLY09} for $0/1$ loss. \cite{ShiRCHW15} provided a condition for a
given loss to be statisticaly consistent for classification, which depends on a
measure of dominance between labels, i.e. the gap between probabilities of the
best labeling and the second best labeling. They showed that the statistical
consistency is necessary also for so called parametric consistency which is
needed when learning models such as the \ac{CRF}s.

\subsection{Statistical consistency of the flat classifiers learned from partially annotated examples}

The literature on consistency of supervised learning methods is rich. The
consistency of methods learning from partially annotated examples has been
addressed very rarely so far. We are aware only of two works addressing the
problem, namely ~\cite{Taskar11-LearningfromPartialLabels}
and~\cite{Cid-SueiroGS14-FlatPartialConsistency}.
\cite{Taskar11-LearningfromPartialLabels} considered the multiclass learning of
flat classifiers from examples with candidate set of admissible labels. 
%with missing labels under some assumptions on the random process generating the
%partial labels. 
They proposed a convex learning formulation based on a minimization of a certain
partial loss. They also analyzed conditions under which their partial loss is
asymptotically consistent against the target $0/1$
loss. \cite{Cid-SueiroGS14-FlatPartialConsistency} proposed a generic framework
which, for a given supervised target loss, allows to derive a classification
calibrated surrogate suitable for learning from training examples with missing
labels.
% the multiclass learning of flat classifiers with missing labels given a
%supervised (target) loss. 
Authors introduced a statistical model under which they show that consistent
surrogate losses for learning with missing labels can be obtained by a linear
transformation of any surrogate consistent loss for the supervised setting. Authors
showed that convexity can be sometimes preserved when adapting a supervised
surrogate loss to its weak consistent counterpart.

Although the setting studied in~\cite{Taskar11-LearningfromPartialLabels,
  Cid-SueiroGS14-FlatPartialConsistency} is quite general, (they considered any
subset of labels as candidate label set), neither of them
can be used to analyze existing methods learning the structured output
classifiers, i.e. the multiclass classification with exponentially large number
of labels.  Their convex surrogate losses designed for flat classifiers are not
suitable for structured case since their evaluation requires solving
computationally intractable subproblems. 

Nevertheless, part of the community drops the statistical part of the problem
and tries to solve the problem using ad-hoc heuristics that give very often
reasonably good results in practice.
%In Chapter~\ref{cha:multi-label-classification}  we give some explanations to the missing statistical component 
%for some heuristics used for structured output learning from  partially annotated data.
For the sake of completeness, we give a brief overview of existing approaches below.