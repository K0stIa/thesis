%!TEX root = ../thesis_main.tex

\section{Ordinal classification}

First it should be mentioned that there is a plethora of works in machine learning
community addressing supervised learning of ordinal classifiers. However, there
is a lack of discriminative methods for learning from partially annotated
examples. The existing approaches are briefly discussed below. 

The ordinal classification models can be split to two groups: a regression based
approaches and a threshold based approaches. The former approach involves the
standard regression model the real-valued output of which is then projected on a
discrete domain corresponding to the ordinal
labels~\cite{CrammerRankingProjecting}. On the other hand, the threshold based
approaches provide a greater flexibility by seeking a mapping $f \colon \SX
\rightarrow \Re$ along with a vector of non-decreasing thresholds which
partition the real-valued prediction into an ordered set of labels. The existing
learning paradigms can be split into two groups as well: the maximum likelihood
based methods and the discriminative methods which are briefly outlined below.

%For the sake of completeness, we give a
%short overview of the supervised learning approaches, which are most relevant to
%our work.  The approaches for learning of ordinal classifier can be split into
%two groups: discriminative methods and maximum likelihood based approaches.
%Properties of the discriminative and the maximum likelihood based methods are complementary each to other.

\subsection{Maximum likelihood methods for learning of ordinal classifier }

%Estimating parameters of a probabilistic model by the maximum likelihood method
%is another paradigm that can be used to learn the ordinal classifiers. 
A plug-in ordinal classifier can be constructed by substituting a
probabilistic model estimated by the ML method to the optimal decision rule
derived for a particular loss function (see e.g.~\cite{Dembczynski-Ordinal-2008}
for a list of losses and corresponding decision functions suitable for ordinal
classification). Parametric probability distributions suitable for modeling the
ordinal labels have been proposed
in~\cite{McCullagh-RegModOrdData-JRSS1980,Fu-CondRisk-JSPI2002,Rennie-Loss-IJCAI2005}.
Besides the parametric methods, the non-parametric probabilistic approaches like
the Gaussian processes were also proposed~\cite{Chu-GaussianProcesses-ICML2005}.


The maximum likelihood approach can be directly applied in the presence of
incomplete annotation (e.g. the setting considered in this work when label
interval is given instead of a single label) by using the
Expectation-Maximization
algorithms~\cite{schlesinger-EM68,Demster-EM-JRSS1997}. However, the maximum
likelihood methods are sensitive to model mis-specification which complicates
their application in modeling complex high dimensional data. In contrast, the
discriminative methods reviewed below are known to be robust against the model
misspecification while their extension for learning from partial annotations is
not trivial. 
%To our best knowledge, the existing discriminative approaches for
%ordinal classification assume the precise annotation only.  That is, each
%training instance is annotated by one label exactly.

\subsection{Discriminative methods for supervised learning of ordinal
  classifiers }

The existing discriminative methods learn parameters of the ordinal classifier by
minimizing a convex proxy of the empirical risk. A Perceptron-like on-line
algorithm PRank has been proposed in~\cite{Crammer-Pranking-NIPS}. A
large-margin principle has been applied for learning ordinal classifiers
in~\cite{Shashua-LargeRank-NIPS2002}. The paper~\cite{Chu-SVOR-ICML2005}
proposed \ac{SVOR-EXP} and the \ac{SVOR-IMC}.
Unlike~\cite{Shashua-LargeRank-NIPS2002}, the \ac{SVOR-EXP} and \ac{SVOR-IMC}
guarantee the learned ordinal classifier to be statistically plausible. The same
approach have been proposed independently by~\cite{Rennie-Loss-IJCAI2005}, who
introduce so called immediate-threshold loss and all-thresholds loss functions.
Minimization of a quadratically regularized immediate-threshold loss and the
all-threshold loss are equivalent to the \ac{SVOR-EXP} and the \ac{SVOR-IMC}
formulation, respectively. A generic framework proposed
in~\cite{Li-OrdregExtBinClass-NIPS2006}, of which the \ac{SVOR-EXP} and
\ac{SVOR-IMC} are special instances, allows to convert learning of the ordinal
classifier into learning of two-class SVM classifier with weighted examples.

\begin{comment}

In this work, we consider learning of the ordinal classifiers from partially
annotated examples. We assume that each training input is annotated by an
interval of candidate labels rather than a single label.
%The complete annotation is a special case of our setting when the intervals
%contain a single label.
This setting is common in practice. For example, let us assume a computer vision
problem of learning an ordinal classifier predicting age from a facial image
(e.g. ~\cite{Ramanathan-CompFacialAging-JVLC2009,Chang-OrdinalHypRanker-CVPR2011}).
%In this application the training examples are face images along with the 
%information about the person age. 
In this case, examples of face images are typically downloaded from the Internet
and the age of depicted persons is estimated by a human annotator. Providing a
reliable year-exact age just from a face image is virtually impossible. For
humans it is more natural and easier to provide an interval estimate of the
age. The interval annotation can be also obtained in an automated way e.g. by
the method of~\cite{Kotlowski-StochasticDominance-JIS2008} removing
inconsistencies in imprecisely annotated data.


To deal with the interval annotations, we propose an interval-insensitive loss
function which extends an arbitrary (supervised) V-shaped loss to the interval
setting. The interval-insensitive loss measures a discrepancy between the
interval of candidate labels given in the annotation and a label predicted by
the classifier. Our interval-insensitive loss can be seen as the ordinal
regression counterpart of the $\epsilon$-insensitive loss used in the Support
Vector Regression~\cite{Vapnik98}. We prove that under reasonable assumptions on
the annotation process, the Bayes risk of the ordinal classifier can be bounded
by the expectation of the interval-insensitive loss. This bound justifies
learning of the ordinal classifier via minimization of an empirical estimate of
the interval-insensitive loss. Tightness of the bound depends on two intuitive
parameters characterizing the annotations process. We show how to control these
parameters in practice by properly designing the annotation process. We propose
a convex surrogate of an arbitrary V-shaped interval-insensitive loss which is
then used to formulate a convex learning problem. We also show how to modify the
existing supervised methods, the SVOR-EXP and the SVOR-IMC algorithms, in order
to minimize a convex surrogate of the interval-insensitive loss associated with
the 0/1-loss and the Mean Absolute Error (MAE) loss. Finally, we design a
variant of a cutting plane algorithm which enables to solve large instances of
the learning problems efficiently.

Discriminative learning from partially annotated examples has been recently
studied in the context of a generic multi-class
classifiers~\cite{Cour-LearnPartLabel-JMLR2011}, the Hidden Markov Chain based
classifiers~\cite{Do-HMMfromPartialStates-ICML2009}, generic structured output
models~\cite{Lou-StructPartialLearn-ICML2012}, the multi-instance
learning~\cite{LuoO10-CandidateSets} etc. All these methods translate
learning to minimization of a partial loss evaluating discrepancy between the
classifier predictions and partial annotations. The partial loss is defined as
minimal value of a supervised loss (defined on a pair of labels, e.g. 0/1-loss)
over all candidate labels consistent with the partial annotation. Our
interval-insensitive loss can be seen as an application of such type of partial
losses in the context of the ordinal classification. In particular, we analyze
the partial annotation in the form of intervals of the candidate labels and the
Mean Absolute Error being the most typical target loss in the ordinal
classification. The bounds of the Bayes risk via the expectation of the partial
loss have been studied in~\cite{Cour-LearnPartLabel-JMLR2011} but only for the
0/1-loss which is much less suitable for ordinal classification.  It worth
mentioning that the ordinal classification model allows for a tight convex
approximations of the partial loss in contrast to previously considered
classification models often requiring a hard to optimize non-convex
surrogates~\cite{Do-HMMfromPartialStates-ICML2009,Lou-StructPartialLearn-ICML2012,LuoO10-CandidateSets}.
\end{comment}
