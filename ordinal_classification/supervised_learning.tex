%!TEX root = ../thesis_main.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{ Supervised learning } 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:supervisedSetting}


There exist several discriminative methods for learning parameters
$(\#w,\#\theta)$ of the ordinal classifier~\equ{equ:OrdRule} from examples, e.g.
~\cite{Crammer-Pranking-NIPS,Shashua-LargeRank-NIPS2002,Chu-SVOR-ICML2005,Li-OrdregExtBinClass-NIPS2006}. To
our best knowledge, all the existing 
methods are fully supervised algorithms requiring a set of completely annotated
training examples
\begin{equation}
   \label{equ:supervisedTrainingSet}
    \SD^m_{xy} = \{(\#x^1,y^1),\ldots,(\#x^m,y^m)\}\in(\SX\times\SY)^m 
\end{equation}
%
typically assumed to be drawn from i.i.d. random variables with some unknown
distribution $p(\#x,y)$. The goal of the supervised learning algorithm is
formulated as follows. Given the loss function
$\ell\colon\SY\times\SY\rightarrow\Re$ and the training
examples~\equ{equ:supervisedTrainingSet}, the task is to learn the ordinal
classifier $h \colon \SX \rightarrow \SY$ with the {\em Bayes risk}
\begin{equation}
  \label{equ:expRisk}
   R^{\ell}(h) = \SE_{p(\#x,y)} \, \ell(y,h(\#x))
\end{equation}
is as small as possible
\begin{equation}
\label{equ:Bayes}
   h^{\ell}_{*} \in \Argmin \limits_{h \colon \SX \rightarrow \SY}R(h) \:.
\end{equation}

The loss functions most commonly used in practice  for ordinal classification are
the \ac{MAE} $\ell^{\mbox{MAE}}(y,y')=|y-y'|$ and the 0/1-loss
$\ell^{0/1}(y,y')=\leftbb y\neq y'\rightbb$. 
Both \ac{MAE} and 0/1-loss are instances of
so called V-shaped losses.
%
\begin{definition}(V-shaped loss). A loss
  $\ell\colon\SY\times\SY\rightarrow\Re$ is V-shaped if $\ell(y,y)=0$ and
$\ell(y'',y)\geq \ell(y',y)$ holds for all triplets $(y,y',y'')\in\SY^3$
such that $|y''-y'| \geq |y'-y|$. 
\end{definition}
%
That is, the value of a V-shaped loss grows monotonically with the distance
between the predicted and the true label. We constrain our
analysis to the V-shaped losses.

Because the expected risk $R^{\ell}(h)$ is not accessible directly due to the
unknown distribution $p(\#x,y)$, the discriminative methods
like~\cite{Shashua-LargeRank-NIPS2002,Chu-SVOR-ICML2005,Li-OrdregExtBinClass-NIPS2006}
minimize a convex surrogate of the empirical risk over a set of linear decision
functions. 
%\cite{Chu-SVOR-ICML2005,Li-OrdregExtBinClass-NIPS2006} adapt
%\ac{SVM} for ordinal classifier~\equ{equ:OrdRule} needs implicitly modelling
%\ac{MAE} and $0/1$ losses.  
In particular, the existing methods approximate the Bayes risk
minimization~\equ{equ:Bayes} by a surrogate \ac{ERM} problem
\begin{equation}
  \label{equ:introBestEmpSurrogate2}
  f_{*}^{\rm emp} \in \Argmin_{f \in \SF } R^{\psi}_{\rm emp} (f) \:,
\end{equation}
%
where 
\begin{equation}
 \label{equ:IntroSurrogateEmpRisk}
R^{\psi}_{\rm emp} (f) = \SE_{s(\#x,y)}\psi(y,f(\#x)) \:
\end{equation}
is the empirical risk and the resulting classification rule is $h = \mbox{pred}
\circ f_{*}^{\rm emp}$. In the case of ordinal classification, the space of
decision functions is defined as
\[
\SF=\Big \{ \#f(\#x)=(\lz \#w,\#x\pz - \theta_1,\ldots,\lz \#w,\#x\pz -
\theta_{Y-1})^T\in\Re^{Y-1} \mid \#w\in\Re^n, \#\theta\in\Theta,
\Omega(\#w,\#\theta) \leq r \Big \}\,,
\]
where $r>0$ is a hyper-parameter and $\Omega \colon\Re^n\times
\Re^{Y-1}\rightarrow\Re_+$ is a convex regularization function. The form of the
prediction transform $\pred$ is defined implicitly by
~\equ{equ:OrdRule}. Because the decision function $\#f\in\SF$ is parametrized by
$(\#w,\#\theta)$ we will use a shortcut $\psi(\#w,\#\theta;\#x,y) = \psi( y,
f(\#x;\#w,\#\theta) )$ which slightly abuses the notation but should not cause a
big confusion. 
%In the next sections we will review a particular form of the surrogates $\psi$
%used by individual methods. 
In practice it is more convenient to solve a problem
%
%
%That is, the \ac{ERM} approach~\cite{Vapnik-Nature95}
%approximates~\equ{equ:Bayes} by
%\begin{equation}
% \label{equ:EmpRiskMin}
%  \hat{h} \in \Argmin_{h\in\SH} \SE_{s(\#x,y)} \, \ell(y,h(\#x)) \:,
%\end{equation}
%where $\SH$ is a fixed hypothesis space and $\SE_{s(\#x,y)}[\ell(y,h(\#x))]$ is the empirical risk with 
%the expectation computed over the empirical distribution
%\begin{equation}
%   s(\#x,y) =\frac{1}{m}\sum_{i=1}^m \leftbb \#x^i=\#x \wedge y^i=y
%   \rightbb \:.
%\end{equation}
%
%It is unknown how to solve the
%\ac{ERM} problem~\equ{equ:EmpRiskMin} efficiently for most loss functions. For
%this reason, the original loss function $\ell(y, y')$ is substituted by convex
%approximation $\psi(\#w, \#\theta ; \#x, y)$.  If we consider the hypothesis
%space $\SH=\{ \#h(\#x;\#w, \#\theta) \mid (\#w, \#\theta)\in\SB(r)\}$ containing
%linear classifiers~\equ{equ:LinClassif} with parameters inside a ball
%$\SB(r)=\{(\#w, \#\theta)\in\Re^n\mid \Omega(\#w, \#\theta)\leq r\}$ with radius
%$r$, the \ac{ERM} problem~\equ{equ:EmpRiskMin} can be approximated by a convex
%problem
%\[
%   (\#w^*, \#\theta^*) \in \Argmin_{(\#w, \#\theta) \in \SB(r)} \frac{1}{m}\sum_{i=1}^m \,
%     \psi(\#w, \#\theta;\#x^i,y^i) \:,
%\]
%
%which is in practice solved via an equivalent unconstrained problem
\begin{equation}
  \label{equ:SOSVMLambda}
  (\#w^*, \#\theta^*) \in \Argmin_{\#w\in\Re^n, \#\theta \in \Theta} \Big
  (\frac{\lambda}{2} \Omega (\#w, \#\theta)  + 
   \frac{1}{m}\sum_{i=1}^m \,
  \psi(\#w, \#\theta;\#x^i,y^i) \Big )\:,
\end{equation}
%
which is however equivalent to~\equ{equ:introBestEmpSurrogate2} with
appropriately set regularization constant $\lambda > 0$.
%
%where the regularization constant $\lambda>0$ implicitly defines the radius $r$. 
%In the literature,
%common choice for regularizer $\Omega(\#w, \#\theta)$  
%in case of ordinal classifier is one of the two followings options 
%$\Omega(\#w, \#\theta) = \| \#w \|^2 + \| \#\theta \|^2$ or $\Omega(\#w, \#\theta) = \|\#w\|^2$.

%To conclude, the \ac{ERM} approach introduces errors of two kinds. First, the approximation error
%is due to constraining the hypotheses to be from $\SH$. Second, the estimation
%error is caused by replacing the expected risk by the empirical risk. The
%statistical learning theory provides conditions under which the empirical risk
%is a good proxy for the expected risk. For example, asymptotic guarantees are
%provided for finite hypothesis space or the space of linear estimators.

In Section~\ref{sec:part-svor-exp} and Section~\ref{sec:part-svor-imc} we review
the most polular methods, i.e. the \ac{SVOR-EXP} algorithm and the \ac{SVOR-IMC}
algorithm~\cite{Chu-SVOR-ICML2005}, respectively. We will show that both
algorithms are instances of~\equ{equ:SOSVMLambda} using a different surrogate
loss $\psi$. We show that the surrogate of the \ac{SVOR-EXP} is an upper bound
of the $0/1$-loss and that the surrogate of the \ac{SVOR-IMC} is an upper bound
of the MAE loss.

In Section~\ref{sec:GenericAlgoOrd} we derive an instance of the \ac{SO-SVM}
algorithm suitable for learning parameters of the \ac{MORD} and the \ac{PW-MORD}
rules. In contrast to the \ac{SVOR-EXP} and \ac{SVOR-IMC}, the \ac{SO-SVM} based
algorithm uses a generic surrogate loss $\psi$ which can approximate arbitrary
target loss function. Another advantage of the \ac{SO-SVM} algorithm is that it
leads to an unconstrained variant of the
problem~\equ{equ:SOSVMLambda}. Therefore larger set of optimization solvers can
be used in contrast to \ac{SVOR-EXP} and \ac{SVOR-IMC} which have to deal with
the constraints.


%incorporating additional constraints on thresholds into optimisation problem,
%which is not always easy to do with existing solvers, and sometimes is
%impossible if one wants to use the solver as a ``black-box''.
%~\cite{Chu-SVOR-ICML2005} show how to augment a classical \ac{SVM} with
%additional constraints by enforcing the inequalities on thresholds taking into
%account all or only adjacent categories. We describe this formulations below and
%additionally rewrite original formulations in view of~\equ{equ:SOSVMLambda}. We
%show the connection between original algorithms from~\cite{Chu-SVOR-ICML2005}
%and algorithms for learning \ac{MORD} classifier. We formulate generic algorithm
%for learning of the ordinal classifier.

A final remark is related to the regularization function $\Omega$. A common
choice in the case of ordinal classifier is either $\Omega(\#w, \#\theta) = \|
\#w \|^2 + \| \#\theta\|^2$ or $\Omega(\#w, \#\theta) = \|\#w\|^2$. 
%The former regularizes all parameters while the letter only the weight vector.  
The former regularizer makes the objective to be smooth, to have a unique
minimizer and easier to deal with in general. However, an influence of the
regularizer on the classification accuracy is unclear. In Section~\ref{sec:cpa}
we develop a generic optimization algorithm which can deal with both variants of
the regularization function. In Section~\ref{sec:experiments} we compare both
variants empirically and show that the choice of the regularizer has a
significant impact on the overall classifier accuracy.

%In the next two sections, we describe the existing learning algorithm for
%ordinal classification, namely \ac{SVM} adaptation for ordinal classifier for
%the \ac{MAE} and $0/1$-losses~\cite{Chu-SVOR-ICML2005}.



\subsection{Support vector ordinal regression: explicit constraints on thresholds}
\label{sec:part-svor-exp}
The original \ac{SVOR-EXP} algorithm~\cite{Chu-SVOR-ICML2005} considers only adjacent categories. In particular, it learns parameters of the
ordinal classifier~\equ{equ:OrdRule} from completely annotated examples $\SD^m_{xy}$
by solving the following convex quadratic optimization problem
%
\begin{equation}
   \label{equ:svor-exp-origin}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\Re^{Y-1}} \bigg[ \frac{\lambda}{2} ||\#w||^2 +
   \sum\limits_{j=1}^{r-1}\Big(\sum\limits_{i=1}^{n^j}\xi^j_i +
   \sum\limits_{i=1}^{n^{j+1}}\xi^{*j+1}_i\Big) \bigg] \: 
\end{equation}
subject to
 %
\begin{eqnarray*}
\lz \#x_i^j, \#w\pz - \theta_j \le -1 + \xi_i^j,\ \xi^j_i  \ge 0,\ \forall i=1,\dots,n^j\,, \\
\lz \#x_i^{j+1}, \#w\pz - \theta_{j} \ge 1 - \xi_i^{*j+1},\ \xi^{*j+1}_i  \ge
0,\ \forall i=1,\dots,n^{j+1}\,, \\ 
\theta_{j} \le \theta_{j+1},\ \forall j \in \{ 1, \ldots, Y-1\}\,.
\end{eqnarray*}

In this setting, the support vector formulation attempts to find the optimal mapping direction~$\#w$ and
thresholds $\#w\in\Re^n$ and $\#\theta\in\Theta=\{\#\theta'\in\Re^{Y-1}\mid \theta_y'
\leq \theta'_{y+1},\;y=1,\ldots,Y-1\}$, which define $Y-1$ parallel discriminative hyperplanes for $Y$ ordered classes
accordingly. In this formulation, each sample in the $y$-th category should have a function value 
that is less than the lower margin $\theta_y-1$, otherwise 
$\lz \#x_i^y, \#w \pz - \left( \theta_y - 1 \right)$ is the error (denoted as $\xi_i^y$). 
Similarly, each sample from $(y+1)$-th category should have a function value that is greater 
than the upper margin $\theta_y + 1$, otherwise $\left( \theta_y + 1 \right) - \lz \#x_i^{y + 1}, \#w \pz$
is the error (denoted as $\xi^{*y}_i$).  See Figure~\ref{fig:SvorExplain} to get more insight to meaning of
$\xi_i^y$ and $\xi_i^{*y}$.


Using auxiliary variables $\theta_0=-\infty$ and  $\theta_Y=\infty$, 
%in~\cite{Antoniuk-Franc-Hlavac-OrdRegIntervalLoss} 
we reformulate~\equ{equ:svor-exp-origin}
 as an equivalent problem  in terms of \ac{ERM} framework as follows

\begin{equation}
  \label{equ:svor-exp}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\hat{\Theta}} \bigg [
    \frac{\lambda}{2} \|\#w\|^2 + \sum_{i=1}^m \psi^{\rm EXP}(\#w,\#\theta;\#x^i,y^i) 
  \bigg ] \:,
\end{equation}
where the optimized convex surrogate loss reads 
\[
    \psi^{\rm EXP}(\#w,\#\theta;\#x,y) = \max (0, 1-\lz \#x,\#w\pz +
    \theta_{y-1}) + \max (0, 1+\lz \#x,\#w\pz - \theta_{y})
\]
%
and $\hat{\Theta}=\{ \#\theta\in\Re^{Y+1} \mid \theta_0 = -\infty, \theta_Y =
\infty, \theta_y \leq \theta_{y+1}, y=1,\ldots, Y-1\}$. 
Note, that the surrogate
$\psi^{\rm EXP}(\#w,\#\theta,\#x,y)$ is a convex upper bound of the 0/1-loss
\[
   \ell^{0/1}(y,h(\#x;\#w,\#\theta)) = \leftbb  y\neq
   h(\#x;\#w,\#\theta) \rightbb = \leftbb \lz \#x, \#w\pz <
    \theta_{y-1} \rightbb +  \leftbb \lz \#x, \#w\pz \ge \theta_{y}
    \rightbb \:,
\]
obtained by replacing the step function $\leftbb t \leq 0\rightbb$ by the
hinge loss $\max(0,1-t)$.

%{\centering
%\begin{picture}(0,0)%
%\includegraphics[width=0.6\textwidth]{./ordinal_classification/svor_demo}%
%\end{picture}%
%\setlength{\unitlength}{3947sp}%
%%
%\begingroup\makeatletter\ifx\SetFigFont\undefined%
%\gdef\SetFigFont#1#2#3#4#5{%
%  \reset@font\fontsize{#1}{#2pt}%
%  \fontfamily{#3}\fontseries{#4}\fontshape{#5}%
%  \selectfont}%
%\fi\endgroup%
%\begin{picture}(11188,7286)(857,-8225)
%\put(1951,-8161){\makebox(0,0)[lb]{\smash{{\SetFigFont{12}{14.4}{\rmdefault}{\mddefault}{\updefault}{\color[rgb]{0,0,0}$\theta_{1}-1$}%
%}}}}
%\end{picture}%
%}

\begin{figure}
\centering
\input{./ordinal_classification/svor_demo.tex}
\caption{The figure explains the meaning of slack variables $\xi$ and $\xi^*$ for the \ac{SVOR-EXP} formulation. Note, example $y+1$ can be counted twice if its projection falls into segment $[ \theta_{j+1}-1, \theta_{j}+1]$, where ($\theta_{j+1}-1 < \theta_{j} + 1$). The idea of the figure taken from~\cite{Chu-SVOR-ICML2005}.
}
\label{fig:SvorExplain}
\end{figure}

\subsection{Support vector ordinal regression: implicit constraints on thresholds}
\label{sec:part-svor-imc}

Instead of considering errors only from the samples of adjacent categories in \ac{SVOR-EXP}, the \ac{SVOR-IMC} algorithm
allows the samples in all categories to contribute errors for each  threshold. That is to say, \ac{SVOR-IMC} algorithm
%~\cite{Chu-SVOR-ICML2005}
 learns parameters of the
ordinal classifier~\equ{equ:OrdRule} from completely annotated examples $\SD^m_{\#xy}$ 
by solving the following quadratic optimization problem
\begin{equation}
   \label{equ:SVOR_IMC}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\Re^{Y-1}} \bigg [\frac{\lambda}{2} ||\#w||^2 +
   \sum\limits_{j=1}^{r-1} \Big(\sum\limits_{k=1}^{j}\sum\limits_{i=1}^{n^k}\xi^j_{ki}
   + \sum\limits_{k=j+1}^{r}\sum\limits_{i=1}^{n^k}\xi^{*j}_{ki} \Big) \bigg ]\:
\end{equation}
%
subject to
%
\[%begin{equation}
   %\label{equ:SVOR_IMC_conditions}
\begin{array}{l}
\lz \#x_i^k, \#w\pz - \theta_j \le -1 + \xi_{ki}^j,\ \xi^j_{ki}  \ge
0,\ k=1,\dots,j,\ i=1,\dots,n^k\:, \\
\lz \#x_i^k, \#w\pz - \theta_{j} \ge 1 - \xi_{ki}^{*j},\ \xi^{*j}_{ki}  \ge
0,\ \ k=j+1,\dots,r,\ i=1,\dots,n^k \:.
\end{array}
\]

The authors of~\cite{Chu-SVOR-ICML2005,Li-OrdregExtBinClass-NIPS2006} proved
that the optimal parameters are admissible,
i.e. $(\#w^*,\#\theta^*)\in(\Re^n,\Theta)$ holds, hence the explicit constraints
$\#\theta\in\Theta$ are not needed in this case. It is also shown that the sum
of slack variables in~\equ{equ:SVOR_IMC} upper bounds the average of the \ac{MAE}
loss $\ell(y,y')=|y-y'|$ computed on the training examples. 
We reformulate~\equ{equ:SVOR_IMC}
 as an equivalent unconstrained minimization problem  in terms of \ac{SO-SVM} framework as follows
%,~\equ{equ:SVOR_IMC_conditions}
\begin{equation}
  \label{equ:svor-imc}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\hat{\Theta}} \bigg [
    \frac{\lambda}{2} \|\#w\|^2 + \sum_{i=1}^m \psi^{\rm IMC}(\#w,\#\theta;\#x^i,y^i) 
  \bigg ]
\end{equation}
where the convex surrogate reads 
\[
    \psi^{\rm IMC}(\#w,\#\theta;\#x,y) = \sum_{y=1}^{y-1} \max (0, 1-\lz \#x,\#w\pz +
    \theta_{y-1}) + \sum_{y=y}^{Y-1} \max (0, 1+\lz \#x,\#w\pz - \theta_{y}) \:.
\]
%
 As in the previous case, the problem~\equ{equ:svor-imc} is an equivalent reformulation of the
quadratic program defining the \ac{SVOR-IMC} algorithm in~\cite{Chu-SVOR-ICML2005}.
It is seen that the surrogate $\psi^{\rm IMC}(\#w,\#\theta;\#x,y)$ is a convex
upper bound of the \ac{MAE} loss 
\[
\ell^{\rm MAE}(y,h(\#x;\#w,\#\theta))= |y-h(\#x;\#w,\#\theta)| 
= \sum_{y'=1}^{y-1} \leftbb\lz\#x,\#w\pz < \theta_{y'-1}\rightbb +
\sum_{y'=y}^{Y-1}\leftbb \lz \#x,\#w\pz \geq \theta_{y'} \rightbb \:.\\
\]

\subsection{Generic learning algorithm for ordinal regression}
\label{sec:GenericAlgoOrd}
\begin{comment}
\todo[inline] {Formulation of the problem. The goal is to get close to the Bayes
classifier. Empirical risk minimization based learning.}
\end{comment}

In Section~\ref{sec:UnifiedView} we showed that various models for ordinal
classification can be seen as a special instances of linear
classifier~\equ{equ:GenClass}. In this section, we derive generic algorithm to
learn~\equ{equ:GenClass} from given fully-supervised set $\SD^m_{\#xy}$ via
\ac{SO-SVM} framework. It is a generic and well understood framework originally
developed for the structured output
learning~\cite{Tsochantaridis-LargeMarginSOL-JMLR2005}. 

Following~\cite{Tsochantaridis-LargeMarginSOL-JMLR2005},
we propose to approximate the empirical risk by 
\begin{equation}
  \label{equ:Risk}
   R(\#W,\#b) = \frac{1}{m}\sum_{i=1}^m \max_{y\in\SY} \Big [ \ell(y,y^i) +
     \big \lz
     \#x^i, \sum_{z\in\SZ} \beta(y,z)\#w_z\big\pz (y - y^i) + b_y - b_{y^i} \Big ]\:.
\end{equation}
%
This risk approximation uses the idea of the margin-rescaling loss
functions~\cite{Tsochantaridis-LargeMarginSOL-JMLR2005} applied to the
classifier~\equ{equ:GenClass}. It is easy to prove that $R(\#W,\#b)$ is a convex
upper bound on the true empirical risk
\[
    R_{\rm emp}(\#W,\#b) = \frac{1}{m}\sum_{i=1}^m \ell(y^i,h(\#x^i,\#W,\#b)) \:
\]
simply by showing that
\begin{equation}
\psi(\#w, \#b ; \#x, y) = \max_{\hat{y}\in\SY} \Big [ \ell(\hat{y},y) +
     \big \lz
     \#x, \sum_{z\in\SZ} \beta(\hat{y},z)\#w_z\big\pz (\hat{y} - y) + b_{\hat{y}} - b_{y} \Big]
\end{equation}
is a convex upper bound on $\ell(\hat{y},y)$.
We can formulate learning of the classifier~\equ{equ:GenClass} as the following
convex unconstrained minimization problem
\begin{equation}
  \label{equ:RiskMinimization}
   (\#W^*,\#b^*) \in \Argmin_{\#W\in\Re^n,\#b\in\Re^Y} \bigg [
     \frac{\lambda}{2} \Omega(\#W, \#b ) + R(\#W,\#b)
  \bigg ] \:,
\end{equation}
where $\Omega(\#W, \#b )$ is typically $\|\#W\|^2$ or $\|\#W\|^2 + \|\#b\|^2$
and $\lambda > 0$ is a prescribed (regularization) constant used to control
over-fitting.

A big effort has been put by the machine learning community into development of
efficient solvers for the problem~\equ{equ:RiskMinimization}. For example, a
generic cutting plane methods like the \ac{BMRM}~\cite{Teo-BMRM-2010} or its
accelerated variant~\cite{Franc-OCA-JMLR2009} can be readily applied to
solve~\equ{equ:RiskMinimization}. However, the existing cutting methods require
the regularizer $\Omega(\#W,\#b)=\|\#W\|^2+\|\#b\|^2$. In Section~\equ{sec:cpa},
we propose a generic solver of ~\equ{equ:RiskMinimization} able to deal with
both regularizers.

Let us compare \ac{SO-SVM} framework with the existing algorithms for learning
the ordinal classifier. First, the \ac{SO-SVM}
formulation~\equ{equ:RiskMinimization} can learn a generic
rule~\equ{equ:GenClass} while the existing methods are tailored to the canonical
form~\equ{equ:OrdRule} only. Second, the existing algorithms consider a limited
set of loss functions $\ell(y,y')$, namely \ac{MAE} and $0/1$-loss. The most
generic approach of~\cite{Li-OrdregExtBinClass-NIPS2006} derives an upper bound
for V-shaped losses.  The third limitation of the existing algorithms is that
they have to care about feasibility of thresholds $\#\theta\in\Theta$ because
they work directly on the parameters of the ordinal classifier (however, it does
not apply to \ac{SVOR-IMC}). This requires to either introduce additional
constraints on the thresholds $\#\theta\in\Theta$ or to impose additional
constraints on the loss function, namely, that the loss must be
convex~\cite{Li-OrdregExtBinClass-NIPS2006}. For instance, the 0/1-loss is not
convex hence the learning algorithms require extra inequality constraints (like
the \ac{SVOR-EXP} algorithm of~\cite{Chu-SVOR-ICML2005}), which may complicate
the optimization. Note that in the proposed approach the
problem~\equ{equ:RiskMinimization} remains unconstrained irrespectively to the
selected loss.

The generality of our framework, however, does not automatically imply that the
risk approximation~\equ{equ:Risk} is better (tighter) than those used in
existing methods. We experimentally show in Section~\ref{sec:StandarBenchmarks}
that in the case of the most frequently used \ac{MAE} loss, the proposed
approximation~\equ{equ:Risk} provides a slightly but consistently better
test accuracy than the existing ones.

%In next section, we will establish connection between \ac{SVOR-IMC} and proposed framework.
Now we are ready to formulate learning of the ordinal classifiers from partially
annotated examples, namely, from interval annotations of the labels. 

\begin{comment}
\subsection{Relation between support vector ordinal regression with implicit constraints on thresholds and proposed framework}
\label{Relation-2-SVOR-IMC}


%The \ac{SO-SVM} learns
%parameters $(\#w, \#b)  \in\Re^n$ of a linear classifier
%\begin{equation}
%  \label{equ:LinClassif}
%  h(\#x;\#w, \#b) \in \Argmax_{y\in\SY} \lz (\#w, \#b),\#\Psi(\#x,y)\pz \:,
%\end{equation}
%%
%where $\#\Psi\colon\SX\times\SY\rightarrow\Re^n$ is a fixed feature map parametrized by $(\#w, \#b)$. The
%value of $\ell(y,h(\#x;\#w, \#b))$ is approximated by a convex upper bound,
%denoted as {\em margin-rescaling loss}, which reads
%\begin{equation}
%  \label{equ:MarginRecaleLoss}
%    \psi(\#w, \#b; \#x,y) = \max_{y'\in\SY}\Big (
%   \ell(y,y') + \lz (\#w, \#b), \#\Psi(\#x,y') - \#\Psi(\#x,y)
%   \pz \Big )\:.
%\end{equation}
%
We are going to show the connection between \ac{SVOR-IMC} and proposed generic approach instantiated for 
\ac{MORD} classifier and \ac{MAE} loss.
When $Z=1$ (thus $\#W = \#w$, \ac{MORD} classifier case), the convex surrogate risk~\equ{equ:Risk} reads as follows
\begin{equation}
\label{equ:MORDSurogateRisk}
   \psi(\#w,\#b; \#x, y) = \max_{\hat{y}\in\SY} \Big [ \ell(\hat{y},y) +
     \big \lz
     \#x, \#w\big\pz (\hat{y} - y) + b_{\hat{y}} - b_{y} \Big ]\:.
\end{equation}
%Clearly, it is a specific instance of~\equ{equ:MarginRecaleLoss}.
When $\ell (y, y') = \ell^{\mbox{MAE}}(y, y')$, an instance of \ac{SO-SVM} with surrogate risk~\equ{equ:MORDSurogateRisk} is, however, not equivalent to \ac{SVOR-IMC}. In our experiments, we observed a slightly better performance of \ac{SO-SVM} with the surrogate risk~\equ{equ:MORDSurogateRisk} against \ac{SVOR-IMC}.
In fact, the \ac{SVOR-IMC} is equivalent to the \ac{SO-SVM}
with the following surrogate risk
\begin{equation}
\label{equ:MORD-IMC-Risk}
   \psi(\#w,\#b; \#x, y) = \max_{\hat{y} \leq y}\Big [\hat{y}-y + 
      \lz \#x,\#w\pz (\hat{y}-y) + b_{\hat{y}} - b_{y} \Big ] + \max_{\hat{y}\ge y} \Big [ 
    y - \hat{y} + \lz \#x,\#w\pz(\hat{y}-y) + b_{\hat{y}} - b_{y}     \Big ]\:.
\end{equation}
\begin{proposition}
\label{theorem:SVOR-IMCEqu}
\ac{SO-SVM} with surrogate risk~\equ{equ:MORD-IMC-Risk} is equivalent to \ac{SVOR-IMC}.
\end{proposition}
This statement is a consequence of the theorem we will formulate and prove later. See Proposition~\ref{theorem:equivalence_svor_imc_iil}.
Note that surrogate risk~\equ{equ:MORD-IMC-Risk} is composed of two surrogate risks of form~\equ{equ:MORDSurogateRisk} 
applied correspondently to two label subdomains $\{ 1, \dots, y\}$ and $\{ y, \dots, Y\}$.
\end{comment}
%Now we are ready to formulate learning of ordinal classifiers from weak annotations, i.e. interval annotations. 