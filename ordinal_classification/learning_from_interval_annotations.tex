%!TEX root = ../thesis_main.tex

\section { Learning from interval annotations} 
\label{sec:partial_formulation}

Analogically to the supervised setting, we assume that the observation
$\#x\in\SX$ and the corresponding hidden label $y\in\SY$ are generated from some
unknown distribution $p(\#x,y)$. In contrast to the supervised setting, 
the training set does not contain a single label for each instance. Instead, we
assume that an annotator provided with the observation $\#x$, and possibly with
the label $y$, returns a partial annotation in the form of an interval of
candidate labels $[y_l,y_r]\in\SP$. The symbol $\SP=\{ [y_l,y_r]\in \SY^2 \mid
y_l\le y_r\}$ denotes the set of all possible partial annotations. The partial
annotation $[y_l,y_r]$ means that the true label $y$ is from the interval
$[y_l,y_r]=\{y\in\SY\mid y_l\leq y \leq y_r\}$. We assume that the
annotator can be modeled by a stochastic process determined by a distribution
$p(y_l,y_r\mid \#x, y)$. That is, we are given a set of partially annotated
examples
\begin{equation}
  \label{equ:partialExamples}
   \SD^m_{xI} =\{(\#x^1,[y^1_l, y^1_r]),\ldots,(\#x^m,[y^m_l, y^m_r])\}\in(\SX\times\SP)^m
\end{equation}
%
assumed to be generated from i.i.d. random variables with the distribution 
\[
   p(\#x,y_l,y_r) = \sum_{y\in\SY} p(y_l,y_r\mid \#x,y)\, p(\#x,y) 
\]
defined over $\SX\times\SP$. The learning algorithms described below do not
require the knowledge of $p(\#x,y)$ and $p(y_l,y_r\mid \#x, y)$. However, it is
clear that the annotation process given by $p(y_l,y_r\mid \#x, y)$ can not be
arbitrary in order to make learning possible. For example, in the case when
$p(y_l,y_r\mid\#x,y) = p(y_l,y_r)$, the annotation would carry no information
about the true label. Therefore we will later assume that the annotation is
consistent in the sense that $y\notin[y_l,y_r]$ implies
$p(y_l,y_r\mid\#x,y)=0$. The consistency of the annotation process is a standard 
assumption used, e.g. in~\cite{Taskar11-LearningfromPartialLabels}.

The goal of learning from the partially annotated examples is formulated as
follows. Given a (supervised) loss function
$\ell\colon\SY\times\SY\rightarrow\Re$ and partially annotated
examples~\equ{equ:partialExamples}, the task is to learn the ordinal
classifier~\equ{equ:OrdRule} whose Bayes risk $R^{\ell}(h)$ defined
by~\equ{equ:expRisk} is as small as possible. Note that the objective remains
the same as in the supervised setting but the information about the labels
contained in the training set is reduced to intervals.

\subsection {Learning by minimizing the interval insensitive loss}
\label{sec:iiLoss}

We define an interval-insensitive loss function in order to measure discrepancy
between the interval annotation $[y_l,y_r]\in\SP$ and the predictions made by
the \ac{MORD} classifier $h(\#x;\#w,\#\theta)\in\SY$ defined by~\equ{equ:MordRule}. 

\begin{definition}(Interval insensitive loss) Let $\ell\colon
  \SY\times\SY\rightarrow\Re$ be a supervised V-shaped loss. The interval
  insensitive loss $\ell_I\colon \SP\times \SY\rightarrow\Re$ associated with
  $\ell$ is defined as
\begin{equation}
   \label{equ:PartialLoss2}
   \ell_I(y_l,y_r,y) = \min_{y'\in[y_l,y_r]} \ell(y',y) = \left \{
     \begin{array}{rcl}
       0 & \mbox{if} & y\in [y_l,y_r] \;,\\
       \ell(y,y_l) & \mbox{if} & y\leq y_l \;,\\
       \ell(y,y_r) & \mbox{if} & y\geq y_r \;.\\ 
     \end{array}
   \right .
\end{equation}
\end{definition}
%\todo[inline]{either label ranges should not overlap or we have to say $\ell(y,y)=0$.}
%
The interval-insensitive loss $\ell_I(y_l,y_r,y)$ does not penalize
predictions, which are in the interval $[y_l,y_r]$. Otherwise the penalty is
either $\ell(y,y_l)$ or $\ell(y,y_r)$ depending on which border of the interval
$[y_l,y_r]$ is closer to the prediction $y$. In the special case of the \ac{MAE}
$\ell(y,y')=|y-y'|$, one can think of the associated interval-insensitive loss
$\ell_I(y_l,y_r,y)$ as the discrete counterpart of the $\epsilon$-insensitive
loss used in the \ac{SVR}~\cite{Vapnik98-StatisticalLearningTheory}.

%It is worth mentioning that discriminative learning from partially annotated
%examples has been recently studied in the context of a generic multi-class
%classifiers~\cite{Taskar11-LearningfromPartialLabels}, the Hidden Markov Chain
%based classifiers~\cite{Do-HMMfromPartialStates-ICML2009}, generic structured
%output models~\cite{Lou-StructPartialLearn-ICML2012}, the multi-instance
%learning~\cite{LuoO10-CandidateSets}, etc. All these methods translate learning
%to minimization of a partial loss evaluating discrepancy between the classifier
%predictions and partial annotations. The partial loss is defined as minimal
%value of a standard loss (defined on a pair of labels, e.g. 0/1-loss) over all
%admissible labels consistent with the partial annotation. 
The interval-insensitive loss~\equ{equ:PartialLoss2} is a special case of the
generic partial loss~\equ{equ:partia-risk-hypothesis} that has been previously
used in the context of different classification models like the generic
multi-class classifiers~\cite{Taskar11-LearningfromPartialLabels}, the Hidden
Markov Chain based classifiers~\cite{Do-HMMfromPartialStates-ICML2009}, generic
structured output models~\cite{Lou-StructPartialLearn-ICML2012}, the
multi-instance learning~\cite{LuoO10-CandidateSets}, etc. However, as it will be
shown later the ordinal classification model allows for a tight convex
approximations of the partial loss in contrast to previously considered
classification models which either require crude approximation or more
frequently a non-convex loss function which is then hard to optimize.
%(\cite{Taskar11-LearningfromPartialLabels}\cite{Do-HMMfromPartialStates-ICML2009}
%\cite{Lou-StructPartialLearn-ICML2012}\cite{LuoO10-CandidateSets}). 
%We propose various convex surrogates for the interval-insensitive
%loss~\equ{equ:PartialLoss2} induced from the 0/1-loss, the MAE loss and a
%generic V-shaped loss in Section~\ref{II-SVOR-EXP}, Section~\ref{II-SVOR-IMC} and
%Section~\ref{genericCvxSurrogate}, respectively.
%~\cite{Do-HMMfromPartialStates-ICML2009,Lou-StructPartialLearn-ICML2012,LuoO10-CandidateSets}.

Having defined the interval-insensitive loss, we can approximate minimization of
the Bayes risk $R^{\ell}(h)$ defined in~\equ{equ:expRisk} by minimization of the expectation
of the interval-insensitive loss
\begin{equation}
  \label{equ:ExpIntervalRisk}
   R^{\ell}_I(h) = \EE_{ p(\#x,y_l,y_r)}\, \ell_I(y_l,y_r,h(\#x;\#w,\#\theta)) \:.
\end{equation}
%
We denote $R^{\ell}_I(h)$ as the {\it partial risk} in the sequel. The question is how
well the partial risk $R^{\ell}_I(h)$ approximates the Bayes risk $R^{\ell}(h)$ being the
target quantity to be minimized. In the rest of this section, we analyze first
this question for the 0/1-loss adapting results
of~\cite{Taskar11-LearningfromPartialLabels}. Next, we present a novel bound for
the \ac{MAE} loss. In particular, we show that the Bayes risk $R^{\ell}(h)$ for both losses
can be upper bounded by a linear function of the partial risk $R^{\ell}_I(h)$.

%In particular, we show that the supervised risk $R(h)$ can be upper bounded by
%$\ell \cdot R_I(h)$ and $R_I(h)+\ell'$ in the case of the 0/1-loss and the
%MAE, respectively, where $\ell$ and $\ell'$ are some constants depending on
%$p(y_l,y_r\mid\#x,y)$.

In the sequel, we assume that the annotation process governed by the
distribution $p(y_l,y_r\mid \#x,y)$ is consistent in the following sense.

\begin{definition}\label{def:consistentAnnotProcess}(Consistent annotation process) Let $p(y_l,y_r\mid \#x,y)$ be
  a properly defined distribution over $\SP$ for any
  $(\#x,y)\in\SX\times\SY$. The annotation process governed by
  $p(y_l,y_r\mid\#x,y)$ is consistent if any $y\in\SY$, $[y_l,y_r]\in\SP$ such
  that $y\notin [y_l,y_r]$ implies $p(y_l,y_r\mid \#x,y) = 0$.
\end{definition}
%
The consistent annotation process guarantees that the true label is always
contained among the candidate labels in the annotation.

We first apply the excess bound for the $0/1$-loss function, which has been
studied in~\cite{Taskar11-LearningfromPartialLabels} for a generic partial annotations
when $\SP$ is not constrained to be a set of label intervals.  The tightness of
the resulting bound depends on the annotation process $p(y_l,y_r\mid \#x, y)$
characterized by so called {\em ambiguity degree} $\veps$. If adopted to
our interval-setting, is defined as
\begin{equation}
  \label{equ:ambiguityDegree}
   \veps=\max_{\#x,y,z\neq y} p(z\in [y_l,y_r] \mid \#x,y) = \max_{\#x,y,z}
   \sum_{[y_l,y_r]\in\SP} \leftbb y_l \leq z \leq y_r \rightbb \;p(y_l,y_r\mid
   \#x, y) \:.
\end{equation}
%
In words, the ambiguity degree $\veps$ is the maximum probability of an extra label $z$
co-occurring with the true label $y$ in the annotation interval $[y_l,y_r]$,
over all labels and observations. 

\begin{theorem}\label{theorem:bound01}
  Let $p(y_l,y_r\mid\#x,y)$ be a distribution describing a consistent annotation
  process with the ambiguity degree $\veps$ defined
  by~\equ{equ:ambiguityDegree}. Let $R^{0/1}(h)$ be the Bayes
  risk~\equ{equ:expRisk} instantiated for the 0/1-loss and let
  $R_I^{0/1}(h)$ be the partial risk~\equ{equ:ExpIntervalRisk}
  instantiated for the interval insensitive loss associated to the
  $0/1$-loss. Then the upper bound
 \[
   %\label{equ:ExcessBound01}
 R^{0/1}(h) \leq \frac{1}{1-\veps} R_I^{0/1}(h)\,
 \]
 holds true for any $h\in\SX\rightarrow\SY$.
\end{theorem}
%
Theorem~\ref{theorem:bound01} is a direct application of Proposition 1
from~\cite{Taskar11-LearningfromPartialLabels}. 

Next we introduce a novel upper bound for the \ac{MAE} loss, which is more frequently used
in applications of the ordinal classifier. We again consider consistent
annotation processes. We characterize the annotation process by two numbers
describing the amount of uncertainty in the training data. First, we use
$\alpha\in [0,1]$ to denoted a lower bound of the portion of exactly annotated
examples, that is, examples annotated by an interval having just a single label
$[y_l,y_r]$, $y_l=y_r$. Second, we use $\beta\in\{0,\ldots,Y-1\}$ to denote the
maximal uncertainty in annotation, that is, $\beta+1$ is the maximal width of
the annotation interval, which can appear in the training data with non-zero
probability.

\begin{definition} 
  \label{def:alphaExactProcess}($\alpha\beta$-precise annotation process) Let
  $p(y_l,y_r\mid \#x,y)$ be a properly defined distribution over $\SP$ for any
  $(\#x,y)\in\SX\times\SY$. The annotation process governed by
  $p(y_l,y_r\mid\#x,y)$ is $\alpha\beta$-precise if 
  \[
  \alpha \leq  p(y,y\mid \#x,y) \quad\mbox{and}\quad 
  \beta \geq \max_{[y_l,y_r]\in\SP} \leftbb p(y_l,y_r\mid  \#x,y) > 0\rightbb \; (y_r - y_l)
  \]
  hold for any $(\#x,y)\in\SX\times\SY$.
\end{definition}
%
Let us consider the extreme cases, to illustrate the meaning of the parameters
$\alpha$ and $\beta$. If $\beta = 0$ or $\alpha=1$ then all examples are
annotated exactly. We are back in the standard supervised setting. On the other
hand, if $\beta=Y-1$ and $\alpha=0$ then it may happen that the annotation
brings no information about the hidden label because the intervals can contain
all labels in $\SY$. With the definition of $\alpha\beta$-precise annotation, we
can upper bound the Bayes risk in terms of the partial risk as follows:

\begin{theorem}\label{theorem:boundMae}
  Let $p(y_l,y_r\mid\#x,y)$ be a distribution describing a consistent
  $\alpha\beta$-precise annotation process.  Let $R^{MAE}(h)$ be the
  Bayes risk~\equ{equ:expRisk} instantiated for the \ac{MAE}-loss and let
  $R_I^{MAE}(h)$ be the partial risk~\equ{equ:ExpIntervalRisk}
  instantiated for the interval insensitive loss associated to the
  \ac{MAE}-loss. Then the upper bound
 \begin{equation}
   \label{equ:ExcessBoundMae}
 R^{MAE}(h) \leq R_I^{MAE}(h) + (1-\alpha)\beta
 \end{equation}
 holds true for any $h\in\SX\rightarrow\SY$.
\end{theorem}
%
Proof of Theorem~\ref{theorem:boundMae} is deferred to Appendix~\ref{appx:boundMae}. 

The bound~\equ{equ:ExcessBoundMae} is obtained by the worst case analysis hence
it may become trivial in some cases. For example, if all examples are annotated
with wide intervals because then $\alpha=0$ and $\beta$ is large. The
experimental study presented in Section~\ref{sec:experiments} nevertheless shows
that the partial risk $R_I$ is a good proxy even in cases when the upper
bound is large. This suggests that better bounds might be derived, for example,
when additional information about $p(y_l,y_r\mid\#x,y)$ is available.

In order to improve the performance of the resulting classifier via the
bound~\equ{equ:ExcessBoundMae}, one needs to control the parameters $\alpha$ and
$\beta$. A possible way, which allows to set the parameters $(\alpha,\beta)$
exactly, is to control the annotation process. For example, given a set of
unannotated randomly drawn input samples $\{\#x_1,\ldots,\#x_m\}\in\SX^m$, we can
proceed as follows:
\begin{enumerate}
\item We generate a vector of binary variables $\#\pi\in\{0,1\}^m$ according to
  Bernoulli distribution with the probability $\alpha$ that the variable is $1$.
\item We instruct the annotator to provide just a single label for each input
  example with index from $\{i\in\{1,\ldots,m\}\mid \pi_i = 1\}$ while the
  remaining inputs (with $\pi_i = 0$) can be
  annotated by intervals not larger than $\beta+1$ labels. That means that
  approximately $m\cdot\alpha$ inputs will be annotated exactly and
  $m\cdot(1-\alpha)$ inputs with intervals.
\end{enumerate}
%
This simple procedure ensures that the annotation process is
$\alpha\beta$-precise though the distribution $p(y_l,y_r\mid\#x,y)$ itself 
is unknown and depends on the annotator.
%We leave the study of consistency of this annotation process for the future work.

%\subsection{Algorithms}
%\label{sec:partial_algorithms}

Above we argued that the partial risk defined as an expectation
of the interval insensitive loss was a reasonable proxy of the target Bayes
risk. In next section, we design algorithms learning the ordinal classifier via
minimization of the quadratically regularized empirical risk used as a proxy for
the expected risk. Similarly to the standard supervised case, we cannot minimize
the empirical risk directly due to a discrete domain of the interval insensitive
loss. For this reason, we derive several convex surrogates, which allow to
translate the risk minimization to tractable convex problems.

We first show how to modify two existing supervised methods in order to learn
from partially annotated examples. Namely, we extend the \ac{SVOR-EXP} and \ac{SVOR-IMC} 
algorithms. The extended interval-insensitive variants
are named \ac{II-SVOR-EXP} (section~\ref{II-SVOR-EXP}) and \ac{II-SVOR-IMC}
(section~\ref{II-SVOR-IMC}), respectively. The \ac{II-SVOR-EXP} is a method
minimizing a convex surrogate of the interval-insensitive loss associated to the
0/1-loss while the \ac{II-SVOR-IMC} is designed for the minimization of \ac{MAE} loss.

In section~\ref{genericCvxSurrogate}, we show how to construct a generic convex
surrogate of the interval-insensitive loss associated to an arbitrary V-shaped
loss. We call a method minimizing this generic surrogate as the \ac{VILMA}. We prove that the
\ac{VILMA} subsumes the \ac{II-SVOR-IMC} (as well as the \ac{SVOR-IMC} as a special case).

\subsection {Interval insensitive support vector ordinal regression: explicit constraints on thresholds}
\label{II-SVOR-EXP}

The interval insensitive loss $\ell_I^{0/1}(y_l,y_r,y)$ derived for the target
$0/1$-loss reads
\[
   \ell_I^{0/1}(y_l,y_r,h(\#x;\#w,\#\theta)) = \min_{y'\in[y_l,y_r]}\leftbb
   y'\neq h(\#x;\#w,\#\theta)\rightbb  = \leftbb \lz \#x, \#w\pz <
    \theta_{y_l-1} \rightbb +  \leftbb \lz \#x, \#w\pz \ge \theta_{y_r}
    \rightbb \:.
\]
We derive its surrogate by replacing the step functions with the hinge loss
which yileds
\[
    \psi_I^{\rm EXP}(\#w,\#\theta; \#x,y_l,y_r) = \max (0, 1-\lz \#x,\#w\pz +
    \theta_{y_l-1}) + \max (0, 1+\lz \#x,\#w\pz - \theta_{y_r}) \:.
\]
The surrogate $\psi_I^{\rm EXP}(\#w,\#\theta; \#x,y_l,y_r)$ is clearly a convex
upper bound of $\ell_I^{0/1}(y_l,y_r,h(\#x;\#w,\#\theta))$ as can be also seen
in Figure~\ref{fig:losses}.


We propose \ac{II-SVOR-EXP} algorithm to learn parameters $(\#w, \#\theta)$ of the
ordinal classifier~\equ{equ:OrdRule} from partially annotated examples $\SD^m_I$
by solving the following convex problem
\begin{equation}
 \label{equ:algoII-SVOR-EXP}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\hat{\Theta}} \bigg [
    \frac{\lambda}{2} \|\#w\|^2 + \sum_{i=1}^m \psi^{\rm EXP}_{I}(\#w,\#\theta; \#x^i,y^i_l, y^i_r)   \bigg ] \:.
\end{equation}

\begin{figure}[htbp]
   \begin{center}
   \includegraphics[height=0.45\textwidth,width=0.45\textwidth]{./ordinal_classification/Fig2}
   \includegraphics[height=0.45\textwidth,width=0.45\textwidth]{./ordinal_classification/Fig3}
   \end{center}
   %
   \caption{The left figure shows the interval insensitive loss
     $\ell^{0/1}_I(\#x,y_l,y_r,h(\#x;\#w,\#\theta))$ associated with the
     0/1-loss and its surrogate $\psi^{\rm
       EXP}_I(\#w,\#\theta; \#x,y_l,y_r))$. The right figure shows the interval
     insensitive loss $\ell^{\rm MAE}_I(\#x,y_l,y_r,h(\#x;\#w,\#\theta))$
     associated with the MAE loss and its surrogate $\psi^{\rm
       IMC}_I(\#w,\#\theta; \#x,y_l,y_r))$. The losses are shown as a function of
     the score $\lz \#x,\#w\pz$ evaluated for $\theta_1=1, \theta_2=2,\ldots,
     \theta_{Y-1}=Y-1$ and $y_l=4$, $y_r=6$. Note that for this particular
     setting of $\#\theta$ the surrogate $\psi^{\rm
       EXP}_I(\#w,\#\theta; \#x,y_l,y_r))$     
     also appears to upper bound $\ell^{\rm MAE}_I(\#x,y_l,y_r,h(\#x;\#w,\#\theta))$,
     however, this does not hold in general. }
   %
   \label{fig:losses}
\end{figure}

\subsection {Interval insensitive support vector ordinal regression: implicit constraints on thresholds}
\label{II-SVOR-IMC}

Analogically, we derive a convex surrogate of the interval
insensitive loss $\ell_I^{MAE}(y_l,y_r,y)$ associated with the \ac{MAE} as follows
\[
   \ell_I^{\rm MAE}(y_l,y_r,h(\#x;\#w,\#\theta)) = \min_{y'\in[y_l,y_r]}|
   y' -h(\#x;\#w,\#\theta)| =
   \sum_{y'=1}^{y_l-1} \leftbb\lz\#x,\#w\pz < \theta_y\rightbb +
   \sum_{y'=y_r}^{Y-1}\leftbb \lz \#x,\#w\pz \geq \theta_y \rightbb \:.
\]
We obtain a convex surrogate by replacing the step functions by the hinge loss
\[
\psi^{\rm IMC}_I(\#w,\#\theta; \#x,y_l,y_r) = \sum_{y'=1}^{y_l-1} \max (0, 1-\lz
\#x,\#w\pz + \theta_{y'-1}) + \sum_{y'=y_r}^{Y-1} \max (0, 1+\lz \#x,\#w\pz -
\theta_{y'}) \:,
\]
which is obviously an upper bound of $\ell_I^{\rm
  MAE}(y_l,y_r,h(\#x;\#w,\#\theta))$ as can be also seen in Figure~\ref{fig:losses}.

Given the partially annotated examples $\SD^m_I$, we can learn parameters
$(\#w,\#\theta)$ of the ordinal classifier~\equ{equ:OrdRule} by
solving
\begin{equation}
 \label{equ:algoII-SVOR-IMC}
   (\#w^*,\#\theta^*) \in \Argmin_{\#w\in\Re^n,\#\theta\in\Re^n} \bigg [
    \frac{\lambda}{2} \|\#w\|^2 + \sum_{i=1}^m \psi^{\rm IMC}_{I}(\#w,\#\theta;\#x^i,y^i_l, y^i_r) 
  \bigg ] \:.
\end{equation}
We denote the modified variant as the \ac{II-SVOR-IMC}
algorithm. Note that due to the equality $\psi^{\rm
  IMC}_I(\#w,\#\theta; \#x,y,y)= \psi^{\rm IMC}(\#w,\#\theta; \#x,y)$ it is clear
that the proposed \ac{II-SVOR-IMC} subsumes the original supervised \ac{SVOR-IMC} as a
special case.

\subsection {V-shaped interval insensitive loss minimization algorithm}
\label{genericCvxSurrogate}

In this section, we propose a generic method for learning the ordinal classifiers
with arbitrary interval insensitive V-shaped loss.  
The \ac{MORD} parametrization allows to adopt existing techniques for linear
classification. Given a V-shaped
supervised loss $\ell\colon\SY\times\SY\rightarrow\Re$, we propose to
approximate the value of the associated interval insensitive loss
$\ell_I(y_l,y_r,h'(\#x;\#w,\#b))$ by a surrogate loss
$\psi_I\colon \Re^n\times\Re^Y \times \SX\times\SP\rightarrow\Re$ 
defined as
\begin{equation}
  \label{equ:genericCvxSurrogate}
  \begin{array}{rcl} \psi_I(\#w,\#b; \#x,y_l,y_r) &= &\displaystyle\max_{y\leq
      y_l}\Big [\ell(y,y_l) + \lz 
    \#x,\#w\pz (y-y_l) + b_y - b_{y_l} \Big ] \\ & & \displaystyle+ \max_{y\ge y_r} \Big [ 
    \ell(y,y_r) + \lz \#x,\#w\pz(y-y_r) + b_y - b_{y_r}     \Big ]\:.
  \end{array}
\end{equation}
%
It is seen that the function
$\psi_I(\#w,\#b; \#x,y_l,y_r)$ is a sum of two point-wise maxima over linear
functions for fixed $(\#x,y_l,y_r)$. Hence, it is convex in the parameters $(\#w,\#b)$. The following
proposition states that the surrogate is like the previous surrogates an upper
bound of the interval insensitive loss.

\begin{proposition}
  \label{proposition:surrogateUpperBound}
  For any $\#x\in\Re^n$, $[y_l,y_r]\in\SP$, $\#w\in\Re^n$ and $\#b\in\Re^Y$ the inequality 
  \[
     \ell_I(y_l,y_r,h'(\#x;\#w,\#b)) \leq \psi_I(\#w,\#b; \#x,y_l,y_r) 
  \]
  holds where $h'(\#x;\#w,\#b)$ denotes response of the \ac{MORD}
  classifier~\equ{equ:MordRule}. 
\end{proposition}
%
Proof is deferred to Appendix~\ref{appx:OrdEqulMord}.

Given partially annotated training examples $\SD^m_I$, 
%$\{(\#x^1,[y^1_l,y^1_r]),\ldots,(\#x^m,[y^m_l, y^m_r])\}\in(\SX\times\SP)^m$, 
we can learn parameters $(\#w,\#b)$ of the \ac{MORD} classifier~\equ{equ:MordRule} by
solving the following unconstrained convex problem
\begin{equation}
 \label{equ:genericIll}
  (\#w^*,\#b^*) = \argmin_{\#w\in\Re^n,\#b\in\Re^Y}
  \bigg [ \frac{\lambda}{2}\|\#w\|^2 + \frac{1}{m}\sum_{i=1}^m
  \psi_I(\#w,\#b; \#x^i,y_l^i,y_r^i) \bigg ] \:,
\end{equation}
%
where $\lambda\in\Re_{++}$ is a regularization constant. A suitable value of the
regularization constant is typically tuned on the validation set.
%For example, in the
%experiments we use $\Omega(\#w,\#b)=\frac{\lambda}{2}\|\#w\|^2$ with the
%regularization constant $\lambda\in\Re_{++}$ tuned on validation data.
%An efficient solver for
% minimization of the objective $F_I(\#w,\#b)$ is proposed in Section [TBA:
% ref].
In the sequel, we denote the method based on solving~\equ{equ:genericIll} as the
\ac{VILMA}.

As important example, let us consider the
surrogate~\equ{equ:genericCvxSurrogate} instantiated for the \ac{MAE} loss. In this
case, the surrogate becomes
\begin{equation}
  \label{equ:II-MAE-surrogate}
  \begin{array}{rcl} \psi_I^{\rm MAE}(\#w,\#b; \#x,y_l,y_r) &= &\displaystyle\max_{y\leq
      y_l}\Big [ y_l-y + \lz 
    \#x,\#w\pz (y-y_l) + b_y - b_{y_l} \Big ] \\ & & \displaystyle+ \max_{y\ge y_r} \Big [ 
    y - y_r + \lz \#x,\#w\pz(y-y_r) + b_y - b_{y_r}     \Big ]\:.
  \end{array}
\end{equation}
%


It is interesting to compare the \ac{VILMA} instantiated for the \ac{MAE} loss with the
\ac{II-SVOR-IMC} algorithm, which optimizes a different surrogate of the same
loss. Note that the \ac{II-SVOR-IMC} learns the parameters $(\#w,\#\theta)$ of the
ordinal classifier~\equ{equ:OrdRule} while the \ac{VILMA} parameters $(\#w,\#b)$ of
the \ac{MORD} rule~\equ{equ:MordRule}. The following proposition states that
surrogates of both methods are equivalent.

\begin{proposition}
  \label{theorem:equivalence_svor_imc_iil} Let $\#w\in\Re^n, \#\theta\in\Theta,
  \#b\in\Re^{Y}$ be a triplet of vectors such that $h(\#x;\#w,\#\theta) =
  h'(\#x;\#w,\#b)$ holds for all $\#x\in\SX$ where $h(\#x;\#w,\#\theta)$ denotes
  the ordinal classifier~\equ{equ:OrdRule} and $h'(\#x;\#w,\#b)$ the \ac{MORD}
  classifier~\equ{equ:MordRule}. Then the equality
  \[
      \psi^{\rm IMC}_I(\#w,\#\theta; \#x,y_l,y_r) = \ell_I^{\rm
        MAE}(\#w,\#b; \#x,y_l,y_r)
   \]
 holds true for any $\#x\in\SX$ and $[y_l,y_r]\in\SP$.
\end{proposition}
%
Proof is deferred to
Appendix~\ref{appx:VILMAequalSVORIMC}. 
%Note, proposition~\equ{equ:MORD-IMC-Risk} follows automatically
%from Proposition~\ref{theorem:equivalence_svor_imc_iil} (This is the reason why we have not provided 
%the proof in section~\ref{Relation-2-SVOR-IMC}).

Proposition~\ref{theorem:equivalence_svor_imc_iil}
ensures that the \ac{II-SVOR-IMC} algorithm and the \ac{VILMA} with \ac{MAE} loss both return
the same classification rules although differently parametrized.

The core properties of the generic method, the \ac{VILMA},
proposed in this section:
\begin{enumerate}
  \item \ac{VILMA} is applicable for an arbitrary V-shaped loss, 
  \item \ac{VILMA} subsumes the \ac{II-SVOR-IMC} algorithm optimizing the \ac{MAE} loss as a
    special case,
  \item \ac{VILMA} converts learning into an unconstrained convex optimization. Note
    that the \ac{II-SVOR-EXP} and the \ac{II-SVOR-IMC} in contrast to \ac{VILMA} maintain the
    set of linear constraints $\#\theta\in\Theta=\{\#\theta'\in\Re^{Y-1}\mid \theta_y'
\leq \theta'_{y+1},\;y=1,\ldots,Y-1\}$.
\end{enumerate}

In next section we will describe a solver for the optimisation problem~\equ{equ:genericIll}.