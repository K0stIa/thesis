%!TEX root = ../thesis_main.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The model} % CHAPTER
\label{sec:model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Let $\SX\subset\Re^n$ be a space of input observations and $\SY=\{1,\ldots,Y\}$
a set of hidden labels endowed with a natural order\footnote{The sequence
  $1,\ldots, Y$ is used just for a notational convenience. However, any other
  finite and fully ordered
  set can be used instead. }.  We consider learning of an ordinal classifier 
 $h\colon\SX\rightarrow\SY$ of the form
\begin{equation}
   \label{equ:OrdRule}
    h(\#x;\#w,\#\theta) = 1 + \sum_{k=1}^{Y-1} \leftbb \lz \#x, \#w\pz >
    \theta_k \rightbb \:,
\end{equation}
%
where $\#w\in\Re^n$ and $\#\theta\in\Theta=\{\#\theta'\in\Re^{Y-1}\mid \theta_y'
\leq \theta'_{y+1},\;y=1,\ldots,Y-1\}$ are admissible parameters. The brackets 
$\lz\cdot,\cdot \pz$ denote the dot product and the operator $\leftbb
A\rightbb$ is the Iverson bracket. It evaluates to 1 if $A$ holds, otherwise it is $0$. The
classifier~\equ{equ:OrdRule} splits the real line of projections $\lz
\#x,\#w\pz$ into $Y$ consecutive intervals defined by thresholds $\theta_1 \le
\theta_2 \le \dots \le \theta_{Y-1}$. The observation $\#x$ is assigned a label
corresponding to the interval, to which the projection $\lz\#w,\#x\pz$ falls to.
The classifier~\equ{equ:OrdRule} is a suitable model if the label can be thought
of as a rough measurement of a continuous random variable
$\xi(\#x)=\lz\#x,\#w\pz+\mbox{noise}$~\cite{McCullagh-RegModOrdData-JRSS1980}.
An example of the ordinal classifier applied to a toy 2D problem is depicted in
Figure~\ref{fig:ordregExample}.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.7\textwidth]{./ordinal_classification/regression_idea}
    \caption{The figure vizualizes division of the 2-dimensional feature space
      into four classes realized by an instance of the
      ordinal classifier~\equ{equ:OrdRule}.  }
    \label{fig:ordregExample}
  \end{center}
\end{figure}

We define an equivalent parametrisation of an ordinal classifier in the next section.

%%%%%%%%%%%
\begin{comment}

The typical class of
classifiers considered in the context of ordinal regression is the linear
thresholded rule
\begin{equation}
   \label{equ:OrdRule}
    h(\#x;\#w,\#\theta) = 1 + \sum_{k=1}^{Y-1} \leftbb \lz \#x, \#w\pz >
    \theta_k \rightbb \:,
\end{equation}
where $\#x\in\SX=\Re^n$ is a vector of real-valued features, $\#w\in\Re^n$ is a
parameter vector and $\#\theta=(\theta_1,\ldots,\theta_{Y-1})\in\Re^{Y-1}$  a
vector of thresholds. In the sequel we refer to~\equ{equ:OrdRule} as the ordinal
(ORD) classifier. We call the vector $\#\theta$ admissible iff its components are
non-decreasing i.e. $\#\theta\in\Theta=\{\#\theta'\in\Re^{Y-1}\mid
\theta'_k\le \theta'_{k+1}$, $k=1,\ldots,Y-1\}$.
The classifier~\equ{equ:OrdRule} is a suitable model if the label can be thought
of as a rough measurement of a continuous random variable
$\xi(\#x)=\lz\#x,\#w\pz+\mbox{noise}$~\cite{McCullagh-RegModOrdData-JRSS1980}.
The form of the ORD classifier reflects the assumption that the classes
correspond to intervals on $\Re$. It is seen that ORD classifier
predicts $y$ iff the value $\lz \#x,\#w\pz$ is in the interval $U(y)$.
An example of the ordinal classifier applied to a toy 2D problem is depicted in
Figure~\ref{fig:ordregExample}.

 \begin{figure}
\centering
\includegraphics[height=4.5cm]{./ordinal_classification/regression_idea}
\caption{The figure vizualizes division of the $2$-dimensional feature space into four classes realized by an instance of the ordinal classifier~\equ{equ:OrdRule}. }
\label{fig:ordregExample}
\end{figure}

\end{comment}
%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection {Ordinal regression as linear multi-class classification}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:Equivalence}

Let us start with one-dimensional observations $x\in\SX=\Re$. In such case the
ordinal classifier $h(x) = 1+\sum_{k=1}^{Y-1} \leftbb x > \theta_k \rightbb$ splits
the real axis into $Y$ intervals defined by thresholds $\theta_1\leq
\theta_2\leq \cdots \leq \theta_{Y-1}$. One may think of representing the ORD
classifier in the form
\begin{equation}
  \label{equ:1dMord}
   h'(x) = \argmax_{y\in\SY} f(x,y) \:,
\end{equation}
%
where $f\colon \Re\times\SY\rightarrow\Re$ is a discriminant function. If we
manage to construct the discriminant functions such that $f(x,y)  > f(x,y')$,
$y'\in\SY\setminus\{y\}$ iff $h(x)=y$ then both representations will be
equivalent i.e. $h'(x) = h(x)$, $x\in\Re$. Let us consider a linear discriminant
function with the slope equal to $y$, i.e. $f(x,y) = x\cdot y + b_y$. In such 
case ~\equ{equ:1dMord} becomes a linear multi-class classifier. It is not
difficult to see that such linear classifier also splits the real axis into
intervals. Figure~\ref{fig:MordIdea} shows an example of the ordinal classifier and
its equivalent linear classifier $h'(x)$.

\begin{figure}
\centering
\includegraphics[height=4.5cm]{./ordinal_classification/mord_idea}
\caption{The figure illustrates the relation between the ordinal classifier
  $h(x)=1+\sum_{k=1}^{Y-1}\leftbb x > \theta_k \rightbb$ and its alternative
  representation $h'(x) =\argmax_{y\in\SY} ( x\cdot y + b_y )$ for the
  $(Y=3)$-class problem. Note, that $x$ and $y$-axes have different scale in order to save
  space.}
\label{fig:MordIdea}
\end{figure}

The same idea can be applied for $n$-dimensional observations
$\#x\in\SX=\Re^n$. The multi-class linear classifier which can represent the ordinal
classifier~\equ{equ:1dMord} reads
\begin{equation}
  \label{equ:MordRule}
   h'(\#x;\#w,\#b) = \argmax_{y\in\SY} \Big ( \lz \#x, \#w\pz\cdot y
   + b_y\Big ) \:,
\end{equation}
%
where $\#w\in\Re^n$ is the parameter vector and $\#b=(b_1,\ldots,b_Y)\in\Re^Y$ is a
vector of intercepts. We denote~\equ{equ:MordRule} as the \acf{MORD}. 
Later in this text, we assume that the ``$\argmax$'' operator
returns the minimal label in the case of more than one maximizer.

A natural question is whether both representations are equivalent in the sense
that any ordinal classifier can be represented by some \ac{MORD} classifier and
vice-versa. The following theorem gives the positive answer to the question.

\begin{theorem}
\label{theorem:MORDEqu}
  The ordinal classifier~\equ{equ:OrdRule} and the \ac{MORD}
  classifier~\equ{equ:MordRule} are equivalent in the following sense.  For any
  $\#w\in\Re^n$ and admissible $\#\theta\in\Theta$ there exists $\#b\in\Re^Y$
  such that $h(\#x,\#w,\#\theta) = h'(\#x,\#w,\#b)$, $\forall \#x\in\Re^n$. For
  any $\#w\in\Re^n$ and $\#b\in\Re^n$ there exists admissible
  $\#\theta\in\Theta$ such that $h(\#x,\#w,\#\theta) = h'(\#x,\#w,\#b)$,
  $\forall \#x\in\Re^n$.
\end{theorem}

Our proof (see Appendix~\ref{appx:MORDEqu}) is constructive in the sense that we can provide a conversion from the
ordinal classifier to the \ac{MORD} classifier and vice-versa. 

In exotic cases, which
however may appear in practice, some classes can collapse to a single point and
effectively disappear. To cover all such situations, we first define the concept
of non-degenerated classifier and then we give formulas for the conversions.

\begin{definition}[Degenerated and non-degenerated classifier]
We call class $y\in\SY$ non-degenerated for classifier $h'(\#x)$ iff $\SX_{y} =
{\rm interior}(\{\#x \in \SX : h'(\#x) = y\}) \neq \emptyset $.
Classifier $h'(\#x)$ is non-degenerated iff all classes are
non-degenerated. In the opposite case, the classifier is called degenerated.
\end{definition}

\begin{definition}
Given a \ac{MORD} classifier, the class $\hat{y}\in\SY$ is non-degenerated iff the linear
inequalities
\begin{equation}
\begin{array}{l}
\label{equ:equality_system}
z \hat{y}  + b_{\hat{y}} > z (\hat{y}-k)  + b_{\hat{y}-k} ,\ 1 \le k < \hat{y}\,, \\
z \hat{y}  + b_{\hat{y}} \ge z (\hat{y}+t)  + b_{\hat{y}+k} ,\ 1 < t \le Y-\hat{y}\,,
\end{array}
\end{equation}
are solvable w.r.t. $z\in\Re$. 
\end{definition}
Note that the validity of~\equ{equ:equality_system} can be verified in $\SO(Y)$
time.  The proof of Theorem~\ref{theorem:MORDEqu} can be found in Appendix
~\ref{appx:MORDEqu}. 
%refer to the proof of theorem~\ref{theorem:MORDEqu} 
%in Appendix ~\ref{appx:MORDEqu} for more details. 
The proof is a constructive, i.e., it provides formulas which allow to convert
the \ac{MORD} classifier~\equ{equ:MordRule} to the standard ordinal
classifier~\equ{equ:OrdRule} and vice-versa.

\paragraph{Conversion formulas.}
Given parameters of the ordinal classifier $\#w\in\Re^n$, $\#\theta\in\Theta$, the
equivalent \ac{MORD} classifier has parameters $\#w$ and $\#b$ given by
\begin{equation}
\begin{array}{l}
  \label{equ:Ord2MordRule}
b_1 = 0 \qquad \mbox{and}\qquad b_{y} = -\sum \limits_{i=1}^{y-1} \theta_i,\ y = 2,\dots,Y .
\end{array}
\end{equation}

The conversion from the \ac{MORD} classifier to the ordinal classifier is done
differently for the non-generated and the degenerated classifier. Given
parameters of a non-degenerated \ac{MORD} classifier $\#w\in\Re^n$ and $\#b\in\Re^Y$,
we can compute thresholds $\#\theta\in\Theta$ of the equivalent ordinal classifier by

\begin{equation}
\begin{array}{l}
  \label{equ:nondegeneratedMord2OrdRule}
\theta_y = b_y - b_{y+1},\qquad y = 1,\dots,Y-1\:.
\end{array}
\end{equation}

Given parameters of a degenerated \ac{MORD} classifier $\#w\in\Re^n$ and $\#b\in\Re^Y$,
we compute thresholds $\#\theta\in\Theta$ of the equivalent ORD classifier by

\begin{equation}
\begin{array}{l}
  \label{equ:degeneratedMord2OrdRule}
\theta_{y_i} = \dots = \theta_{y_{i+1}-1} = \frac{b_{y_i} - b_{y_{i+1}}} { (y_{i+1}-y_{i}) },\ i=1,\dots,p,
\end{array}
\end{equation}

where $y_i \in \SY,\ i=1,\dots,p$ is an increasing subsequence of non-degenerated classes.

Finally, let us note that the \ac{MORD} classifier is represented by $n+Y$ parameters insted of
$n+Y-1$ parameters of the ordinal classifier. However, the parameters of the \ac{MORD}
classifier are unconstrained, which makes the \ac{MORD} representation attractive for
learning because no additional constraints on the intercepts $\#\theta \in
\Theta$ are needed.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection {Piece-wise ordinal regression classifier}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:PW-MORD}

The discriminative power of the ordinal classifier can be limiting in some cases.
Mapping the observations into higher dimensional space via usage of kernel
functions is one way to make the linear ordinal classifier more
discriminative. Though the ``kernalization'' of the ordinal classifier is
straightforward it is not suitable in all cases. For example, the kernels
are prohibitive in applications, which require processing of large amounts of
training examples and/or if a real-time response of the classifier is the
must. Instead, we proposed to stay in the original feature space where we
construct a combined classifier from a set of simpler component classifiers.
In our case, the component classifiers will be the \ac{MORD} classifiers, each
responsible for a subset of labels.

Let $Z> 1$ be a number of cut labels
$(\hat{y}_1,\hat{y}_2,\ldots,\hat{y}_{Z})\in\SY^{Z}$ such that
$\hat{y}_1=1$, $\hat{y}_{Z}=Y$ and $\hat{y}_z\leq
\hat{y}_{z+1}$, $z\in\SZ=\{1,\ldots,Z-1\}$. The cut labels define a partitioning
of $\SY$ into $Z$ subsets $\SY_z=\{y\in\SY\mid \hat{y}_z\leq y\leq
\hat{y}_{z+1}\}$, $z\in\SZ$. We will model a dependence between the observation
$\#x$ and a subset of labels $\SY_z$ by the component classifier
\begin{equation}
  \label{equ:ComponentClassifier}
   h_z(\#x) = \argmax_{y\in\SY_z} f_z(\#x,y) \:,
\end{equation}
where $f_z\colon \Re^n\times\SY_z\rightarrow\Re$ is a discriminant
function. We define a combined classifier whose discriminant function is composed of
discriminant functions of the component classifiers as follows
\begin{equation}
  \label{equ:CombinedClassifier}
   h''(\#x) = \argmax_{z\in\SZ}\max_{y\in\SY_z} f_z(\#x,y) \:.
\end{equation}
%
We set the discriminant functions to be
\begin{equation}
  \label{equ:comFceParticular}
   f_z(\#x,y) = \big \lz \#x, \#w_z(1-\alpha(y,z))+\#w_{z+1}\alpha(y,z)\big \pz +
   b_y \:,
\end{equation}
where
\[
   \alpha(y,z) = \frac{y-\hat{y}_z}{\hat{y}_{z+1}-\hat{y}_z} \:
\]
and $\#W=[\#w_1,\ldots,\#w_Z]\in\Re^{m}$, $\#b\in\Re^Y$, (where $m=n\times Z$) are
parameters. With these definitions, it can be claimed that: 
\begin{enumerate}
\item the component classifiers~\equ{equ:ComponentClassifier} are the ordinal classifiers,
\item the combined classifier~\equ{equ:CombinedClassifier} is well defined because all its
	neighboring discriminant functions are consistent at the cut labels,
	i.e. $f_z(\#x,\hat{y}_{z+1})=f_{z+1}(\#x,\hat{y}_{z+1})$, $z\in\SZ$, holds.
\end{enumerate}
The claim 1 is seen after substituting~\equ{equ:comFceParticular}
into \equ{equ:ComponentClassifier}, which after some algebra yields
\[
   h_z(\#x) = \argmax\limits_{y\in\SY_z} \Big ( \lz \#x, \#w_{z+1}-\#w_{z}\pz
   \alpha(y,z) +  b_y \Big ) \:.
\]
Since $\alpha(y,z)$ is linearly increasing with $y$, Theorem 1 guarantees
that $h_z(\#x)$ is the \ac{MORD} classifier equivalent to the ordinal classifier. The
claim 2 follows from the fact that $\alpha(\hat{y}_{z+1},z)=1$ and
$\alpha(\hat{y}_{z+1},z+1)=0$, and thus $f_z(\#x,\hat{y}_{z+1}) =
\lz\#x,\#w_{z+1}\pz + b_{\hat{y}_{z+1}} = f_{z+1}(\#x,\hat{y}_{z+1})$.

We can write explicitly the component classifier, which we call the \ac{PW-MORD}, as follows
\begin{equation}
  \label{equ:PW-MORD}
     h''(\#x, \#W,\#b) = \displaystyle\argmax_{z\in\SZ}\argmax_{y\in\SY_Z} \Big ( \lz \#x,
      \#w_z(1-\alpha(y,z))+\#w_{z+1}\alpha(y,z)\pz +  b_y \Big ) \:.
\end{equation}

Figure~\ref{fig:OrdAndPwOrd} visualizes the ordinal (=\ac{MORD}) and the \ac{PW-MORD} classifier on a
toy data. It is seen that the distribution of the data cannot be well described
by the ordinal classifier, while the \ac{PW-MORD} composed of three ordinal classifiers
provides much better model in this case.


\begin{figure}
\centering
\begin{tabular}{cc}
    ordinal classifier & \ac{PW-MORD} classifier \\
  \includegraphics[height=6.4cm]{./ordinal_classification/ordreg_example} &
  \includegraphics[height=6.4cm]{./ordinal_classification/pword_example}
\end{tabular}
\caption{The figure shows the partitioning of 2-dimensional feature space
  realized by the ordinal classifier and the \ac{PW-MORD} classifier with $Z=3$
  components. The cut labels for the \ac{PW-MORD} classifier were set to
  $\{1,4,7,10\}$. }
\label{fig:OrdAndPwOrd}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection {Unified view of classifiers for ordinal regression}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:UnifiedView}

In this section, we are going to describe several instances of the classifier
%
\begin{equation}
  \label{equ:GenClass}
   h(\#x,\#W,\#b) = \argmax_{y\in\SY} \Big ( \lz \#x,\sum_{z=1}^Z \beta(y,z) 
      \#w_z\pz + b_y \Big ) \:,
\end{equation}
%
where $\#W=[\#w_1,\ldots,\#w_Z]\in\Re^{n\times Z}$,
$\#b=[b_1;\ldots;b_Y]\in\Re^Y$ are parameters and $\beta\colon
\SY\times\{1,\ldots,Z\}\rightarrow\Re$ are fixed numbers, that can be useful models for
ordinal regression. The instances of~\equ{equ:GenClass} differ in the way how one defines $\beta$ and $\SZ$.
We show below how to derive various instances of the ordinal classifier.

\paragraph{1. Rounded linear-regression rule }
\begin{equation}
  \label{equ:LinReg}
    h(\#x,\#w,b) = \max(1, \min(Y, \rnd(\lz \#w,\#x\pz + b ) ))
\end{equation}
%
is the most simplest model for the ordinal regression obtained by clipping a
rounded response of the standard linear regression rule to the interval
$[1,Y]$. It is easy to show that~\equ{equ:LinReg} is an instance
of~\equ{equ:GenClass} recovered after setting $Z=1$, $\beta(1,y)=2y$, $y\in\SY$,
and fixing the components of the intercept vector $\#b$ to $b_y=2by-y^2$. Using
the conversion formula~\equ{equ:nondegeneratedMord2OrdRule}, we can show that the
rounded linear-regression rule is equivalent to the ordinal classifier with equal
width of the decision intervals, namely, with $\theta_{k+1}-\theta_k=2$, $k=1,\ldots,Y-2$.

\paragraph{2. Multi-class linear classifier}
\begin{equation}
  \label{equ:MLinClass}
    h(\#x,\#W,\#b) = \argmax_{y\in\SY} \big ( \lz \#w_y,\#x\pz + b_y \big )
\end{equation}
%
is recovered after setting $Z=Y$ and $\beta(y,z)=\leftbb y=z\rightbb$,
$y\in\SY$, $z\in\{1,\ldots,Z\}$. It is the most generic (and also most
discriminative) form of~\equ{equ:GenClass}, which completely ignores ordering of
the labels.

\paragraph{3. The proposed \ac{MORD}} classifier~\equ{equ:MordRule} is recovered after setting $Z=1$,
$\#W=\#w_1$, and $\beta(y,1)=y$, $y\in\SY$. We showed that the \ac{MORD} classifier
is equivalent to the standard ordinal classifier~\equ{equ:OrdRule} most frequently
used in the ordinal regression.

\paragraph{4. The proposed \ac{PW-MORD}} classifier~\equ{equ:PW-MORD} is recovered
after setting $\beta(y,z)$ according to
\begin{equation}
  \label{equ:BetaOfPwMord}
   \begin{array}{rcllll}
    \beta(y,z) & = & 1 - \alpha(y,z) &\quad&\mbox{for} & z=1,\ldots,Z-1\,, y\in\SY_z\:, \\
    \beta(y,z) &= &\alpha(y,z-1)     && \mbox{for} & z=2,\ldots,Z\,, y\in\SY_z\:, \\
    \beta(y,z) &= &0                 && \multicolumn{2}{l}{\mbox{otherwise.}}
  \end{array}
\end{equation}
%
The \ac{PW-MORD} is composed of $Z-1$ \ac{MORD} classifiers each modeling a subset of labels (see
Section~\ref{sec:PW-MORD}). The \ac{PW-MORD} is most flexible as it allows
 controling its complexity smoothly by a single parameter Z.  It is easy to see
that for $Z=2$ the \ac{PW-MORD} is equivalent to the \ac{MORD} (=ordinal) classifier. \
For $Z=Y$, it becomes the multi-class linear classifier.

To summarize, one can see \ac{PW-MORD} classifier as a classifier whose discriminative power varies from \ac{MORD} classifier ($Z=1$, labels are fully ordered) to multi-class linear classifier~\equ{equ:MLinClass} ($Z=Y$, no order of labels) depending on the number cutting labels $Z$.