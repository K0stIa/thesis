%!TEX root = ../thesis_main.tex

\FloatBarrier

\section {Generic cutting plane solver}
\label{sec:cpa}

The proposed method \ac{VILMA} translates learning into a convex optimization
problem~\equ{equ:genericIll} that can be re-written as
\begin{equation}
  \label{equ:masterProblem}
   (\#w^*,\#b^*) \in \Argmin_{\#w\in\Re^n,\#b\in\Re} \bigg [
   \frac{\lambda}{2}\|\#w\|^2 + \frac{1}{m}\sum_{i=1}^m \psi_I(\#w,\#b; \#x^i,y^i_l, y^i_r) \bigg ] \:.
\end{equation}
%Parameter $\#\xi$ denotes either $\#b$ or $\#\theta$ depending on what algorithm we consider, 
%e.g. \ac{VILMA}, \ac{II-SVOR-IMC}, \ac{II-SVOR-EXP}. $\#\Xi$ means $\Re^{Y}$ if by $\#\xi$ we assume
%$\#b$, $\#\Xi$ means $\Theta=\{\#\theta'\in\Re^{Y-1}\mid \theta_y'
%\leq \theta'_{y+1},\;y=1,\ldots,Y-1\}$ if by $\#\xi$ is assumed $\#\theta$. 
Note, we dropped regularization over  bias term $\#b$ in formulation~\equ{equ:masterProblem}.
The motivation for this comes from practical problems. Experiments in Section~\ref{sec:regularization_matters} show that
in case of high dimensional parameter vector $\#w$ and small number of classes the formulation~\equ{equ:masterProblem}
has an advantage over its possible alternative
\begin{equation}
  \label{equ:masterProblemFullReg}
   (\#w^*,\#b^*) \in \Argmin_{\#w\in\Re^n,\#b\in\Re} \bigg [
   \frac{\lambda}{2}(\|\#w\|^2  + \|\#b\|^2)  + \frac{1}{m}\sum_{i=1}^m \psi_I(\#w,\#b; \#x^i,y^i_l, y^i_r) \bigg ] \:.
\end{equation}

Therefore we concentrate our attention to problem~\equ{equ:masterProblem}.
 Of course, the task~\equ{equ:masterProblem} can be reformulated as a quadratic program with
$\SO(n+m+Y)$ variables and $\SO(Y\cdot m)$ constraints. However, generic off-the-shelf \ac{QP} solvers
are applicable only to small problems. Unlike problem~\equ{equ:masterProblemFullReg},
 we can not plug problem~\equ{equ:masterProblem} into~\ac{CPA} framework directly
due to the regularizer that operates only on part of variables to be optimized.
In this section, we derive the instance of
the \ac{CPA} tailored to the
problem~\equ{equ:masterProblem}. The resulting \ac{CPA} is applicable for large
problems and it provides a certificate of the optimality.

More details on the \ac{CPA} based solvers applied to the machine learning problems
can be found for example in~\cite{Teo-BMRM-2010,FraSonWer-CPA-MIT2012}. The
standard \ac{CPA} is suitable for solving convex tasks of the form
\begin{equation}
  \label{equ:cpaProblem}
    \#w^* \in \Argmin_{\#w\in\Re^n} F(\#w)\:, \quad\mbox{where}\quad F(\#w)=
    \frac{\lambda}{2}\|\#w\|^2 + G(\#w) 
\end{equation}
%
and $G\colon\Re^n\rightarrow\Re$ is a convex function. In contrast to our
problem~\equ{equ:masterProblem}, the objective of~\equ{equ:cpaProblem} contains
a quadratic regularization imposed on all variables. It is well known that the
\ac{CPA} applied directly to the un-regularized problem like~\equ{equ:masterProblem}
exhibits a strong zig-zag behavior leading to a large number of iterations. A
frequently used an ad-hoc solution is to impose an artificial regularization on
$\#b$, which may however significantly spoil the results as demonstrated in
section~\ref{sec:experiments}. In the rest of this section, we first outline the
\ac{CPA} algorithm for the problem~\equ{equ:cpaProblem} and then show how it can be
used to solve the problem~\equ{equ:masterProblem}.

The core idea of the \ac{CPA} is to approximate the solution of the master
problem~\equ{equ:cpaProblem} by solving a {\em reduced problem}
\begin{equation}
  \label{equ:reducedProblem}
   \#w_t \in \Argmin_{\#w\in\Re^n} F_t(\#w) \:, \quad\mbox{where}\quad F_t(\#w)=
   \frac{\lambda}{2}\|\#w\|^2 + G_t(\#w) \:.
\end{equation}
%
The reduced problem~\equ{equ:reducedProblem} is obtained
from~\equ{equ:cpaProblem} by substituting a cutting-plane model $G_t(\#w)$ for
the convex function $G(\#w)$ while the regularizer remains unchanged. The
cutting plane model of $G(\#w)$ reads
\begin{equation}
 \label{equ:CPmodel}
   G_t(\#w) = \max_{i=0,\ldots,t-1}\big [ G(\#w_i) + \lz G'(\#w_i),\#w-\#w_i\pz \big] \:,
\end{equation}
%
where $G'(\#w)\in\Re^n$ is a sub-gradient of $G$ at point $\#w$. Thanks to
the convexity of $G(\#w)$, $G_t(\#w)$ is a piece-wise linear underestimator of $G(\#w)$, which is
tight in the points $\#w_i$, $i=0,\ldots,t-1$. In turn, the reduced problem
objective $F_t(\#w)$ is an underestimator of $F(\#w)$. The cutting plane model
is build iteratively by the following simple procedure. Starting from
$\#w_0\in\Re^n$, the \ac{CPA} computes a new iterate $\#w_t$ by solving the reduced
problem~\equ{equ:reducedProblem}. In each iteration $t$, the cutting-plane
model~\equ{equ:CPmodel} is updated by a new cutting plane computed at the
intermediate solution $\#w_t$ leading to a progressively tighter approximation
of $F(\#w)$. The \ac{CPA} halts if the gap between $F(\#w_t)$ and $F_t(\#w_t)$ gets
below a prescribed $\veps>0$, meaning that $F(\#w_t)\leq F(\#w^*) + \veps$
holds. The \ac{CPA} is guaranteed to halt after $\SO(\frac{1}{\lambda\veps})$
iterations at most~\cite{Teo-BMRM-2010}.  The \ac{CPA} is outlined in
Algorithm~\ref{algo:CPA}.


\begin{algorithm}[t]
 \caption{Cutting Plane Algorithm}
% \begin{algorithmic}[1]
  \KwIn{$\veps > 0$, $\#w_0\in \Re^n$, $t \gets 0$}
  \KwOut{vector $\#w_t$ being $\veps$-precise solution of~\equ{equ:cpaProblem} }
  \Repeat{$F(\#w_t) - F_t(\#w_t) \leq \veps$}{
      $t \gets t + 1$ \\
      Compute $G(\#w_{t-1})$ and $G'(\#w_{t-1})$ \\
      Update the model $G_t(\#w) \gets \max_{i=0,\ldots,t-1} G(\#w_{i}) +\lz G'(\#w_{i}, \#w-\#w_{i}\pz$ \\
      Solve the reduced problem $\#w_t \gets \argmin_{\#w} F_t(\#w)$
      where $F_t(\#w) = \lambda \Omega(\#w) + R_t(\#w)$ \\
  }
% \end{algorithmic}
 \label{algo:CPA}
\end{algorithm}
%

We can convert our problem~\equ{equ:masterProblem} to~\equ{equ:cpaProblem}
by setting
\begin{equation}
\label{equ:defG}
  G(\#w) = R_{\rm emp}(\#w,\#b(\#w) ) \:, \quad\mbox{where}\quad \#b(\#w) \in
    \Argmin_{\#b\in\Re^Y} R_{\rm emp}(\#w,\#b) \:.
\end{equation}
%
%Here by $R_{\rm emp}(\#w,\#b)$, we denote correspondent convex surrogate loss for 
%the either of methods:~\ac{VILMA}, \ac{II-SVOR-IMC}, \ac{II-SVOR-EXP}.
It is clear that if $\#w^*$ is the solution of the problem~\equ{equ:cpaProblem}
with the function $G(\#w)$ defined by the equation~\equ{equ:defG}. Consequently, $(\#w^*,
\#b(\#w^*))$ must be a solution of~\equ{equ:masterProblem}. Because $R_{\rm
  emp}(\#w,\#b)$ is jointly convex in $\#w$ and $\#b$, the function $G(\#w)$
in~\equ{equ:defG} is also convex in $\#w$ (see for
example~\cite{Boyd:2004:CO:993483}). Hence, the application of
Algorithm~\ref{algo:CPA} to solve~\equ{equ:masterProblem} will preserve all
its convergence guarantees. To this end, we only need to provide the first-order
oracle computing $G(\#w)$ and the sub-gradient $\nabla G(\#w)$ required to
build the cutting plane model. For given $\#b(\#w)$, the subgradient of $G(\#w)$
reads~\cite{Boyd:2004:CO:993483}
\begin{equation}
  \label{equ:subgrad}
   \nabla G(\#w) = \frac{1}{m}\sum_{i=1}^m \#x^i (\hat{y}^i_l + \hat{y}^i_r- y_l^i-y_r^i)
\end{equation}
where 
\[
\begin{array}{rcl}
  \hat{y}^i_l &=& \displaystyle\argmax_{y\leq y^i_l} \big [ \ell(y,y_l^i) + \lz \#w,\#x^i\pz
  y - b_y(\#w) \big ]\:, \\
  \hat{y}^i_r &=& \displaystyle\argmax_{y\geq y^i_r} \big [ \ell(y,y_r^i) + \lz \#w,\#x^i\pz
  y - b_y(\#w) \big ] \:.
\end{array}
\]
%
The proposed \ac{CPA} transforms solving of the problem~\equ{equ:masterProblem} into a
sequence of two simpler problems:
\begin{enumerate}
\item The reduced problem~\equ{equ:reducedProblem} solved in each iteration of
  the \ac{CPA}. The problem~\equ{equ:reducedProblem} is a quadratic program that can
  be approached via its dual formulation~\cite{Teo-BMRM-2010} having only $t$
  variables where $t$ is the number of iterations of the \ac{CPA}. Since the \ac{CPA}
  rarely needs more than a few hundred iterations, the dual
  of~\equ{equ:reducedProblem} can be solved by off-the-shelf QP libraries.
\item The problem~\equ{equ:defG} providing $\#b(\#w)$, which is required to
  compute $G(\#w)=R_{\rm emp}(\#w,\#b(\#w) )$ and the sub-gradient $G'(\#w)$ via
  equation~\equ{equ:subgrad}. The problem~\equ{equ:defG} has only $\SO(Y)$ (the
  number of labels) variables. Hence it can be approached by generic convex
  solvers like the \ac{ACCPM} algorithm~\cite{ACCPM-COIN-OR}.
\end{enumerate}
%
We call the proposed solver as the {\em double-loop \ac{CPA}}, 
because we use another cutting plane method in the inner loop to implement the
first-order oracle.

Finally, we point out that the convex problems associated with the generic
\ac{SO-SVM} formulation~\equ{equ:RiskMinimization} can be solved by the same
solver. The only change is in using a different formulas for evaluating the risk
and its subgradient. The same holds for the convex problems associated with the
\ac{II-SVOR-EXP} and the \ac{II-SVOR-IMC}, in which case, however, we have to
use additional constraints $\#\theta\in\hat{\Theta}$ in~\equ{equ:masterProblem}
which propagate to the problem~\equ{equ:defG}.
