%!TEX root = ../thesis_main.tex

%\appendix
\section{Proof of Theorem~\ref{theorem:risk_consistency}}
\label{sec:AppendixConsistency}

In this section, we give detailed proofs mentioned in Section~\ref{sec:Result}. 
We start with showing positiveness of functions $H^p(\epsilon)$ and $H(\epsilon)$. 
To show this, we need to show first that the set of all conditional distributions $p(\#y, \#a \mid \#x)$ is a compact set.

\begin{lemma}
  For any $\#x \in \SX$ a set $\SP_{\#x}$ containing all distributions $p(\#y,\#a
  \mid \#x) =p(\#y \mid \#x) \, p(\#a \mid \#y, \#x) $ induced from a
  distribution $p(\#x,\#y,\#a)$ with the property A is a compact set.
\end{lemma}

\proof Using $p(\#y,\#a \mid \#x) =p(\#y \mid \#x) \, p(\#a \mid \#y, \#x) $,
\equ{equ:Pzcx} and~\equ{equ:Pycxa} we see that for any $\#x\in\SX$,
$\#p_{\#y\#a}(\#x)$ is a composition of functions with vector variables
$\#p_{\#y}(\#x)$ and $\#p_{\#a}(\#x)$, i.e. $\#p_{\#y\#a}(\#x) = \SF(\#p_{\#y}(\#x), \,
\#p_{\#z}(\#x))$. The function $\SF \colon \Delta_{|\SY^{\SV}|} \times
\Delta_{|\SZ^{\SV}|} \rightarrow \Delta_{|\SY^{\SV}| \times |\SA^{\SV}|}$
 is continuous on a compact set $\{ \#p_{\#y} (\#x) \in \Delta_{|\SY^{\SV}|} \mid
p(\#y \mid \#x) \ge \rho \} \times\{ \#p_{\#z} (\#x) \in \Delta_{|\SZ^{\SV}|} \mid
p(\#z \mid \#x) \ge \rho \}$. Thus, $\SP_{\#x}
\triangleq \{ \#p_{\#y\#a}(\#x) = \SF(\#p_{\#y}(\#x), \, \#p_{\#z}(\#x)) \mid p(\#y \mid \#x) \ge \rho, p(\#z \mid \#x) \ge \rho, \#p_{\#y} (\#x)
\in \Delta_{|\SY^{\SV}|}, \#p_{\#z}(\#x) \in
\Delta_{|\SZ^{\SV}|} \}$ is a compact set.  \eproof

\begin{lemma}
\label{emu:risk_continuous}

Functions $\min \limits_{\#t' \in \ST^{\SV}} \#p_{\#a}^\top \#\ell^p_{\#t'} $ and
$\min \limits_{\#t' \in \ST^{\SV}} \#p_{\#y}^\top \#\ell_{\#t'} $ are continuous
functions w.r.t. $\#p_{\#y\#a} \in \Delta_{|\SY^{\SV}| \times |\SA^{\SV}|}$.
\end{lemma}
\proof Since $p(\#y\mid\#x)=\sum_{\#a}p(\#y,\#a\mid\#x)$ and
$p(\#a\mid\#x)=\sum_{\#y}p(\#y,\#a\mid\#x)$ the functions $\#p_{\#y}$ and
$\#p_{\#a}$ are continuous functions of $\#p_{\#y\#a}$.
Hence, both functions $\min_{\#t' \in \ST^{\SV}} \#p_{\#a}^\top
\#\ell^p_{\#t'} $ and $\min_{\#t' \in \ST^{\SV}} \#p_{\#y}^\top \#\ell_{\#t'}
$ are continuous since each of them is a composition of minimum over set of
continuous functions.  \eproof

Now we are going to give a proof of positive of function $H^p(\epsilon)$ for any positive~$\epsilon$.

\begin{lemma}
  \label{lemma:H_ep}\ Let $H^p(\epsilon, \#p_{\#y\#a})\colon \Re \times
  \Delta_{|\SY^{\SV}| \times |\SA^{\SV}|} \rightarrow \Re$ be a function defined
  as follows

%\begin{equation}
%\label{equ:H_ep}
%\begin{array}{lcr}
%H^p(\epsilon, \#p_{\#y\#a}) & = & \min \limits_{\#t \in \ST^{\SV}} \#p_{\#a} \#\ell^p_{\#t} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#a} \#\ell^p_{\#t'}  \\ 
%& \mbox{s.t.} & \#p_{\#y} \#\ell_{\#t} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y} \#\ell_{\#t'}  \ge \epsilon \:,
%\end{array}
%\end{equation}

\begin{equation*}
\label{equ:Hp_ep}
\begin{aligned}
& \underset{\#t \in \mathcal{T}^{\mathcal{V}}}{\text{minimize}}
& & \#{p_a}^\top \#\ell^p_{\#t} - \min_{\#t' \in \ST^{\SV}}  \#{p_a}^\top \#\ell^p_{\#t'} \\
& \text{subject to}
& &  \#{p_y}^\top \#\ell_{\#t} - \min_{\#t' \in \ST^{\SV}}  \#{p_y}^\top \#\ell_{\#t'}  \geq \epsilon.
\end{aligned}
\end{equation*}
where loss functions $\ell(\#y, \#t)$ and $\ell^p(\#a, \#t)$ are defined by~\equ{equ:CompleteLoss},~\equ{equ:PartialLoss}.
Then for any compact subset $\SP \subseteq \Delta_{|\SY^{\SV}| \times
  |\SA^{\SV}|}$ and for any $\epsilon > 0$ there exists $ \delta > 0$ such that
$ \forall \#p_{\#y\#a} \in \SP $ holds $ H^p(\epsilon, \#p_{\#y\#a}) > \delta$,
i.e. $H^p(\epsilon) = \inf_{\#p_{\#y\#a} \in \SP}  H^p(\epsilon, \#p_{\#y\#a})
 > \delta$.
\end{lemma}

\proof 
We prove the lemma by contradiction. Assume that~\equ{equ:H_ep} does not hold,
then $\exists \epsilon > 0$, and a sequence $(\#t^m, \#p_{\#y\#a}^m)$ with $\#t^m \in
\ST^{\SV} $ and $\#p_{\#y\#a}^m \in \SP$ such that $ \#p_{\#y}^{m\: T} \#\ell_{\#t^m} - \min
\limits_{\#t' \in \ST^{\SV}} \#p^{m\: T}_{\#y} \#\ell_{\#t'} \ge \epsilon$ and $ \lim
\limits_{m \rightarrow \infty} \#p_{\#a}^m \#\ell^p_{\#t^m} - \min \limits_{\#t'
  \in \ST^{\SV}} \#p^m_{\#a} \#\ell^p_{\#t'} = 0$. Since $\SP$ is compact, we
can choose sub-sequence (which we still denoted as a whole sequence for
simplicity) such that $\lim \limits_{m \rightarrow \infty} \#p_{\#y\#a}^m = \#p_{\#y\#a}^* \in
\SP$. Hence, from lemma~\equ{emu:risk_continuous} it follows that $ \lim
\limits_{m \rightarrow \infty} \#p_{\#a}^m \#\ell^p_{\#t^m} - \min \limits_{\#t'
  \in \ST^{\SV}} \#p^*_{\#a} \#\ell^p_{\#t'} = 0$ and $ \lim \limits_{m
  \rightarrow \infty} \#p_{\#y}^m \#\ell_{\#t^m} - \min \limits_{\#t' \in
  \ST^{\SV}} \#p^*_{\#y} \#\ell_{\#t'} \ge \epsilon$. Sequence $(\#t^m)$ consists
of elements from the exponentially large but a finite set. Therefore there exists
element of sequence $\#t^* \in \ST^{\SV}$ such that the sequence contains infinite
number of copies of $\#t^*$. Let us choose this subsequence (which we again
denoted as a whole sequence) such that $\lim \limits_{m \rightarrow \infty}
\#t^m = \#t^*$. Note that $\lim \limits_{m \rightarrow \infty} \#p_{\#y\#a}^m = \#p_{\#y\#a}^*$
stays same. It follows that $ \#p_{\#a}^* \#\ell^p_{\#t^*} - \min
\limits_{\#t' \in \ST^{\SV}} \#p^*_{\#a} \#\ell^p_{\#t'} = 0$ and $ \#p_{\#y}^*
\#\ell_{\#t^*} - \min \limits_{\#t' \in \ST^{\SV}} \#p^*_{\#y} \#\ell_{\#t'} \ge
\epsilon,\ \epsilon > 0$. We have thus obtained the contradiction, i.e. we have found a model
$\#p^* \in \SP$ for which lemma~\equ{theorem:PlainPartLoss} does not hold.
\eproof

\begin{lemma}
\label{lemma:corollary26-Hp}
If $\forall \epsilon > 0, H^p(\epsilon) \triangleq \inf \limits_{\#p_{\#a \#y} \in \SP_{\#x}} H^p(\epsilon, \#p_{\#a \#y}) >
0$ for the loss functions $\ell(\#y, \#t)$ and $\ell^p(\#a, \#t)$ defined by~\equ{equ:CompleteLoss},~\equ{equ:PartialLoss}
 then there exists a nonnegative concave function $\xi \colon \Re \rightarrow
\Re_{+}$, right continuous at $0$ with $\xi(0) = 0$, such that $ \forall \ \#h
\colon \SX \rightarrow \ST^{\SV}$ 
and for all distributions with property A it holds that
\[
\begin{split}
\SE_{p(\#x)}  \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \SE_{p(\#x)} \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}(\#x)^\top \#\ell_{\#t'}   \le \\
\xi \left( \SE_{p(\#x)}  \#p_{\#a}(\#x)^\top \#\ell^p_{\#h(\#x)} - \SE_{p(\#x)} \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#a}(\#x)^\top \#\ell^p_{\#t'}  \right)\:.
\end{split}
\]
\end{lemma}

The main idea of Lemma~\ref{lemma:corollary26-Hp} proof is analogical to the proof of Corollary 26 in~\cite{Zhang04a}. 
Thus, we provide proof only for Lemma~\ref{lemma:corollary26-Hp} together with two auxiliary lemmas needed for its proof 
and proof of ``flipped'' version of this lemma we leave to the reader.

\begin{lemma}
\label{lemma:Jensen}
Let $\mu(\epsilon) \colon \Re \rightarrow \Re_+$ be a convex function  such that $\mu(\epsilon) \le H^p(\epsilon)$. Then for any classifier $\#h(\#x) \colon \SX \rightarrow \ST$ we have 
\[ 
\begin{split}
\mu(\SE_{p(\#x)}  \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \SE_{p(\#x)} \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}(\#x)^\top \#\ell_{\#t'}   )  \le \\
 \SE_{p(\#x)}  \#p_{\#a}(\#x)^\top \#\ell^p_{\#h(\#x)} - \SE_{p(\#x)} \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#a}(\#x)^\top \#\ell^p_{\#t'} 
\end{split}
\]
\end{lemma}

\proof
Using Jensen's inequality together with inequality 
\[
H^p(\#p_{\#y}^\top \#\ell_{\#t} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}^\top \#\ell_{\#t'} ) \le
\#p_{\#a}^\top \#\ell^p_{\#t} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#a}^\top \#\ell^p_{\#t'} 
\] 
we have
\[
\begin{split}
 \mu(\SE_{p(\#x)}  \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \SE_{p(\#x)} \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}(\#x)^\top \#\ell_{\#t'}   )  \le \\
\SE_{p(\#x)}   \mu(  \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}(\#x)^\top \#\ell_{\#t'}   ) \le \\
\SE_{p(\#x)}   H^p(  \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#y}(\#x)^\top \#\ell_{\#t'}   ) \le \\
 \SE_{p(\#x)}  ( \#p_{\#a}(\#x)^\top \#\ell^p_{\#h(\#x)} - \min \limits_{\#t' \in \ST^{\SV}}   \#p_{\#a}(\#x)^\top \#\ell^p_{\#t'}  ) \:.
 \end{split}
\]
\eproof

\begin{lemma}
\label{lema:proposition_25}
Let $\zeta_*(\epsilon) = \sup \limits_{a \ge0, b} \{ a\epsilon + b \mid \forall z \ge 0, az+b \le H^p(z) \}$, then $\zeta_*$ is a convex function. It has the following properties:
\begin{itemize}
\item $\zeta_*(\epsilon) \le H^p(\epsilon)$,
\item $\zeta_*(\epsilon)$ is non-decreasing,
\item for all convex functions $\zeta(\cdot)$ such that $\zeta (\epsilon) \le H^p(\epsilon)$, $\zeta(\epsilon) \le \zeta_*(\epsilon)$.
\item Assume that $\exists a > 0$ and $b \in \Re$ such that $a \epsilon + b \le H^p(\epsilon)$ and $\forall \epsilon > 0, H^p(\epsilon) > 0$. Then $\forall \epsilon > 0, \zeta_*(\epsilon) > 0$.
\end{itemize}
\end{lemma}

Lemma~\ref{lema:proposition_25} is a proposition 25 from~\cite{Zhang04a} for the function $H^p(\epsilon)$ Thus, we omit its proof here. Now we are ready to prove Lemma~\ref{lemma:corollary26-Hp}. 

\proof
Consider $\zeta_*(\epsilon)$ in Lemma~\ref{lema:proposition_25}, Let $\xi(\delta) = \sup \{ \epsilon \colon \epsilon \ge 0, \zeta_*(\epsilon) \le \delta \}$. Then $\zeta_*(\epsilon) \le \delta$ implies $\epsilon \le \xi(\delta)$. Therefore desired inequality comes from Lemma~\ref{lemma:Jensen}. 

Given $\delta_1, \delta_2 \ge 0$ : from $\zeta_*(\frac{\xi(\delta_1) + \xi(\delta_2)}{2}) \le \frac{\delta_1 + \delta_2}{2}$ we know that $\frac{\xi(\delta_1) +\xi( \delta_2)}{2} \le \xi(\frac{\delta_1 + \delta_2}{2})$. Thus, $\xi(\epsilon)$ is concave function.

We now only need to show that $\xi(\epsilon)$ is continuous at $0$. From the boundedness of $\ell(\#y, \#t)$, we know that $H^p(z) = + \infty$ when $z > \max \limits_{\#y \in \SY^{\SV}, \#t \in \ST^{\SV}} \ell(\#y, \#t)$. Therefore $\exists a > 0$ and $b \in \Re$ such that $a \epsilon + b \le H^p(\epsilon)$. Now we pick up any $\epsilon' > 0$, and let $\delta' = \frac{ \zeta_*(\epsilon')} {2}$. We know from Lemma~\ref{lema:proposition_25} that $\delta' > 0$. This implies that $\xi(\delta) < \epsilon'$ when $\delta' < \delta$.
\eproof

Here we just give formulation of ``flipped'' version of Lemma~\ref{lemma:corollary26-Hp} and its auxiliary Lemma~\ref{lemma:Hp_ep}. 
To prove Lemma~\ref{lemma:corollary26-H}, we need modified Lemma~\ref{lemma:Jensen} and~\ref{lema:proposition_25} for the function from Lemma~\ref{lemma:Hp_ep} which is straightforward to do, thus we leave it for the reader.

\begin{lemma}
  \label{lemma:Hp_ep}\ Let $H(\epsilon, \#p_{\#y\#a}) \colon \Re \times
  \Delta_{|\SY^{\SV}| \times |\SA^{\SV}|} \rightarrow \Re$ be a function defined
  as follows
 
\begin{equation*}
\label{equ:H_ep}
\begin{aligned}
& \underset{\#t \in \mathcal{T}^{\mathcal{V}}}{\text{minimize}}
& &  \#{p_y}^\top \#\ell_{\#t} - \min_{\#t' \in \ST^{\SV}}  \#{p_y}^\top \#\ell_{\#t'}  \\
& \text{subject to}
& &  \#{p_a}^\top \#\ell^p_{\#t} - \min_{\#t' \in \ST^{\SV}}  \#{p_a}^\top \#\ell^p_{\#t'} \geq \epsilon.
\end{aligned}
\end{equation*}
where loss functions $\ell(\#y, \#t)$ and $\ell^p(\#a, \#t)$ are defined by~\equ{equ:CompleteLoss},~\equ{equ:PartialLoss}.
Then for any compact subset $\SP \subseteq \Delta_{|\SY^{\SV}| \times |\SA^{\SV}|}$ and for any $\epsilon > 0$ there exists $ \delta > 0$ such that $ \forall \#p_{\#y\#a} \in \SP $ holds $ H(\epsilon, \#p_{\#y\#a}) > \delta$, i.e. $H(\epsilon) = \inf \limits_{\#p_{\#y\#a} \in \SP} H(\epsilon, \#p_{\#y\#a}) > \delta$.
\end{lemma}

\proof
The proof is analogous to the proof of Lemma~\ref{equ:Hp_ep}.
\eproof


\begin{lemma}
\label{lemma:corollary26-H}
If $\forall \epsilon > 0, H(\epsilon) \triangleq \inf \limits_{\#p_{\#a \#y} \in \SP_{\#x}} H(\epsilon, \#p_{\#a \#y}) >
0$ for the loss functions $\ell(\#y, \#t)$ and $\ell^p(\#a, \#t)$ defined by~\equ{equ:CompleteLoss},~\equ{equ:PartialLoss}
then there exists a nonnegative concave function $\zeta \colon \Re \rightarrow
\Re_{+}$, right continuous at $0$ with $\zeta(0) = 0$, such that $ \forall \ \#h
\colon \SX \rightarrow \ST^{\SV}$
and for all distributions with property A it holds that
\[
\begin{split}
\SE_{p(\#x)} \#p_{\#a}(\#x)^\top \#\ell^p_{\#h(\#x)} - \SE_{p(\#x)} \min
\limits_{\#t' \in \ST^{\SV}} \#p_{\#a}(\#x)^\top \#\ell^p_{\#t'} \le  \\
\zeta \left( \SE_{p(\#x)} \#p_{\#y}(\#x)^\top \#\ell_{\#h(\#x)} - \SE_{p(\#x)} \min
  \limits_{\#t' \in \ST^{\SV}} \#p_{\#y}(\#x)^\top \#\ell_{\#t'} \right) \:.
\end{split}
\]
\end{lemma}

Proof of Lemma~\ref{lemma:corollary26-H} is similar to proof of Lemma~\ref{lemma:corollary26-Hp}. 