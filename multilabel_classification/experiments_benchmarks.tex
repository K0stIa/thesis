%!TEX root = ../thesis_main.tex

%\section{Experiments}

%In this section we empirically show empirical justification of considered partial risk $\Delta^{pp}(\#a, \#y)$. in Section~\ref{sec:OCRBenchmark} we present %experiment on standard OCR data taken from [?]

\subsection{Classification models}
\label{sec:ExpBenchmark}

In our experiments we learn a pair-wise Markov Network classifiers 
%that are Pair-wise Markov network classifiers with log-linear parameters
\begin{equation}
  \label{equ:MaxSumClassifier} 
  \#h(\#x; \#w) \in  \Argmax \limits_{\#y \in \SY} \Big (\sum \limits_{t \in
    \ST} \lz \#w_t, \Psi_t(\#x, y_t) \pz + \sum \limits_{tt' \in \mathcal{E}}
  \lz \#w_{tt'}, \Psi_{tt'} (y_t, y_{t'}) \pz \Big ),
\end{equation}
%
where $(\ST,\mathcal{E})$ is a given graph specifying neighborhood structure of
interrelated labels, $\#\Psi_t\colon\SX\times\SY_t\rightarrow\Re^{n_u}$,
$\#\Psi_{tt'}\colon\SY_t\times\SY_{t'}\rightarrow\Re^{n_{p}}$ are unary and
pairwise features, respectively, and $\#w\in\Re^n$ is a concatenation of unary
and pairwise parameters $\#w_t$, $\#w_{tt'}$. We constrain ourselves to acyclic
graphs $(\ST,\mathcal{E})$ which allow to solve the inference
problem~\equ{equ:MaxSumClassifier} by the dynamic programming. It is seen
that~\equ{equ:MaxSumClassifier} is an instance of the linear
classifier~\equ{equ:LinClassif} whose parameters can be learned by the methods
analyzed in this paper. 

We evaluate the following classificaion models:
\begin{enumerate}\itemsep=0pt
\item [SO-SVM] Learning of the MN classifier~\equ{equ:MaxSumClassifier} by
  solving the supervised SO-SVM problem~\equ{equ:SOSVMLambda}. This method
  requires completely annotated examples $\{(\#x^1,\#y^1)$, $\ldots$,
  $(\#x^m,\#y^m)\}$. We use an implementation of the Bundle Method for Risk
  Minimization~\cite{Teo-2007-BMRM} to find $\veps$-optimal solution of the
  convex problem~\equ{equ:SOSVMLambda}. 
\item [Partial-SO-SVM] Learning the MN classifier~\equ{equ:MaxSumClassifier} by
  solving the Partial-SO-SVM problem~\equ{equ:PartialSO-SVM}. This method is
  applicable for learning from the partially annotated examples
  $\{(\#x^1,\#a^1),\ldots,(\#x^m,\#a^m)\}$. 
%
\item [Flat-SVM] Flat (unstructured) multi-class SVM classifiers
  \[ 
     h_t(\#x;\#w_t) \in \Argmax_{y\in\SY_t} \lz \#w_t,\#\Psi_t(\#x,y)\pz\,, \qquad
     t\in\ST\:,
  \]
  learned independently by supervised multi-class SVM algorithm. The method is
  applicable for the partially annotated examples
  $\{(\#x^1,\#a^1),\ldots,(\#x^m,\#a^m)\}$. The completely annotated examples
  for the $t$-th classifier are defined as $\{(\#x^i,y_t^i) \mid i\in \SI_t\}$
  where $\SI_t=\{ i\in \{1,\ldots m\} \mid a_t^i \neq \flipedMathQuestion\}$
  contains indices of those partially annotated examples whose $t$-th local part
  is annotated. It is interesting to note that learning of flat SVM classifiers
  from $\{(\#x^i,y_t^i) \mid i\in \SI_t\}$ is equivalent to solving the
  Partial-SO-SVM problem~\equ{equ:PartialSO-SVM} with the parameters
  $\#w_{tt'}$, $tt'\in\mathcal{E}$, of the pair-wise potentials fixed to zero.
\end{enumerate}

We optimize the non-convex Partial-SO-SVM problem~\equ{equ:PartialSO-SVM} by the
CCCP Algorithm~\ref{algo:GenericCCCP} initialized from labeling
$\{\hat{\#y}^1,\ldots,\hat{\#y}^m \}$ generated by the following three
ways:
\begin{enumerate}\itemsep=0pt
\item [CCCP-CA] CCCP initialized from the complete annotation,
  i.e. $\hat{\#y}^i = \#y^i$, $\forall i$, holds in the first iteration of the
  CCCP. Note, that this is not a method to be used in practice as the complete
  labeling is not accessible in general. We use this method to get the best estimate of the
  optimal solution of~\equ{equ:PartialSO-SVM}.
\item [CCCP-FC] CCCP initialized from the responses of the Flat-SVM classifier,
  i.e. $\hat{y}_t^i = h_t(\#x^i;\#w_t)$, $t\in\{t'\in\ST\mid
  a_t^i=\flipedMathQuestion\}$ and $\hat{y}^i_t = a^i_t$, $t\in\{t'\in\ST\mid
  a_t^i\neq\flipedMathQuestion\}$. This is a practical method applicable for the
  Markov-Network structured classifier whose unary potentials define the score
  functions of the flat classifiers.
\item [CCCP-RD] CCCP initialized from randomly generated labels. This
  approach is most frequently used in practice, e.g. in the reference
  papers~\cite{Lou-SLfromPartAnnot-ICML2012,Li-StructCandidate-ECML2013}.
\end{enumerate}

\begin{comment}
Namely, the parameters $\#w\in\Re^n$ are learned from
example set $\{(\#x^1,\#a^1),\ldots,(\#x^m,\#a^m)\}$ by solving
\begin{equation}
     \label{equ:ConvexPartSOSVM}
      \hat{\#w} \ass \min_{\#w\in\Re^n} F(\#w) \ass \Big ( 
        \frac{\lambda}{2}\|\#w\|^2 + \frac{1}{m}\sum_{i=1}^m
        \ell(\#x^i,\#a^i,\SY^i,\#w) \Big )
\end{equation}
where 
\[
\ell(\#x^i,\#a^i,\SY^i,\#w) = \max_{\#y\in\SY} \Big ( \Delta^p(\#y, \#a^i) +
\lz \#w,\#\Psi(\#x^i,\#y)\pz \Big )- \max_{\#y'\in \SY^i} \lz
\#w,\#\Psi(\#x^i,\#y') \pz\:.
\]
%
We benchmark the following four instances of a generic learning
algorithm~\equ{equ:ConvexPartSOSVM}:
%
\begin{enumerate}
\item [SO-SVM] Supervised (standard) SO-SVM algorithm obtained
  from~\equ{equ:ConvexPartSOSVM} when $\#a^i=\#y^i$, $\SY^i=\{\#y^i\}$ where
  $\#y^i\in\SY$. In this case~\equ{equ:ConvexPartSOSVM} is a convex problem
  which can be solved with a guaranteed precision. Namely, we use an
  implementation of the Bundle Method for Risk Minimization [Teo].
\item [Partial-SO-SVM-FA] The SO-SVM with the margin-rescaling partial loss for
  training from partially annotated examples, i.e. $\#a^i\in\SA$ where the set
  of partial annotations $\SA$ is defined by~\equ{equ:SetPartAnnot} and
  $\SY^i=\SY(\#a^i)$ is a set of labellings consistent with the annotation
  $\#a^i$. In this case, ~\equ{equ:ConvexPartSOSVM} is a non-convex problem
  solved by the CCCP algorithm which is initialized from the full annotation
  (FA) $\#y^i$, i.e. $\hat{\#y}^i = \#y^i$ is set in the first iteration of the
  CCCP. Note, that this is not a method to be used in practice. It is a mean to
  get the best estimate of the optimal solution of~\equ{equ:ConvexPartSOSVM}.
\item [Partial-SO-SVM-RD] The same algorithm as Partial-SO-SVM-FA but started
  from a random guess of $\hat{\#y}^i$. This optimization technique has been
  used in~\cite{Lou-SLfromPartAnnot-ICML2012,Li-StructCandidate-ECML2013}.
\item [Partial-SO-SVM-LC] The same algorithm as Partial-SO-SVM-FA but the
  started from labelling $\hat{\#y}^i=(\hat{y}^i_t \mid t\in\ST)$ with the
  missing labels estimated by a flat multi-class SVM classifier
  \[ 
     h_t(\#x^i;\#w_t) \in \Argmax_{y\in\SY_t} \lz \#w_t,\#\Psi_t(\#x,y)\pz\,, \qquad
     t\in\{t'\in\ST\mid a_t^i=\flipedMathQuestion\} \:.
  \]
  The $t$-th flat classifier $h_t(\#x^i;\#w_t)$ is learned by standard
  supervised SVM algorithm from examples $\{(\#x^i,y_t^i) \mid i\in \SI_t\}$
  where $\SI_t=\{ i\in \{1,\ldots m\} \mid a_t^i \neq \flipedMathQuestion\}$
  contains indices of those partially annotated examples whose $t$-th local part
  is annotated. It is interesting to note that training the flat SVM classifiers
  as described above is equivalent to solving the generic
  problem~\equ{equ:ConvexPartSOSVM} with the parameters $\#w_{tt'}$,
  $tt'\in\mathcal{E}$, corresponding to pair-wise potentials, fixed to zero.  We
  use a standalone flat SVM classifier, further abbreviated as {\bf Flat-SVM},
  as additional baseline for comparison.
\end{enumerate}
\end{comment}

%In our experiments we use adopted hamming loss
%\begin{equation}
%    \Delta^{p}(\#y,\hat{\#a}) = C \sum_{t\in\ST} \leftbb \hat{a}_t\neq \flipedMathQuestion \rightbb
%    \Delta_t(\hat{a}_t,y_t)
%\end{equation}

%In our experiments we consider applications with tractable inference task,
%eg. in our applications the graph structure of~\equ{equ:MaxSumClassifier} is
%chain or tree, in order to avoid possible issues with precision of inference
%task of classifier~\equ{equ:MaxSumClassifier} that appear when the inference
%task is not tractable
%
%The goal we pursued in experiments was to evaluate the quality of classifier learned from partially annotated data and from fully annotated data.

\subsection{Benchmark datasets}
\label{sec:benchmarks}

\paragraph{OCR on sequence of handwritten characters.}
We use OCR datasets introduced by~\cite{Taskar-M3N-NIPS2004} which has become
one of the most frequently used benchmark for structured output learning. The
dataset constains a set of 6877 hand-written words with average length of 8
characters collected form 150 human subjects. Each image depicting a single word
is divided into characters each of which rasterised into 16 by 6 binary image.
A complete annotation is a sequence of labels $\#y^i=(y^i_1,\dots,y^i_{T^i})$
where each label is one of 26 lower-case characters $\SY_t=\{'a',\dots,'z'\}$,
$t\in\ST$.

The neighborhood structure $(\ST,\mathcal{E})$ of the Markov-Network
classifier~\equ{equ:MaxSumClassifier} is set to be a chain. Each unary feature
$\#\Psi_t(\#x,y)\in\Re^{16\cdot 8\cdot 27}$ is split to $27$ slots all set to
zeros but $y$-th slot whose components are unnormalized pixel values
of the subimage of the $t$-th character in the input image $\#x$. The pair-wise
feature vector $\#\Psi_{tt'}(y_t,y_{t'})\in \Re^{27\cdot 27}$ is all zeros but a
single component set to one its position encodes the combination $(y_t,y_{t'})$.
All unary as well as pair-wise potentials share the same parameters,
i.e. $\#w_t=\#w_u$ and $\#w_{tt'}=\#w_p$ (so called homogeneous Markov
model). We have to learn $n=26\cdot 26 + 26\cdot 16\cdot 8=4004$
parameters in total. As the complete loss we use a normalized Hamming distance
\[
   \Delta(\#y,\#h(\#x)) = \frac{1}{|\ST|}\sum_{t\in\ST} \leftbb y_t \neq
   h_{t}(\#x) \rightbb
\]
i.e., the expected error $\SE_{p(\#x,\#y)}[\Delta(\#y,\#h(\#x))]$ corresponds to
an average number of misclassified characters in a randomly selected word.


%The graph structure behind Classifier~\equ{equ:MaxSumClassifier} is a chain. All
%pairwise interactions share the same parameters $\#w_{tt'}=\#w^p$ as well as
%unary part share same parameters $\#w_t=\#w^u$, eg. we use homogeneous
%model. Thus, the classifier has $26\times26 + 26\times16\times8=4004$ number of
%parameters in total. The we set the constant $C$ in the risk $\Delta^p(\#y,
%\hat{\#a})$ to $C=\frac{1}{T^i}$ where $T^i$ is a length of word $\#x^i$.


%In this experiment the image for each word
%corresponds to $\#x^i=(x^i_1,\dots,x^i_{T^i})$, a label of an individual
%character $x^i_t$ to $y^i_t$, and a labeling of complete word to
%$\#y^i=(y^i_1,\dots,y^i_{T^i})$. Each label takes values from one of 26 classes
%$\{'a',\dots,'z'\}$.

We generate several partially annotated example sets with increasing amount of
annotated labels. Specifically, the annotation mask $\#z$ is generated according
to $p(z_t=1 \mid \#x) = q/100$ with $q \in \{ 6, 12, 25, 50, 100\}$. The mask
$\#z$ together with the complete labeling $\#y$ then produces the partial
annotation $\#a = \alpha(\#y, \#z)$. For example, setting $q=12$ means that
approximately $12\%$ of randomly selected labels are annotated.
Figure~\ref{fig:ocr_example} shows examples of the generated partial
annotations.

%We consider several scenarios with different portion of annotated data. To get
%partial annotation for each word $\#x^i=(x^i_1,\dots,x^i_{T^i})$ we generate a
%random mask $\#z^i=(z^i_1,\dots,z^i_{T^i})$ so that $p(z^i_t=1 \mid \#x^i) =
%q/100$ where $q \in \{ 6, 12, 25, 50, 100\}$. Random mask $\#z^i$ together with
%labelling $\#y^i$ give a partial annotation $\#a^i = \alpha(\#y^i, \#z^i)$.

%Each generated example set is randomly partitioned into the training, validation
%and test part such that part contains approximately the same number of
%examples. The random partitioning was repeated 5 times. The measured
%statistics are the mean values and the standard deviation computed over the five
%random splits. The train and the validation sets contains the partial annotated
%pairs $(\#x, \#a)$ while test set always consists of the completely annotated
%pairs $(\#x, \#y)$.  The training set is used to learn the parameters $\#w$, the
%validation set is used to tune the regularization parameter from the range
%$\{10,1,0.1,0.01,0.001\}$ and the test set is used to get estimate of the
%expected risk.




%\begin{table}
%\scalebox{0.63} {
%\begin{tabular} {   cc }
%\begin{tabular} {   |c|c|c| }
%
%\hline
%$method$   &  $\hat{\#a}^i$ & $\hat{\SY}^i$  \\
%\hline
%SO-SVM    & $\#y^i$ & $\{\#y^i\}$  \\
%Patial-SO-SVM-Convex    & $\#a^i$ & $\{\#y^i\}$ \\
%Patial-SO-SVM-CCCP-GT    & $\#a^i$ & $\SY(\#a^i)$ \\
%Patial-SO-SVM-CCCP-LC & $\#a^i$ & $\SY(\#a^i)$ \\
%Patial-SO-SVM-CCCP-RD & $\#a^i$ & $\SY(\#a^i)$ \\
%Flat SVM  & $\#y_t^i$ & $\{\#y^i_t\}$ \\
%\hline
%\end{tabular}
%
%&

\begin{figure}
\begin{tabular}{cccc}
 \scalebox{1.2}{$\#x$} &
\includegraphics[width=0.26\linewidth] {multilabel_classification/ocr/anquish.pdf}  &	
\includegraphics[width=0.26\linewidth] {multilabel_classification/ocr/mbraces.pdf}  &
\includegraphics[width=0.26\linewidth] {multilabel_classification/ocr/olcanic.pdf}  \\
 \scalebox{1.2}{$\#y$} & \scalebox{1.6} {(a,n,q,u,i,s,h)} & \scalebox{1.6} {(m,b,r,a,c,e,s)} & \scalebox{1.6} {(o,l,c,a,n,i,c)}  \\
 \scalebox{1.2}{$\#z$} & \scalebox{1.5} {(0,1,1,0,1,1,0)} & \scalebox{1.5} {(1,1,1,1,0,0,1)} & \scalebox{1.5} {(1,0,0,0,1,1,1)}  \\
 \scalebox{1.2}{$\#a$} & \scalebox{1.6} {(?,n,q,?,i,s,?)} & \scalebox{1.6} {(m,b,r,a,?,?,s)} & \scalebox{1.6} {(o,?,?,?,n,i,c)} \\
\end{tabular}
\caption{Illustration of three examples selected from the OCR benchmark. The
  figure shows inputs $\#x$, complete labeling $\#y$, annotation mask $\#z$ and
  the partial annotation $\#a$.}
\label{fig:ocr_example}
\end{figure}

\paragraph{Facial landmark detection.}
%
We adopt the structured classifier proposed
in~\cite{Uricar-Franc-Hlavac-VISAPP-2012} designed for estimation of a set of
facial landmarks on an image of a human face. We use the Labeled
Faces in the Wild \todo[inline]{cite} containing 13,233 images of size $250\times 250$
pixels. The faces are manually annotated with positions of eight
landmarks: corners of the left and the right eyes, corners of the mouth, the tip
of the nose and the center of the face.

The classifier input $\#x\in\SX$ is $40$ by $40$ pixel gray-scale image obtained
by rescaling a box found by a commercial face detector. The complete labeling is
a sequence of landmark positions $\#y=(y_0,\ldots,y_7)$ with each position $y_t$
being from a fixed area $\SY_t\subset \{1,\ldots,40\}\times
\{1,\ldots,40\}$. The neighborhood structure $(\ST,\mathcal{E})$ of the
Markov-Network classifier~\equ{equ:MaxSumClassifier} is a tree whose edges
connect the related landmarks, see Figure~\ref{fig:Landmarks}(a). The unary
potentials $\lz \#w_t,\#\Psi(\#x,y)\pz$ measure how likely it is that the $t$-th
landmark is in the image $\#x$ at the position $y$. The feature vector
$\#\Phi_t(\#x,y)$ is a collection of Locally Binary Patterns computed in a
rectangular area around $y$. The pair-wise potential $\lz \#w_{tt'},
\Psi_{tt'}(y_t, y_{t'}) \pz$ is a quadratic deformation cost evaluating a
relative positions of the neighbouring landmarks $t$ and $t'$, $\{ t, t' \} \in
\mathcal{E}$. The structured classifier has $n=232,476$ parameters in total and
its performance is measured by the loss
\[
   \Delta(\#y,\#h(\#x)) = \frac{100}{|\ST| \cdot s(\#y)}\sum_{t\in\ST} \| y_t - h_t(\#x)\| \:,
\]
where $s(\#y)$ is a size of the face defined as the distance from the center of
the mouth to the mid point between the eyes. I.e., the expected risk
$\SE_{p(\#x,\#y)}[\Delta(\#y,\#h(\#x))]$ is an average deviation of the
estimated and the actual landmark position measured in percents of the face
size. For more details we refer to the original paper~\cite{Uricar-Franc-Hlavac-VISAPP-2012}.

%configuration of $M$ landmarks is described by graph $\SG = (\ST, \mathcal{E})$,
%where $\ST = \{ 0,\dots, M-1\}$ is a set of landmarks and $\mathcal{E} \subseteq
%\ST^2$ is a set of edges defining the neighbouring landmarks, see Table 2. In
%our experiments $H = W = 40$, $M=8$. Each landmark $t \in \ST$ has its set of
%possible positions $\SY_t \subset \{ 1,\dots,H\} \times \{ 1,\dots,W\}$ within
%image $\#x \in \SX$. The quality of landmark configuration for fixed parameters
%$\#w$ given input image $\#x \in \SX$ is measured by scoring function of
%classifier~\equ{equ:MaxSumClassifier}. The first term $\lz \#w_{t},
%\Psi_{t}(\#x, \#y) \pz$ of~\equ{equ:MaxSumClassifier} corresponds to a local
%appearance model evaluating match between landmarks positions $\#y$ and the
%input image $\#x$, affine transform and described by pyramid-of-LBP
%descriptor~\cite{Uricar-Franc-Hlavac-VISAPP-2012}. The second term $\lz
%\#w_{tt'}, \Psi_{tt'}(\#x, \#y) \pz$ is the deformation cost evaluating the
%relative positions of the neighbouring landmarks $t$ and $t'$, $\{ t, t' \} \in
%\mathcal{E}$. The graph structure of classifier~\equ{equ:MaxSumClassifier} is a
%tree (see Fig HERE).

We randomly generated two different sets of partially annotated examples
with different amount of annotated labels:

\medskip
\begin{tabular}{lll}
  Data set 1 ($56\%$ annotated labels) && Data set 2 ($34\%$ annotated labels)\\
\begin{tabular}{c|c}
 $\#z$                & $p(\#z|\#x)$ \\
 \hline 
 $(1,1,1,0,0,1,1,0)$  & $0.5$ \\
 $(1,0,0,1,1,0,0,1)$  & $0.5$ \\
 \multicolumn{2}{c}{} \\
 \multicolumn{2}{c}{} \\
\end{tabular}
& &
\begin{tabular}{c|c}
 $\#z$                & $p(\#z|\#x)$ \\
 \hline 
 $(0,1,0,0,0,1,0,0)$  & $0.25$ \\
 $(0,0,1,0,0,0,1,0)$  & $0.25$ \\
 $(1,0,0,0,0,0,0,1)$  & $0.25$ \\
 $(1,0,0,1,1,0,0,0)$  & $0.25$ \\
\end{tabular}

\end{tabular}


\medskip
\noindent
In the data set 1, approximately $50\%$ of faces has annotated landmarks in
the upper part of the face (centers of the eyes and the face center) while
$50\%$ has annotated lower part of the face (the corners of the mouth, the nose and
the face center). In data set 2, approximately $25\%$ of faces has annotation of
the corners of the left eye, $25\%$ has annotation of the corners of the right
eye, $25\%$ has annotation of the nose and the face center and $25\%$ has
annotation of the corners of the mouth and the face center.
Figure~\ref{fig:Landmarks} shows the numbering of the local parts as well as
examples of the partial annotations.

%The classifier has XXX number of parameters in total. The we set the constant
%$C$ in the risk $\Delta^p(\#y, \hat{\#a})$ as
%in~\cite{Uricar-Franc-Hlavac-VISAPP-2012}.

The training examples were randomly partitioned into training, validation and
test part. For the OCR benchmark the ratio was $30\%/30\%/30\%$ as
in~\cite{Taskar-M3N-NIPS2004}. In the case of the Landmarks, the ratio was $60\%/20\%/20\%$ as
in~\cite{Uricar-Franc-Hlavac-VISAPP-2012}. For both benchmarks the random
partitioning was repeated 5 times. 
%We report the mean values and the standard
%deviations computed over the five random splits. 
The training and the validation sets contain the partial annotated pairs $(\#x,
\#a)$ while the test set always contains the completely annotated pairs $(\#x,
\#y)$. For one random split we learn the parameters $\#w$ from the training
examples for a range of regularization parameters 
$\{10,1,0.1,0.01,0.001\}$. The best $\lambda$ is then selected based on the average
partial loss evaluated on the validation examples. 
%The test risk is computed on
%the test part of the examples untouched during the training and validation
%stage. 
The procedure is repeated five times and the reported values are the mean values
and the standard deviations of the loss evaluated on the test splits.




%The validation part is used to tune
%the regularization constant. The reported results are averages and standard
%deviations of test errors computed over 5 splits. For the train and validation
%sets we use a set of pairs $(\#x^i, \#a^i)$ while test set always consists of
%completely annotated pairs $(\#x^i, \#y^i)$.

\begin{comment}
  \caption{SO-SVM standard loss for supervised examples. SO-SVM-PartCvx partial
loss with convex reward term. CCCP-*** minimizes partial loss with non-convex
reward term. CCCP-GT is initialized from groundtruth annotation. CCCP-LC is
initialize from local detectors. CCCP-Rand is initialized randomly.
}
\end{comment}

\begin{comment}
\begin{table}
\begin{center}
\scalebox{0.75} {
  \begin{tabular}{|l|c|c|}
   \hline
              & 100$\%$             & 56$\%$ \\
   \hline
    SO-SVM          &  $5.4225 \pm 0.0611$  & $5.6206 \pm 0.0546$  \\
%    Patial-SO-SVM-Convex  &  $5.4225 \pm 0.0611$  & $5.4936 \pm 0.0403$ \\
    Patial-SO-SVM-CCCP-GT         &  $5.4349 \pm 0.0481$  & $5.4984 \pm 0.0306$ \\
    Patial-SO-SVM-CCCP-LC         &  NA                   & $5.5395 \pm 0.0432$  \\
    Patial-SO-SVM-CCCP-RD       &  NA                   & $5.5561 \pm 0.0360$  \\
    Flat SVM        &  $6.9039 \pm 0.0986$  & $7.2167 \pm 0.1204$  \\ 
   \hline
  \end{tabular}
  }
\end{center}
\end{table}
\end{comment}


\begin{center}

\begin{figure}
\begin{tabular}{ccc}
\includegraphics[width=0.38\linewidth] {multilabel_classification/graph_constraints8l.pdf}  &	
%\includegraphics[width=0.3\linewidth] {img/halleBerryBwDetected.pdf} 
\includegraphics[width=0.28\linewidth] {multilabel_classification/halleBerry_half.pdf} &
\includegraphics[width=0.28\linewidth] {multilabel_classification/Lucy_Liu_bottom.pdf} \\
 (a) & (b) & (c)
\end{tabular}
\caption{Figure (a) shows numbering of the local parts (landmarks) and the neighborhood graph $(\ST,\SV)$ of the
  Markov-Network classifier used for landmark detection. Figure (b) and (c)
  shows an example of the partially annotated face corresponding to the
  annotation mask $(1,1,1,0,0,1,1,0)$ and $(1,0,0,1,1,0,0,1)$, respectively. }
\label{fig:Landmarks}
\end{figure}
\end{center}
