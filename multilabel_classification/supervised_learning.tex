%!TEX root = ../thesis_main.tex

\section {Supervised learning of structured output classifiers}
\label{sec:supervised}


Let $\SX$ be an input space, $\SV$ a finite set of local parts and $\SY$ a
finite set of labels. An object is fully characterized by an input (observation)
$\#x\in\SX$ and a labelling $\#y=(y_v\in\SY\mid v\in\SV)$ of local parts
$\SV$. In the supervised setting, we are given the training set
\[
  \SD^m_{\#x\#y}=\{(\#x^1,\#y^1),\ldots,(\#x^m,\#y^m)\}\in(\SX\times \SY^\SV)^m 
\]
drawn from i.i.d. random variables with distribution $p(\#x,\#y)$ defined over
$\SX\times\SY^\SV$. We want to design a decision function
$\#h\colon\SX\rightarrow\ST^\SV$, which maps an input $\#x\in\SX$ to a vector of
decisions $\#t=(t_v\in\ST\mid v\in\SV)\in\ST^\SV$. We assume that the decision
set $\ST$ for each local part is finite. For example, in the most typical
setting $\ST=\SY$ and $\#h$ is the structured output classifier predicting
directly the labels. Note that $\ST$ can be different from $\SY$ in general. For
example, in the case of the classification with the reject option $\ST=\SY
\cup \{\mbox{don't know}\}$.


%\subsection{Empirical risk minimization based learning of structured output classifiers}
%\label{sec:supervised}


Let $\ell \colon \SY^\SV\times\ST^\SV\rightarrow\Re_+$ be a given loss function
assigning a non-negative number to each pair of labelling $\#y\in\SY^\SV$ and a
decision $\#t\in\ST^\SV$. We confine ourselves to losses additive over the local
parts which is a natural choice in many applications, i.e.
\begin{equation}
  \label{equ:CompleteLoss}
   \ell(\#y,\#t) = \sum_{v\in\SV} \ell_v(y_v,t_v) \:,
\end{equation}
where $\ell_v\colon \SY\times\ST\rightarrow\Re_+$, $v\in\SV$, are single label
losses. We assume that $\ell_v$ are bounded and
non-trivial, i.e. $\ell_v(y,t) < \infty$ and $\forall y \: \exists t$ such that
$\ell_v(y,t)> 0$. An example of a frequently used additive loss is the Hamming
loss obtained when $\ST=\SY$ and $\ell_v(y_v,t_v)=\leftbb y_v\neq
t_v\rightbb$, $v\in\SV$. A decision function $\#h$ is then evaluated by the
$\ell$-risk
\[ 
%\begin{split}
% \displaystyle R^\ell(\#h;p) = \displaystyle\SE_{p(\#x,\#y)} \, \ell(\#y,\#h(\#x)) = \\
%  \displaystyle \SE_{p(\#x)} \sum_{\#y\in\SY^\SV} \, p(\#y\mid\#x) \,
% \ell(\#y,\#h(\#x)) 
%  =  \SE_{p(\#x)}\, \#p_{\#y}(\#x)^\top \, \#\ell_{\#h(\#x)}\:,
%  \end{split}
 \displaystyle R^\ell(\#h;p) = \displaystyle\SE_{p(\#x,\#y)} \, \ell(\#y,\#h(\#x)) = 
  \displaystyle \SE_{p(\#x)} \sum_{\#y\in\SY^\SV} \, p(\#y\mid\#x) \,
 \ell(\#y,\#h(\#x)) 
  =  \SE_{p(\#x)}\, \#p_{\#y}(\#x)^\top \, \#\ell_{\#h(\#x)}\:,
\]
where $\#p_{\#y}(\#x) = (p(\#y\mid\#x) \mid \#y\in\SY^\SV)$ is a vector function
denoting the conditional probabilities at $\#x$ and
$\#\ell_{\#t}=(\ell(\#y,\#t)\mid \#y\in\SY^\SV)$ is a vector of losses for the 
decision $\#t\in\ST^\SV$. The ultimate goal is to learn from $\SD_m$ a decision function with
the $\ell$-risk close to the Bayes $\ell$-risk
\[
  R^\ell_*(p) = \inf_{\#h\colon\SX\rightarrow\ST^\SV} R^\ell(\#h;p) =
  \SE_{p(\#x)}\min_{\#t\in\ST^\SV} \#p_{\#y}(\#x)^\top \#\ell_{\#t} \:.
\]
A direct minimization of the loss $\#\ell$ is often a hard problem. Therefore it
is common to replace $\ell \colon \SY^\SV\times\ST^\SV\rightarrow\Re_+$ by a surrogate loss function
$\psi\colon\SY^\ST\times\hat{\ST}\rightarrow\Re_+$, which operates on a surrogate
decision set $\hat{\ST}\subseteq\Re^d$. The goal is then to learn a 
function $\#f\colon\SX\rightarrow\hat{\ST}$ minimizing the $\psi$-risk
\[
%\begin{split}
%R^\psi(\#f;p) = \displaystyle \SE_{p(\#x,\#y)}  \,  \psi(\#y, \#f(\#x)) = \\
%\displaystyle  \SE_{p(\#x)} \, \sum_{\#y\in\SY^\SV} p(\#y\mid\#x)
%\psi(\#y,\#f(\#x))  = 
%  \SE_{p(\#x)} \, \#p_{\#y}(\#x)^\top \#\psi_{\#f(\#x)}\:,
%\end{split}
R^\psi(\#f;p) = \displaystyle \SE_{p(\#x,\#y)}  \,  \psi(\#y, \#f(\#x)) = 
\displaystyle  \SE_{p(\#x)} \, \sum_{\#y\in\SY^\SV} p(\#y\mid\#x)
\psi(\#y,\#f(\#x))  = 
  \SE_{p(\#x)} \, \#p_{\#y}(\#x)^\top \#\psi_{\#f(\#x)}\:,
\]
where $\#\psi_{\hat{\#t}}=(\psi(\#y,\hat{\#t})\mid \#y\in\SY^\SV)$ is a vector
of proxy losses at the decision $\hat{\#t}\in\hat\ST$. The learned function
$\#f$ is used to construct the decision function via a 
transform~$\pred\colon\hat{\ST}\rightarrow\ST$. The $\ell$-risk of the resulting decision
function $\pred(\#f(\#x))$ is $R^\ell( \pred\circ\#f; p)$. For example,
$\#f(\#x) = (\lz \#w,\#\Psi(\#x,\#y)\pz \mid \#y\in\SY^\SV)$ is a vector of
scores linear in parameters $\#w\in\Re^n$ and $\pred(\hat{\#t}) \in
\Argmax_{\#y\in\SY^\SV} \hat{t}_{\#y}$, which yields the linear structured output
classifier $\#h(\#x) \in \Argmax_{\#y\in\SY^\SV} \lz \#w, \#\Psi(\#x,\#y) \pz$.

Under suitable conditions the uniform law of large numbers
applies~(e.g.~\cite{Vapnik98-StatisticalLearningTheory}) and learning $\#f_m$ from
$\SD^m_{\#x\#y}$ by minimizing the empirical risk $R_{\rm
  emp}^\psi(\#f)=\frac{1}{m}\sum_{i=1}^m\psi(\#y^i,\#x^i)$ is statistically
consistent, i.e. for the number of examples $m$ going to infinity,
$R^\psi(\#f_m;p)$ converges in probability to the minimal (Bayes) $\psi$-risk
\[
     R^\psi_*(p) = \inf_{f\colon \SX\rightarrow\hat{\ST}} R^\psi(\#f;p) \:.
\] 
%
It has been shown (e.g. \cite{Zhang04a,Tewari-JMLR2007, Gao-JMLR2011}) that the
consistency with respect to the $\psi$-risk implies the consistency with respect
to the $\ell$-risk provided the surrogate loss $\psi$ is so called
classification calibrated w.r.t the loss~$\ell$. In this chaper, we will extend
this result to the setting when the training examples are partially annotated as
defined in the next section.
