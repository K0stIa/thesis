import bibtexparser

with open('thesis.bib') as bibtex_file:
    bibtex_str = bibtex_file.read()

bib_database = bibtexparser.loads(bibtex_str)
# print(bib_database.entries)

files = ['ID', 'author', 'book_pages', 'book_title', 'chapter', 'editor', 'isbn', 'pages', 'publisher', 'title', 'year', 'ENTRYTYPE', 'journal', 'series', 'author']