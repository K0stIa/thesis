%!TEX root = ../thesis_main.tex

\section{Discriminative learning from partially annotated examples}
\label{sec:Discriminative-formulation}

In the case of learning from partially annotated examples, we are provided with
a set of admissible labels only one of each is correct. This differs from the
supervised setting, where we have one to one correspondence between the input
instances and labels.  More precisely, we consider a set of partially annotated
training examples
\begin{equation}
   \label{equ:intro-patiallysupervisedTrainingSet}
    \SD^m_{xa} = \{(\#x^1,\#a^1),\ldots,(\#x^m,\#a^m)\}\in(\SX\times2^{\SY})^m \:,
\end{equation}
assumed to be drawn from i.i.d. random variables with some unknown joint
distribution 
\[
          p(\#x,\#a) = \sum\limits_{y} p(\#x, \#a, y) \:.
\]
Each training input $\#x^i$ comes along with a set of candidate labels $\#a^i
\subset 2^{\SY}$ ($\ |\#a| \ge 1$). A common assumption on the data generating
distribution $p(\#x,\#a,y)$ is that the ground truth label $y^i$ is among the
known candidate labels $\#a^i$, i.e. $y^i\in \#a^i$.
%In contrast to the supervised scenario, example may be provided 
%not with single label $y$, but with candidate set of possible ground truth labels $\#a \subset 2^{\SY}$ ($\ |\#a| \ge 1$),
%only one of which is the real ground truth label.

The ultimate goal is the same as in the supervised learning, that is,
for a given loss function $\ell\colon\SY\times\SY\rightarrow\Re_{+}$ we want to
learn a classifier $h \colon \SX \rightarrow \SY$ whose Bayes
risk~\equ{equ:expRiskGeneric} defined w.r.t 
\[
 p(\#x, y) = \sum\limits_{\#a} p(\#x, \#a, y)
\]
is as small as possible. Although the goals are the same, the learning
algorithms are not. Namely, the \ac{ERM} methodology cannot be used directly
because the loss function $\ell\colon\SY\times\SY\rightarrow\Re_{+}$ is
undefined over the annotations (i.e. the subsets $2^{\SY}$) contained in the
partially annotated training set $\SD_{xa}^m$.
% Clearly, the loss function
%$\ell\colon\SY\times\SY\rightarrow\Re_{+}$ can not be used directly for provided
%data $\SD_{xa}^m$, since it is undefined over $2^{\SY}$.  
One option to make the \ac{ERM} applicable
%, first appearing in~\cite{Taskar11-LearningfromPartialLabels}, 
is to derive
%One possible option is
%to derive 
so called partial loss $\ell^P \colon \SY \times 2^{\SY} \rightarrow \Re_{+}$
from a given complete (target) loss $\ell$ by minimizing over admissible labels: 
\begin{equation}
\label{equ:partia-risk-hypothesis}
\ell^P(y, \#a) = \min \limits_{\hat{y} \in \#a} \ell(\hat{y}, y) \:.
\end{equation}
%
The partial loss $\ell^P$ has been explicitly defined
in~\cite{Taskar11-LearningfromPartialLabels} for a case when $\ell$ is the
0/1-loss. However implicitly, via defining a learning algorithm which in its
core minimizes the partial loss, it has been used many times in various
contexts.  For example, it is minimized by an algorithm learning the Hidden
Markov Chain based classifiers~\cite{Do-HMMfromPartialStates-ICML2009}, generic
structured output models~\cite{Lou-StructPartialLearn-ICML2012}, the
multi-instance learning~\cite{LuoO10-CandidateSets} or the named entity
recognizer~\cite{Fernandes-ECML2011}.
%The view of partial loss $\ell^P$ has been proposed for the first time
%in~\cite{Taskar11-LearningfromPartialLabels}.  
%Extending the loss function $\ell \colon \SX \rightarrow \SY$ on domain
%$2^{\SY}$ is not enough. 
%One has to specify the distribution over pairs $(\#x,
%\#a)$, so we can treat learning as seeking for the classifier $h \colon \SX
%\rightarrow \SY$ that minimizes the partial risk

Having the partial loss, we can define the partial risk 
\begin{equation}
\label{equ:partialRiskIntro}
   R^{\ell^P}(h) = \SE_{p(\#x,\#a)} \ell^P(h(\#x), \#a) \:,
\end{equation}
and search for the best (Bayes) classifier $h \colon \SX\rightarrow \SY$ that
minimizes the partial risk 
\begin{equation}
\label{equ:bestPartialClassifier}
   h^{\ell^P}_{*} \in \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell^P}(h) \:.
\end{equation}
%
The partial risk minimization problem~\equ{equ:bestPartialClassifier} can be
already approached by the \ac{ERM} methods. However, the central question is
whether the \ac{ERM} methods can provide a good approximation of the target
problem~\equ{equ:IntroBayesGeneric}. An answer to this question in the case of
structured output classification (i.e. when $y$ is a vector of labels) is one of
the contributions of the thesis. Our approach is very briefly outlined in
the rest of this section.

We will show that for some distributions $p(\#x,\#a,y)$ the
problem~\equ{equ:bestPartialClassifier}
%which is solvable in the presence of partially annotated examples $\SD^m_{xa}$, 
is equivalent to the target problem~\equ{equ:IntroBayesGeneric} in the sense
that both problems share the set of solutions. In particular, the classifier
$h^{\ell^P}_{*}$ obtained by minimizing the partial
risk~\equ{equ:bestPartialClassifier} is a minimizer of the target (complete)
risk~\equ{equ:expRiskGeneric} as well, i.e.  the inclusion
%
% Ideally, we would like to have sufficient conditions on the relation between
% distributions $p(\#x, \#a)$ and $p(\#x, y)$, so that the classifier
% $h^{\ell^P}_{*}$ minizing the partial risk~\equ{equ:partialRiskIntro} is a
% minimizer of the target (complete) risk~\equ{equ:expRiskGeneric} as well,
% i.e. 
\begin{equation}
\label{equ:DesiredProperty}
   h^{\ell^P}_{*} \in \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell}(h) \:
\end{equation}
holds or equivalently 
\begin{equation}
\label{equ:DesiredProperty2}
R^{\ell}(h^{\ell^P}_{*}) = R^{\ell}(h^{\ell}_{*}) \:.
\end{equation}

%There is currently no theoretical general justification for the methods based on this partial loss function 
%apart from special cases have been studied so far~\cite{Taskar11-LearningfromPartialLabels, Cid-SueiroGS14-FlatPartialConsistency}.
%The question ``why a classifier minimizing induced partial loss'' should in general minimize original loss
%which is real objective stays open.
%
%Likewise in supervised scenario the task now is to learn the 
%classifier $h \colon \SX \rightarrow \SY$ whose {\em Bayes risk}
%\begin{equation}
%  \label{equ:expPartialRiskGeneric}
%   R^{\ell^P}(h) = \SE_{p(\#x,\#a)} \ell^P(h(\#x), \#a)
%\end{equation}
%is as small as possible
%\begin{equation}
%\label{equ:IntroPartialBestGeneric}
%	h^{\ell^{P}}_{*} \in \Argmin \limits_{h \colon \SX \rightarrow \SY}R^P(h) \:.
%\end{equation}
%
%Of course, one has to answer how the distribution $p(\#x, \#a)$ is related to distribution $p(\#x, y)$ and 
%what is the relation between $h^{\mbox{bayess}}$ and $h^{\mbox{best}}$. 
After establishing the equivalence~\equ{equ:DesiredProperty2}, 
%such sufficient conditions, 
one can solve the partial risk minimization
problem~\equ{equ:bestPartialClassifier} by the \ac{ERM} methods as follows.
%
%with surrogate (easier)
%problem like in previous section. 
%Under certain conditions~\cite{Vapnik-Nature95} 
The partial risk~\equ{equ:partialRiskIntro} can be approximated by the empirical risk
\begin{equation}
 \label{equ:IntroPartialEmpRisk}
 R^{\ell^P}_{\rm emp} (h) = \SE_{s(\#x,\#a)}\ell^P(h(\#x), \#a) \:,
\end{equation}
where
\begin{equation}
   s(\#x,\#a) =\frac{1}{m}\sum_{i=1}^m \leftbb \#x^i=\#x \wedge \#a^i=\#a \rightbb \:.
\end{equation}
%Moreover, if property~\equ{equ:DesiredProperty} is established,
%empirical risk~\equ{equ:IntroPartialEmpRisk} is asymptotically consistent with estimate of risk~\equ{equ:expRiskGeneric}.

\begin{comment}
As in supervised scenario, solving 
This would mean that we can 
follow the \ac{ERM} approach formulated by~\cite{Vapnik-Nature95} and we could substitute 
task~\equ{equ:IntroPartialBestGeneric} by the following one:

\begin{equation}
 \label{equ:PartialEmpRiskMin}
   \hat{h}^{\ell^{P}}\in \Argmin_{h\in\SH} \SE_{s^P(\#x,\#a)}[\ell^P(h(\#x), \#a)] \:,
\end{equation}
where $\SH$ is a fixed hypothesis space and $\SE_{s^P(\#x,\#a)}[\ell^P(h(\#x), \#a)]$ is the empirical risk by 
the expectation computed over the empirical distribution
\begin{equation}
   s^P(\#x,\#a) =\frac{1}{m}\sum_{i=1}^m \leftbb \#x^i=\#x \wedge \#a^i=\#a
   \rightbb \:.
\end{equation}

Likewise in supervised setting the \ac{ERM} task%~\equ{equ:PartialEmpRiskMin} is
hard to solve and therefore it can be replaced by the task
\begin{equation}
%\label{equ:PartialSurrogateRiskMin}
   h^{\psi^{P}}_{*} \in \Argmin_{h \in \SH} \frac{1}{m}\sum_{i=1}^m 
     \psi^P(h(\#x^i);\#a^i) \:,
\end{equation}
after making same assumption on hypothesis space $\SH$ and considering partial bound 
$\psi^P(h(\#x);\#a)$ on partial empirical risk $ \ell^P(h(\#x), \#a)$.
\end{comment}

As in the supervised setting, the partial empirical
risk~\equ{equ:IntroPartialEmpRisk} is hard to minimize directly. Hence, the
partial loss $\ell^P \colon \SY \times 2^{\SY} \rightarrow \Re_{+} $ is replaced
by an easier-to-minimize surrogate partial loss $\psi^P \colon 2^{\SY}
\times\hat{\ST}\rightarrow\Re_{+}$, which operates on the surrogate decision set
$\hat{\ST} \subset \Re^{Y}$.
%
%problem~\equ{equ:IntroPartialEmpRisk} of
%partial empirical risk minimization can be simplified by shrinking the
%minimization set of all possible classifiers $h \colon \SX \rightarrow \SY$ to
%some hypothesis space $\SH$ that contains Bayes classifier and by substitution
%of the partial loss function $\ell^P \colon \SY \times 2^{\SY} \rightarrow
%\Re_{+} $ by surrogate one $\psi^P \colon 2^{\SY}
%\times\hat{\ST}\rightarrow\Re_{+}$, which operates on the surrogate decision set
%$\hat{\ST} \subset \Re^{Y}$.
%
%Problem~\equ{equ:DesiredProperty} transforms to the following optimization problem
%\begin{equation}
% \label{equ:PartialOriginalRiskMin}
%  h_{*}^{\ell^P} \in \Argmin_{h \in \SH} R^{\ell^P}_{\rm emp} (h) \:.
%\end{equation}
% 
%The problem~\equ{equ:DesiredProperty} can be then approximated by a surrogate problem, 
The surrogate $\psi^P$ loss is used to learn a surrogate decision function $f\colon \SX
\rightarrow \hat{\ST}$ such that
\begin{equation}
\label{equ:introPartialSurrogate}
%   f^{{\rm emp},\psi^p}_{*} \in \Argmin_{f \colon \SX \rightarrow \hat{\ST}}  R^{\psi^P}_{\rm emp} (f)  \:,
   f^{{\rm emp},\psi^p}_{*} \in \Argmin_{f \in\SF}  R^{\psi^P}_{\rm emp} (f)  \:,
\end{equation}
where
\begin{equation}
 \label{equ:IntroSurrogatePartialEmpRisk}
 R^{\psi^P}_{\rm emp} (f) = \SE_{s(\#x,\#a)}\psi^P(\#a,f(\#x)) \:.
\end{equation}
Finally, the resulting classification rule is constructed by composing the
learned function $f^{{\rm emp},\psi^p}_{*}$ and a fixed prediction function
$\mbox{pred}$, i.e. $h =\mbox{pred} \circ f^{{\rm emp},\psi^p}_{*}$.
%
%The learned surrogate desicion function $f \colon \SX \rightarrow \hat{\ST}$ is
%used to construct the decision function $h \colon \SX \rightarrow \SY$ via a
%transformation $\mbox{pred} \colon \hat{\ST} \rightarrow \ST$, i.e. $h =
%\mbox{pred} \circ f$.

%That's to say, the initial problem of learning with missing data is reduced to the following one
%\begin{equation}
% \label{equ:PartialSurrogateRiskMin}
%  f^{\psi^P}_{\#w^*_{m}} \in \Argmin_{\#w \in \Pi} \frac{1}{m}\sum_{i=1}^m
%     \psi^P(\#a^i, f(\#x^i; \#w)) \:,
%\end{equation}
%
%The difficulty of the problem~\equ{equ:introPartialSurrogate} very much depends on the form of surrogate loss $\psi^P$
%(e.g. it is helpful if $\psi^P$ is convex),
%as well as on the type of function $f\colon \SX \rightarrow \hat{\ST}$, e.g. ordinal classification problem,
%flat multlabel classification problem, structured output problem, etc.

%Likewise in the supervised setting, we want solutions of the
%problem~\equ{equ:introPartialSurrogate} to be statistically consistent, namely,
%as the number of examples goes to infinity the expected risk of the learned
%classifier $R^\ell( \mbox{pred} \circ f^{{\rm emp},\psi^p}_{*} )$ should converge in
%probability to the minimal (Bayes) risk $R^\ell(h_{*}^\ell)$.
%, the empirical risk $R^{\ell}_{\rm
%  emp} (\mbox{pred} \circ f^{\psi^P {\rm emp}}_{*})$ should converge in
%probability to the minimal Bayes risk $R^{\ell} (h_*^{\ell})$. 

Likewise in the supervised setting, in order to justify the \ac{ERM}
problem~\equ{equ:introPartialSurrogate} we also need to study consistency of the
surrogate partial loss. To this end, we introduce in this thesis a concept of a
classification calibrated partial loss. Loosely speaking, if a surrogate partial
loss $\psi^p$ is classification calibrated w.r.t. the partial loss $\ell^p$ then
for any distributions $p(\#x,\#a)$ it holds that
\begin{equation}
\label{equ:introPartialConsistency}
\mbox{pred} \circ f^{\psi^P}_{*} \in  \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell^P}(h) \:,
\end{equation}
where $f^{\psi^P}_{*}$ is a minimizer of the partial surrogate risk, i.e.,
\begin{equation}
f^{\psi^P}_{*} \in \Argmin \limits_{f \colon \SX \rightarrow \hat{\ST}} R^{\psi^P} (f) \:,
\end{equation}
\begin{equation}
 \label{equ:IntroSurrogateRisk}
R^{\psi^P} (f) = \SE_{p(\#x,\#a)}\psi^P(\#a,f(\#x)) \:.
\end{equation}
%
Consequently, using the inclusion~\equ{equ:introPartialSurrogate} we can show
that any minimizer of the partial surrogate risk $f^{\psi^P}_{*}$ is the Bayes
classifier of the target (complete) risk, i.e., it holds that
\begin{equation}
\label{equ:introPartialAuxConsistency}
\mbox{pred} \circ f^{\psi^P}_{*} \in  \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell}(h) \:.
\end{equation}
%
In this sense the minimization of the surrogate partial risk $R^{\psi^P} (f)$ is
equivalent to (or consistent with) the minimization of the target risk
$R^{\ell}(h)$. Under some conditions, the equivalence is preserved even if the
true risks are replaced by their empirical estimates. This allows us to show
that the learning algorithms which in their core solve the
problem~\equ{equ:introPartialSurrogate} with calibrated surrogate partial loss
are statistically consistent. Namely, we will prove that for the number of
examples going to infinity the expected risk of the learned classifier $R^\ell(
\mbox{pred} \circ f^{{\rm emp},\psi^p}_{*} )$ converges in probability to the
minimal (Bayes) risk $R^\ell(h_{*}^\ell)$.

%problem~\equ{equ:introPartialSurrogate} to be statistically consistent, namely,
%as the number of examples goes to infinity the expected risk of the learned
%classifier $R^\ell( \mbox{pred} \circ f^{{\rm emp},\psi^p}_{*} )$ should converge in
%probability to the minimal (Bayes) risk $R^\ell(h_{*}^\ell)$.
%, the empirical risk $R^{\ell}_{\rm
%  emp} (\mbox{pred} \circ f^{\psi^P {\rm emp}}_{*})$ should converge in
%probability to the minimal Bayes risk $R^{\ell} (h_*^{\ell})$. 
%

\begin{comment}
%
%
%
To this end, we need to study statistical consistency of the partial surrogate
losses $\psi^P$. We call the partial surrogate loss $\psi^P$ classification
calibrated with respect to the original loss $\ell$ if
%statistically
%consistent, or classification calibrated, or Fisher consistent with respect to
%the original loss $\ell$ if
\begin{equation}
\mbox{pred} \circ f^{\psi^P}_{*} \in  \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell}(h) \:,
\end{equation}
where
\begin{equation}
f^{\psi^P}_{*} \in \Argmin \limits_{f \colon \SX \rightarrow \hat{\ST}} R^{\psi^P} (f) \:,
\end{equation}
\begin{equation}
R^{\psi^P} (f) = \SE_{p(\#x,\#a)}\psi^P(\#a,f(\#x)) \:,
\end{equation}
%
for any distribution $p(\#x, \#a, y)$.


Note, the classifier $\mbox{pred} \circ f^{\psi^P}_{*}$ is required to be in the set of minimizers of 
not only the partial risk~\equ{equ:partialRiskIntro}, but the original risk~\equ{equ:expRiskGeneric} also.
That is to say, showing the statistical consistency became more complex in contrast to the supervised
setting. Existing methods that analyse statistical consistency can not be used. 
We proposed the way to approach the problem~\equ{equ:introPartialConsistency} by breaking it to subproblems, 
e.g. to give sufficient conditions, under which 
\begin{equation}
\label{equ:partial_consistency}
\mbox{pred} \circ f^{\psi^P}_{*} \in  \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell^P}(h)
\end{equation}
and
\begin{equation}
\label{equ:partial_consistency2}
h^{\ell^P}_{*} \in  \Argmin \limits_{h \colon \SX \rightarrow \SY}R^{\ell}(h) \:.
\end{equation}
More precisely, we separate consistency of partial surrogate loss against
partial loss~\equ{equ:partial_consistency} and consistency of partial loss
against complete loss~\equ{equ:partial_consistency2}.  The
problem~\equ{equ:partial_consistency} can be seen as analogical to multiclass
problem and if~\equ{equ:partial_consistency2} is established, we can decide
if~\equ{equ:introPartialConsistency} holds using existing criterias for
statistical consistency of surrogate losses for supervised multiclass task.

\end{comment}

%{equ:introPartialSurrogate}
%The 

%More precisely, we separate consistency of partial surrogate loss against
%partial loss~\equ{equ:partial_consistency} and consistency of partial loss
%against complete loss~\equ{equ:partial_consistency2}.  The
%problem~\equ{equ:partial_consistency} can be seen as analogical to multiclass
%problem and if~\equ{equ:partial_consistency2} is established, we can decide
%if~\equ{equ:introPartialConsistency} holds using existing criterias for
%statistical consistency of surrogate losses for supervised multiclass task.

%To conclude, unlike in supervised setting, learning from partial examples is
%poorly studied.  Developing good learning algorithms requires more effort, since
%apart from dealing with statistical consistency of surrogate losses we have to
%come up with pleasant (convex) surrogate losses, if they exists of course.  In
%next section, we describe our goals that we wanted to reach in our proposed
%\ac{ERM} approach for learning from missing labels.

\section{Thesis goals}

This thesis is centered around the \ac{ERM} based algorithms learning
classifiers from partially annotated examples. More precisely, we concentrated
on learning algorithms which in their core solve the surrogate \ac{ERM}
problem~\equ{equ:introPartialSurrogate}. At the beginning of our work on this
topic, there were many ad-hoc methods showing that algorithms
implementing~\equ{equ:introPartialSurrogate} give promising results, i.e. they
were shown to provide a good approximations of the Bayes
classifier~\equ{equ:IntroBayesGeneric}. However, there was no firm theory which
would support these empirical findings. In addition, the existing algorithms
often suffer from using a non-convex surrogate partial losses making the
problem~\equ{equ:introPartialSurrogate} hard to optimize. And thus a further
question is in which cases one can construct a good convex and, at the same
time, easy-to-optimize surrogate partial loss. After recognizing the open
problems, we focused our work on the following questions:
\begin{itemize}
  \item How to design a convex surrogate of the partial loss~\equ{equ:partia-risk-hypothesis}?
  \item How to solve \ac{ERM} problem~\equ{equ:introPartialSurrogate} efficiently?
  \item Under which conditions are algorithms implementing \ac{ERM} problem~\equ{equ:introPartialSurrogate} statistically consistent?
%  \item Which of the existing algorithms implementing \ac{ERM}
%    problem~\equ{equ:introPartialSurrogate} is statistically conxsistent?
\end{itemize}
%
We have not found a complete and general answer to these questions, yet we
managed to contributed to all of them. A summary of our contributions is
provided in the next section.

% approach suitable for learning problems with missing data.  At the beginning of
%our work on this topic, there were many ad-hoc methods showing that
%solving~\equ{equ:introPartialSurrogate} gives good solution
%of~\equ{equ:IntroBayesGeneric}.

\begin{comment}
We set up goal to push forward existing methods to make make \ac{ERM} approach
suitable for learning problems with missing data.  At the beginning of our work
on this topic, there were many ad-hoc methods showing that
solving~\equ{equ:introPartialSurrogate} gives good solution
of~\equ{equ:IntroBayesGeneric}.  However, only few
works~\cite{Taskar11-LearningfromPartialLabels,
  Cid-SueiroGS14-FlatPartialConsistency} providing theoretical studying of the
problem of learning from partial annotation, have appeared in this direction so
far.

In our work, we wanted to discover classes of learning problems, for which in
terms of discussion in the previous section there exist
\begin{itemize}
\item convex surrogate loss function $\psi^P$
\item the surrogate loss function $\psi^P$ is statistically consistent
\end{itemize}
and, thus, make our holly grail goal of making \ac{ERM} approach for learning
from data with missing labels achievable.
\end{comment}

% \cite{Taskar11-LearningfromPartialLabels} provided statistical model and
% sufficient conditions for their setting, under which their convex surrogate
% loss function for learning with missing labels was statistically consistent
% with the initial complete loss.  Unfortunately, the work
% of~\cite{Taskar11-LearningfromPartialLabels} concerned only with $0/1$ loss.
% Simultaneously with our work,~\cite{Cid-SueiroGS14-FlatPartialConsistency}
% showed how we can adopt existing convex surrogate losses for the supervised
% multiclass setting to deal with missing labels. Their work assumes the
% existence of the consistent surrogate loss for the
% multiclass %(structured output setting),
% which, as shown in their work, can be adopted for setting with missing labels.
% Unfortunately, we can not take the existing structured output statistically
% consistent surrogate loss and adopt it to the setting with missing labels,
% because, up to our best knowledge, there are only computationally infeasible
% statistically consistent convex surrogate losses for the supervised structured
% output classifier learning, and according to proposed receipt
% of~\cite{Cid-SueiroGS14-FlatPartialConsistency} we would have to solve
% counting problem over exponentially large set.

% We concentrated our attention in this thesis to structured output and ordinal
% classifiers learning setting as specific subclasses of multiclass learning.
% The task was to find a convex statistically consistent surrogate loss for the
% structured output and ordinal classifiers.  To do that, we had to put existing
% ad-hoc methods into perspective, e.g. to understand which of them are
% statistically consistent and which of them are not.  We had to understand
% whether it is possible to build a computationally feasible convex consistent
% surrogate loss for structured output setting in principle.
%We explain our contributions to this problem in the next section.

\section{Contributions}
%Unarguably, finding a statistically consistent convex surrogate loss $\psi^P(\#a, f(\#x))$
%for the structured output setting is definitely very attractive and difficult question.

In this work, we investigated two different classification scenarios both falling
under the umbrella of learning from partial annotations. First, learning of
ordinal classifiers from interval annotations. Second, learning of structured
output classifiers from examples with missing labels.

\begin{comment}
  We concentrated on the ordinal classifier as some very special and relatively
  well studied subclass of multiclass classification problems. The goal of
  working at learning of ordinal classifier form interval annotations was to
  demonstrate our motivation of diving deeper into learning from partially
  annotated examples scenario.  As result, we discovered convex surrogate loss
  $\psi^P(\#a, f(\#x))$ for learning ordinal classifier from interval
  annotations. While working on this question, we also contributed to supervised
  setting of ordinal classifier learning.  We provide the bound on the partial
  risk similar to~\cite{Taskar11-LearningfromPartialLabels}.
  \todo[inline]{\cite{BachOrdConsistency} made comprehensive study of supervised
    setting of ordinal classifier learning. Thanks to it we can make conclusions
    about proposed setting for learning ordinal classifier from interval
    annotations.  If there is time I think it worth of working out this
    question.}

  In case of general structured output setting, our contribution are sufficient
  conditions on statistical consistency of the surrogate loss function. We show
  the existence of the convex statistically consistent surrogate loss. We
  analyse existing ad-hoc methods as ramp-loss non-convex minimization on
  statistical consistency.
\end{comment}

\subsection{Learning ordinal classifier from interval annotations}

We consider learning of the ordinal classifiers (i.e. classification model
assuming ordered labels) from examples of inputs annotated by intervals of
admissible labels.

\begin{itemize}
  %
\item We propose an interval insensitive loss (IIL) function to measure
  discrepancy between the interval of admissible labels given in the annotation
  and a label predicted by the classifier. The IIL can be build from arbitrary
  target (complete) V-shape loss like, for example, the 0/1-loss or mean
  absolute error (MAE).  The IIL is an instance of the generic partial
  loss~\equ{equ:partia-risk-hypothesis}. In contrast to existing instances of
  the partial loss~\equ{equ:partia-risk-hypothesis}, the IIL for ordinal
  classification can be approximated by tight convex surrogates as we will show.
  %
\item We show that the expectation of the IIL is a reasonable proxy of the
  expectation of the target complete loss. In particular, we show that the
  target risk $R^\ell$ is upper bounded by a linear function of the partial risk
  $R^{\ell^P}$. We show how the tightness of this upper bound depends on the
  annotations process which was used to generate the training examples.
  %
\item We show how to build tight convex surrogates of the IIL. The convex
  surrogates are obtained by extending surrogates known from existing
  supervised algorithms for ordinal regression. These surrogates are can be used
  as a proxy for the 0/1-loss or the MAE loss. We also propose a novel convex
  surrogate of a generic V-shaped interval-insensitive loss.
  %
\item We propose an efficient cutting plane solver for minimization of the
  \ac{ERM} problem~\equ{equ:introPartialSurrogate}. In contrast to existing CPA
  solvers, it can deal with situations when the quadratic regularizer is not
  imposed on all model parameters which, as will be also shown, has significant
  influence on the final accuracy of the learned ordinal classifier. 
  %
\item We have not managed to prove consistency of the IIL. Instead, we performed
  a thorough empirical evaluation showing that minimization of the interval
  insensitive loss provides a good approximation of the target Bayes
  classifier~\equ{equ:IntroBayesGeneric}. 
\end{itemize}

%We show how to learn the ordinal classifier from given coarse labels, 
%i.e. range (interval) of possible ground true labels.
%To deal with the interval annotations, we propose interval-insensitive loss
%function, which extends an arbitrary (supervised) V-shaped loss to the interval
%setting. The interval-insensitive loss measures a discrepancy between the
%interval of possible labels given in the annotation and a label predicted by the
%classifier. Our interval-insensitive loss can be seen as the ordinal regression
%counterpart of the $\epsilon$-insensitive loss used in the Support Vector
%Regression~\cite{Vapnik98-StatisticalLearningTheory}. We prove that the expectation of the
%interval-insenstive loss, which can be evaluated on partially annotated examples
%is a reasonable proxy of the expectation of the supervised loss evaluated on precisely
%annotated examples. We show how to modify the existing supervised methods, the
%\ac{SVOR-EXP} and the \ac{SVOR-IMC} algorithms, to be applicable on partially annotated
%examples. We also propose a generic convex surrogate of the V-shaped
%interval-insensitive loss and show how to efficiently optimize large instances
%of the resulting convex program by a double-loop cutting plane algorithm.
%

While working on this topic, we also made some progress on supervised learning
of ordinal classifiers as a byproduct:

\begin{itemize}
  %
  \item We proved that the ordinal classifier is equivalent to
  a linear multi-class classifier whose class parameter vectors are collinear and
  with magnitude linearly increasing with the labels. We call the new
  representation as the \ac{MORD} classifier.  Our equivalence proof
  is constructive so that we can convert any ordinal classifier to the \ac{MORD} classifier
  and vice-versa. 
 %
\item The MORD representation allows to express the space of ordinal classifiers
  $\SH_{\rm ord}$ as composition of the ``argmax'' prediction transform ${\rm
    pred(\#t)}=\argmax_{y\in\SY}t_y$ and a linear decision function $f\in\SF$,
  i.e. $\SH_{\rm ord}=\{ {\rm pred} \circ f \mid f\in\SF\}$.
  %
%Hence, we show that conversions of learning of the ordinal classifier into
%learning a set of two-class classifiers after what the resulting two-class classifiers are
%trained by a modified \ac{SVM} algorithm~\cite{Shashua-LargeRank-NIPS2002, Chu-SVOR-ICML2005, Li-OrdregExtBinClass-NIPS2006}
%or Perceptron~\cite{Crammer-Pranking-NIPS} are not necessary.
  %
  In turn, the \ac{MORD} representation can be beneficial for learning and analysis
  of the ordinal classifiers by using algorithms and results for well understood
  multi-class linear classification.
% In particular, the well understood methods for learning multi-class linear
  %classifiers can be applied readily. 
  For example, we show that a generic \ac{SO-SVM} algorithm can be applied for
  learning of the \ac{MORD} classifier and that it delivers the same (or
  slightly better) results when compared to the existing learning algorithms for
  the ordinal classification. Moreover, the \ac{SO-SVM} approach works for
  arbitrary loss function in contrast to existing methods which require the
  V-shaped losses.
 %
\item We show that the \ac{MORD} representation allows introduce more complex
  models for ordinal classification. Namely, we propose a \ac{PW-MORD} which
  subsumes the standard ordinal classifier and unrestricted multi-class
  classifies as special cases. We demonstrate advantages of the proposed models
  on standard benchmarks as well as on solving a real-life problem of estimating
  human age from facial images.
\end{itemize}

\subsection{Learning structured output classifier from examples with missing labels}

We concentrate on a scenario, when the object is characterized by an input
observation and labelling of a set of local parts, however, a training set
contains examples of inputs and labelings only for a subset of the local parts.

\begin{itemize}
 %
\item We provide sufficient conditions which admit to prove that the expected
  risk $R^\ell(h)$ of the structured predictor $h$ learned by minimizing the
  partial risk $R^{\ell^P}(h)$ converges in probability to the optimal Bayes
  risk $R^\ell(h_*^\ell)$. The sufficient conditions restrict the target loss
  $\ell$ to be additive over the local parts while the data generating process
  $p(\#x,\#a,\#y)$ can be fairly generic.
% In particular, it is
%  sufficient to require that the target loss $\ell$ is additively decomposable
%  over the local parts and that the annotation $\#a$.
  %  and labeling $\#y$ is
  %conditionally independent given $\#x$, $p(\#a,\#y\mid \#x) = p(\#a\mid\#x)
  %p(\#y\mid\#x)$.
  %
\item We define a concept of classification calibrated surrogate partial losses
  which are easier to optimize, yet their minimization preserves the statistical
  consistency.  
%cy by adapting the existing general
%  framework for supervised learning. 
% of~\cite{CC-dimm-Multiclass} to our setting. 
  % 
  %We define a concept of surrogate classification calibrated partial losses,
  %  which are easier to optimize, yet their minimization preserves the statistical
  %  consistency.  
  %
\item We analyze surrogate losses used by the existing algorithms implementing
  the \ac{ERM} minimization~\equ{equ:introPartialSurrogate} for learning of
  structured output classifiers from examples with missing labels. For example,
  we show that the ramp-loss and some of its modifications which have been most
  frequently used are classification calibrated and, in turn, the corresponding
  algorithms are statistically consistent. Our analysis provides a missing
  theoretical justification for so far heuristic methods.
  %
\item We prove the existence of a convex classification calibrated surrogate for
  partial learning. The proof is based on establishing a connection between
  learning from partially annotated examples and the recently published theory
  on consistency of supervised learning. 
%Unfortunately, the proof is not
%  constructive and hence the convex classification calibrated surrogate,
%  minimization of which would be tractable at the same time, remains an open
%  problem.
\end{itemize}

%  Our contribution is in providing a missing theoretical justification for many
%  existing methods, which have been working well with empirical justification
%  only and have been used in practice routinely. 

\section{Thesis outline}

\begin{enumerate}\itemsep=10pt
\item[] {\bf Chapter~\ref{cha:state-of-the-art}} contains the state-of-the-art
  relevant to the topics studied in the thesis. In particular, we review works
  related to the statistical consistency of algorithms learning from fully
  annotated and partially annotated examples, we also review optimization
  algorithms which have been used to solve the \ac{ERM}
  problem~\equ{equ:introPartialSurrogate} and, finely, we review existing
  discriminative learning methods for the ordinal classifiers.
  %
\item[] {\bf Chapter~\ref{cha:ordinal_classification}} describes our
  contributions to the problem of learning ordinal classifiers from fully
  annotated examples and examples with interval (partial) annotation of labels.
  %
%\begin{comment}
%In particular:
%\begin{itemize}
%\item the efficient parametrisation of the ordinal classier. 
%\item introduce \ac{PW-MORD} classifier. 
%\item experimental evaluation to show the performance of proposed reparametrization of ordinal classifier and~\ac{PW-MORD} classifier for 
%supervised setting and \ac{VILMA} for learning from interval annotations.
%\end{itemize}
%\end{comment}
 %
\item[] {\bf Chapter~\ref{cha:multi-label-classification}} describes our
  contributions to the problem of statistical consistency of algorithms learning 
  structured output classifiers from examples with missing labels.
%\begin{comment}
%\begin{itemize}
%\item We provide a statistical model to deal with examples with missing labels and provide sufficient conditions on proposed model,
%under which minimization of the partial loss gives Bayes classifier.
%\item We establish the way of studying existing surrogate loss functions for structured output classifier learning from data with missing labels.
%\item We prove the existence of statistically consistent convex surrogate loss function for structured output classifier learning from data with missing labels.
%\item We conduct the analysis of the existing ad-hoc methods for structured output classifier learning from the data with missing labels.
%\end{itemize}
%\end{comment}
  %
\item[] {\bf Chapter~\ref{cha:conclusions}} contains conclusions resulting from
  the work done in this thesis and also a discussion of a possible future work.
  %
\end{enumerate}

We give a more detailed road map of the each individual chapter at its beginning.