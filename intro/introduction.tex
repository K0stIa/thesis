%!TEX root = ../thesis_main.tex

\chapter{Introdution} 

In this thesis, we consider a problem of learning classifiers from partially
annotated examples. This means that instead of a single label per instance, we
are given a set of admissible labels only one of which is correct. Such scenario
is common in practice.
% where data is often provided with weak (coarse) annotation and nevertheless we
% have to solve classification problem.
For instance, the problem of learning from partially annotated examples
naturally arises in age recognition from facial images. Instead of acquiring a
precise age for each facial image in the training set, which is often
expensive or impossible, it is easier to collect age ranges that can be, for
example, estimated by a human annotator.
% for each person in the training set, which is sometimes even
% impossible, %one would rather ask to estimate person's age range,
%it is easier to collect their age range.
See Figure~\ref{fig:age_example} where each subject is annotated by a range of
ages instead of a precise age.
\begin{figure}
\center
\includegraphics[width=0.4\linewidth] {img/age_example.png} 
\caption{Example of facial images with partial annotation of age. Getting rough
  age ranges of each person is relatively easy while providing exact age is
  difficult. }
\label{fig:age_example}
\end{figure}
Another motivating application can be image segmentation as illustrated in
Figure~\ref{fig:cow_example}. Obtaining a ground true label for each pixel in
the image is obviously tedious and expensive, therefore very often we are
provided with an incomplete labeling, meaning that some pixels in the training
image are left unannotated.
% The presence of weak annotation makes training linear structured output
% classifier difficult, since there is no known transformation of such learning
% scenario to a tractable convex optimization task unlike in well understood
% fully-supervised case.


\begin{figure}[htb]
\center
\begin{tabular}{ccc}
\includegraphics[width=0.28\linewidth] {img/cows_image.png}  &	
\includegraphics[width=0.28\linewidth] {img/cows_annotation.png} &
\includegraphics[width=0.28\linewidth] {img/cows_coarse_annotation.png} \\
 (a) & (b) & (c)
\end{tabular}
\caption{An example of a training instance when learning structured output classifier for image segmentation task. Example of an input image (a), a good(complete) labeling (b), coarse (partial) labeling (c).}
\label{fig:cow_example}
\end{figure}


To put the problem of learning from partial annotations into perspective, it is
useful to list other common learning scenarios (see also Figure~\ref{fig:scenarious}):
%
\input{scenarios_tikz.tex}

\begin{itemize}
\item In the {\bf supervised} scenario each training instance is annotated with a single label.
\item In the {\bf unsupervised} scenario training instances have no label at all. 
\item In the {\bf semi-supervised} scenario each training instance either has a single
  label or it has no label at all.
%\item In {\bf multi-label} scenario each example is assigned multiple labels, all of which can be true.
\item In the {\bf multi-instance} scenario training instances are not
  individually labeled but grouped into sets, which either contain at least one
  positive example or only negative examples.
\item In the {\bf partially annotated} scenario, i.e. the scenario analyzed in this
  thesis, each training instance is annotated with a set of admissible labels
  only one of which is correct.
\end{itemize}

% Note, that learning from partially labeled data is different from the
% semi-supervised setting where labeled and un-labeled examples are available.

There exists two standard paradigms that have been used for learning from
partially annotated examples: the generative approach and the discriminative approach. 
%
%
%To tackle the problem of learning form partially annotated data, one could choose one of two approaches:
%
%\begin{itemize}
%\item maximulgenerative approach,
%\item discriminative approach.
%\end{itemize}
%
%\begin{itemize}
%\item likelihood maximization methods, % estimating model parameters from partially annotated data,
%\item partial empirical risk minimization based methods (analog of empirical risk used in the supervised scenario).
%\end{itemize}
%
The generative approach tries to model the joint probability distribution of the
input observations and the labels. To this end, one has to select an appropriate
class of probabilistic models.
%This means that one has to spend an additional effort on choosing appropriate
%probabilistic model. 
As soon as the class of the probabilistic models is chosen, the maximum
likelihood method (or other estimation method) is used to select a single model
best fitting to the training data. Finally, the required classification rule is
inferred from the learned probabilistic model.
% for finding parameter values of a model that fits the training data best.  
On the other hand, the discriminative approach tries to learn the classification
rule directly. To this end, one has to select an appropriate class of
classification rules. Once the classification model is
chosen, the \ac{ERM} principle (or other method) is
used to select a single classification rule best fitting to the training data.
%
% approach directly cares about assigning the label to input example, so one
% does not need to spend his modelling efforts on the choosing probabilistic
% model. This approach can be used only in the situation, when the desired
% outcome of the learning algorithm is not a probabilistic model, but only a
% classifier that assigns labels to unseen instances.  The advantage of such
% models is their ability to provide probabilities to all unseen inputs.  Of
% course, one has to define the form of generative model to use this learning
% paradigm.  This might become a problem in many practical situations. To
% overcome (or rather avoid) this difficulty, one can consider the
% discriminative learning paradigm, which tries to model a posterior
% distribution directly without specifying the real distribution model. As the
% result, this approach can not be used in the situation when the distribution
% model is the desired outcome of the learning algorithm.
%

In this thesis, we follow the discriminative approach.
%, i.e. the \ac{ERM} based approach. 
%In particular, we concentrate on the problem of learning from the partially
%annotated data, where, to our best knowledge, 
The existing discriminative methods for learning from partially annotated
examples often suffer from the following problems:
\begin{enumerate}
\item There is no clear connection between the target objective and the
  objective function actually optimized by the learning algorithm. The target
  objective is typically the expectation of the complete loss which evaluates
  the response of the classifier given the ground truth label. The 
  objective function of the learning algorithm is typically an average of a
  ``partial loss'' computed on the partially annotated examples. The partial
  loss is a certain function which evaluates the response of the classifier
  given the partial annotation.
%They provide no connection between the optimized objective, the empirical
%  partial risk, and the true objective, which is the Bayes risk with complete
%  loss.
\item The learning problem is usually transformed into a non-convex minimization
  problem which is then approached by a local optimization method with no
  certificate of optimality.
%They provide no guarantees on achieving the global optima of the learning objective
%(as they transform such learning problems into non-convex optimization tasks).
 \end{enumerate}

During our work, we were mainly focused on these two problems. In short, our
main contributions are the following: 
% Contributions described in this thesis concern two specific type of classifiers, i.e. ordinal
% and structured output classifiers. Namely,
\begin{itemize}
\item (Ad problem 1) We developed tools which allow to analyze the statistical
  consistency of algorithms learning the structured output classifiers from
  partially annotated examples. Here the partial annotation means that a subset
  of output labels describing the training instance is missing, e.g. like in the
  image segmentation examples show in Figure~\ref{fig:cow_example}. We applied
  the proposed methodology to existing ad-hoc algorithms and we showed which of
  them are statistically consistent and which are not. Loosely speaking, the
  consistent algorithm provides a minimizer of the target objective, i.e. the
  expectation of the complete loss, provided the number of partially annotated
  training examples goes to infinity. That is, we built a missing bridge between
  the objective function of the consistent algorithms and the target objective.
  %
\item (Ad problem 2) We introduced a new partial loss applicable for learning
  the ordinal classifiers from examples with the interval annotation of the
  labels, e.g. like in the example shown in Figure~\ref{fig:age_example}. We
  establish a connection between the proposed partial loss and an associated
  complete target loss. We designed a convex surrogate of the partial loss which
  allows to convert learning into an optimization problem which can be solved
  efficiently and we show how to do it by cutting plane methods. As a byproduct
  we made several contributions to the supervised learning of the ordinal
  classifiers, namely, we proposed new parametrization of the ordinal classifier
  and we introduced more flexible piece wise version of the ordinal classifier.

 % loss for ordinal classifier learning from interval annotations,
%\item we analyze existing heuristics for structured output classifier learning to be statistical consistent.
\end{itemize}

\medskip In the following two sections we briefly describe the \ac{ERM} based
learning algorithms. We first outline the standard supervised scenario and then
the scenario with the partially annotated examples. We use the two sections in
order to introduce a notation which allow us to describe goals and contributions
of the thesis more precisely at the very end of this chapter.

%To give better intuition about considered problem to the reader,
%we start from short description of known supervised scenario of \ac{ERM} based methods.
\FloatBarrier

