#include <iostream>
using namespace std;

const int N = 505;
int e[N][N];

int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      cin >> e[i][j];
    }
  }
  for (int i = 0; i < n; ++i) e[i][i] = 0;

  for (int k = 0; k < n; ++k) {
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        e[i][j] = min(e[i][j], e[i][k] + e[k][j]);
      }
    }
  }

  return 0;
}
