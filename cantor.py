import numpy as np

def cantor(n):
    return [0.] + cant(0., 1., n) + [1.]

def cant(x, y, n):
    if n == 0:
        return []

    new_pts = [2.*x/3. + y/3., x/3. + 2.*y/3.]
    return cant(x, new_pts[0], n-1) + new_pts + cant(new_pts[1], y, n-1)

x = np.array(cantor(5))
y = np.cumsum( np.ones(len(x))/(len(x)-2) ) - 1./(len(x)-2)
y[-1] = 1

x += 0.3
x = np.r_[-x[::-1], x]
y = np.r_[y[::-1], y]

np.savetxt('cantor.dat', np.vstack([x,y]).T)